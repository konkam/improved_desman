---
title: "Untitled"
format: html
---

```{r}
library(DesmanResultsAnalysis) # available at https://github.com/konkam/DesmanResultsAnalysis
library(tidyverse)
```

```{r}
path_desman_results = "nathan_test/"
```


## If we took the results seriously, we would say

```{r}
path_desman_results %>% 
  paste("all_fit.txt", sep = "") %>% 
  read_delim(delim = ",", col_names = FALSE) %>% 
  ggplot(aes(x = X2, y = X5)) +
  theme_bw() + 
  geom_point() + 
  xlab("Number of variants") + 
  ylab("Deviance")
```
```{r}
path_desman_results %>% 
  paste("all_fit.txt", sep = "") %>% 
  read_delim(delim = ",", col_names = FALSE) %>% 
  filter(X5 == min(X5))
```

The chain with lowest deviance is the 4th when trying 4 variants.

```{r}
path_desman_results %>% 
  paste("desman.4.4/Filtered_Tau_star_haplotypes", sep = "") %>% 
  read_delim(delim = "\t", col_names = FALSE) %>% 
  rename(Position = X2, Variant1 = X3, Variant2 = X4, Variant3 = X5, Variant4 = X6)
```

We actually know that there should be only two variants in the simulated dataset.

### Looking at the trace of the log-likelihood

#### 1 variant: looks ok

```{r, message=FALSE}
log_likelihood_trace(number_of_variants = 1, include_warmup = T, prefix = path_desman_results)
```

#### 2 variants: mixing looks good within chain but not among chains

```{r, message=FALSE}
log_likelihood_trace(number_of_variants = 2, include_warmup = F, prefix = path_desman_results)
```

#### 4 variants: same problem, good mixing within chain, bad mixing among chains

```{r, message=FALSE}
log_likelihood_trace(number_of_variants = 4, include_warmup = F, prefix = path_desman_results)
```


### Looking at the estimation of the error matrix

#### 1 variant: looks ok

```{r, fig.height=20, fig.width=20}
eta_trace(number_of_variants = 1, include_warmup = T, prefix = path_desman_results)
```

### 2 variants: mixing looks good too

```{r, fig.height=20, fig.width=20}
eta_trace(number_of_variants = 2, include_warmup = T, prefix = path_desman_results)
```

#### 4 variants: although we can see that the chains are not mixing, no striking problem with the error estimates

```{r, message=FALSE}
eta_trace(number_of_variants = 4, include_warmup = F, prefix = path_desman_results)
```

### Looking at the nucleotides

Here we count the number of As, Cs, Ts, Gs in the genome of each variant at each iteration. This diagnostic shows that the sequence of the nucleotides does not change along the iterations, highlighting the main problem with DESMAN -> the Gibbs sampler does not mix.

#### For 1 variant: the inferred genome does not change -> no mixing

```{r}
nucleotide_count_trace(number_of_variants = 1, include_warmup = T, prefix = path_desman_results)
```

#### For 2 variants: the inferred genome does not change -> no mixing

```{r}
nucleotide_count_trace(number_of_variants = 2, include_warmup = T, prefix = path_desman_results)
```

```{r}
variant_table = lapply(1:5, function(chain_id) {
    DesmanResultsAnalysis:::load_tau_one_chain(number_of_variants = 2, chain = chain_id, include_warmup = FALSE, prefix = path_desman_results)
  }) %>%
    DesmanResultsAnalysis:::which_variants_in_each_chain() %>%
    dplyr::rename(
      Variant = variant_id_in_chain,
      Sample_id = chain_id
    ) %>%
    dplyr::mutate(
      Variant = as.character(Variant),
      Sample_id = as.character(Sample_id)
    )

print(variant_table)

variant_table$variant_id_in_ref %>% unique()
```


```{r}
gamma_trace_with_variants_identified(number_of_variants = 2, prefix = path_desman_results, samples = 1:5, nchains = 5, include_warmup = FALSE)
```

### Exploring the relative abundances

#### Visualising the relative abundances for troubleshooting

There are many samples and variants, so it's better to plot only a subset of the samples.

```{r}
gamma_trace(number_of_variants = 2, prefix = path_desman_results, samples = c(1, 2))
```

By default, all variants are plotted, but when the number of variants starts to be large, it's possible to plot only a subset of them via a dedicated keyword

```{r}
gamma_trace(number_of_variants = 4, prefix = path_desman_results, samples = c(1, 2), variants = 1:3)
```


#### Summarising the relative abundances

We do not plot the relative abundances with only 1 variant, since they are trivially 1 everywhere. Here, you should choose the chain with the lowest deviance. 

```{r}
plot_relative_abundance(number_of_variants = 4, chain = 2, prefix = path_desman_results)
```

I did not include a function to choose this chain automatically, but it may be guessed by looking at the log likelihood trace plot. The deviance is related to minus the likelihood, so choosing the chain with the lowest deviance amounts to choosing the chain with the largest likelihood.

```{r, message=FALSE}
log_likelihood_trace(number_of_variants = 4, include_warmup = F, prefix = path_desman_results)
```

Here, chains 1 and 2 are probably the ones with the highest likelihood


# Exploring the Gibbs update step

```{r}
raw_count_table = read_csv("nathan_test/file.outsel_var.csv", col_names = TRUE) 
V = raw_count_table %>% nrow()
S = raw_count_table %>% ncol() %>% (function(x){(x-2)/4})
n_vsa = array(data = 0, dim = c(V, S, 4))
for (v in seq(V)){
  n_vsa_for_v = raw_count_table[v,3:(4*S+2)] %>% matrix(ncol = 4, byrow = TRUE)
  for (s in seq(S)){
    for (a in seq(4)){
      n_vsa[v,s,a] = n_vsa_for_v[[s,a]]
    }
  }
}

G = 2

pi_hat = read_csv("nathan_test/desman.2.2/Gamma_star.csv") %>% 
  .[,2:3] %>% 
  t

tau_hat_csv = read_csv("nathan_test/desman.2.2/Filtered_Tau_star.csv")

tau_hat = array(data = 0, dim = c(V, G, 4))
for (v in seq(V)){
  tau_vga_for_v = tau_hat_csv[v,3:(4*G+2)] %>% matrix(ncol = 4, byrow = TRUE)
  for (g in seq(G)){
    for (a in seq(4)){
      tau_hat[v,g,a] = tau_vga_for_v[[g,a]]
    }
  }
}

error_prob = 0.1/4
eps = 1/(1+4*error_prob) * (diag(nrow = 4) + matrix(data = error_prob, nrow = 4, ncol = 4))

compute_logp_vga_unnormalised = function(v, tau_v, pi_hat, n_vsa, eps){
   tau_eps = tau_v %*% eps
   # G = nrow(tau_v)
   S = dim(pi_hat)[2]
   logprob = 0
   for (s in seq(S)){
     for (bprime in seq(4)){
       p_sbprime = sum(as.numeric(pi_hat[,s]) * tau_eps[,bprime])
       logprob = logprob + n_vsa[v, s, bprime] * log(p_sbprime)
     }
   }
   return(logprob)
}

compute_p_vga = function(v, g, tau_hat, pi_hat, n_vsa, eps){
  
  logp_vga_unnormalised = sapply(seq(4), function(a){
    tau_vga = tau_hat[v,,]
    tau_vga[g,] = 0
    tau_vga[g,a] = 1
    compute_logp_vga_unnormalised(v = v, tau_v = tau_vga, pi_hat = pi_hat, n_vsa = n_vsa, eps = eps)
  })

  exp(logp_vga_unnormalised - matrixStats::logSumExp(logp_vga_unnormalised)) %>% 
    return()
}

compute_p_vga(1, 1, tau_hat, pi_hat, n_vsa, eps)
compute_p_vga(1, 2, tau_hat, pi_hat, n_vsa, eps)
```
```{r}
v = 1
print(n_vsa[v,,])
```

```{r}
error_probs = seq(0.65, 0.75, length.out = 50)

lapply(error_probs, function(error_prob){
  eps = (diag(x = 1-4/3*error_prob, nrow = 4) + matrix(data = error_prob/3, nrow = 4, ncol = 4))
  tibble(error_prob = error_prob, 
         nucleotide = c("A", "C", "G", "T"),
         p = compute_p_vga(v, 1, tau_hat, pi_hat, n_vsa, eps)) 
}) %>% 
  bind_rows() %>% 
  ggplot(aes(x = nucleotide, y = p, colour = error_prob)) + 
  theme_bw() + 
  geom_point() + 
  scale_colour_viridis_c()
```

```{r}
v = 2
print(n_vsa[v,,])
```

```{r}
error_probs = seq(0.65, 0.75, length.out = 50)

lapply(error_probs, function(error_prob){
  eps = (diag(x = 1-4/3*error_prob, nrow = 4) + matrix(data = error_prob/3, nrow = 4, ncol = 4))
  tibble(error_prob = error_prob, 
         nucleotide = c("A", "C", "G", "T"),
         p = compute_p_vga(v, 1, tau_hat, pi_hat, n_vsa, eps)) 
}) %>% 
  bind_rows() %>% 
  ggplot(aes(x = nucleotide, y = p, colour = error_prob)) + 
  theme_bw() + 
  geom_point() + 
  scale_colour_viridis_c()
```
