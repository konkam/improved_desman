2022-12-21 10:38:05,213:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.2.4
2022-12-21 10:38:05,214:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 10:38:05,247:INFO:root:Running Desman with 9 samples and 94 variant positions finding 2 genomes.
2022-12-21 10:38:05,247:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 10:38:05,248:INFO:root:Set second adjustable random seed = 4
2022-12-21 10:38:05,249:INFO:root:Perform NTF initialisation
2022-12-21 10:38:05,251:INFO:root:NTF Iter 0, div = 317.992336
2022-12-21 10:38:05,348:INFO:root:NTF Iter 100, div = 37.361989
2022-12-21 10:38:05,445:INFO:root:NTF Iter 200, div = 36.814399
2022-12-21 10:38:05,542:INFO:root:NTF Iter 300, div = 36.052609
2022-12-21 10:38:05,637:INFO:root:NTF Iter 400, div = 34.623876
2022-12-21 10:38:05,732:INFO:root:NTF Iter 500, div = 30.873295
2022-12-21 10:38:05,826:INFO:root:NTF Iter 600, div = 27.767930
2022-12-21 10:38:05,920:INFO:root:NTF Iter 700, div = 25.496644
2022-12-21 10:38:06,014:INFO:root:NTF Iter 800, div = 23.970178
2022-12-21 10:38:06,108:INFO:root:NTF Iter 900, div = 23.482555
2022-12-21 10:38:06,200:INFO:root:NTF Iter 1000, div = 23.278383
2022-12-21 10:38:06,292:INFO:root:NTF Iter 1100, div = 23.198963
2022-12-21 10:38:06,387:INFO:root:NTF Iter 1200, div = 23.173511
2022-12-21 10:38:06,484:INFO:root:NTF Iter 1300, div = 23.091759
2022-12-21 10:38:06,580:INFO:root:NTF Iter 1400, div = 23.066546
2022-12-21 10:38:06,673:INFO:root:NTF Iter 1500, div = 23.032695
2022-12-21 10:38:06,765:INFO:root:NTF Iter 1600, div = 23.003526
2022-12-21 10:38:06,856:INFO:root:NTF Iter 1700, div = 22.985315
2022-12-21 10:38:06,950:INFO:root:NTF Iter 1800, div = 22.967227
2022-12-21 10:38:07,029:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 10:38:07,148:INFO:root:Gibbs Iter 0, no. changed = 29, nlp = -2059.214849
2022-12-21 10:38:08,126:INFO:root:Gibbs Iter 10, no. changed = 28, nlp = -2023.341677
2022-12-21 10:38:09,074:INFO:root:Gibbs Iter 20, no. changed = 27, nlp = -2050.423138
2022-12-21 10:38:10,022:INFO:root:Gibbs Iter 30, no. changed = 27, nlp = -2040.422053
2022-12-21 10:38:10,989:INFO:root:Gibbs Iter 40, no. changed = 26, nlp = -2044.676507
2022-12-21 10:38:11,955:INFO:root:Gibbs Iter 50, no. changed = 27, nlp = -2061.359465
2022-12-21 10:38:12,942:INFO:root:Gibbs Iter 60, no. changed = 26, nlp = -2029.078737
2022-12-21 10:38:13,887:INFO:root:Gibbs Iter 70, no. changed = 24, nlp = -2049.227171
2022-12-21 10:38:14,835:INFO:root:Gibbs Iter 80, no. changed = 26, nlp = -2033.825231
2022-12-21 10:38:15,783:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2053.606599
2022-12-21 10:38:16,774:INFO:root:Gibbs Iter 100, no. changed = 26, nlp = -2021.732600
2022-12-21 10:38:17,753:INFO:root:Gibbs Iter 110, no. changed = 28, nlp = -2042.509754
2022-12-21 10:38:18,718:INFO:root:Gibbs Iter 120, no. changed = 27, nlp = -2043.660524
2022-12-21 10:38:19,707:INFO:root:Gibbs Iter 130, no. changed = 26, nlp = -2032.154684
2022-12-21 10:38:20,671:INFO:root:Gibbs Iter 140, no. changed = 26, nlp = -2023.284522
2022-12-21 10:38:21,656:INFO:root:Gibbs Iter 150, no. changed = 25, nlp = -2032.760148
2022-12-21 10:38:22,652:INFO:root:Gibbs Iter 160, no. changed = 27, nlp = -2044.105321
2022-12-21 10:38:23,626:INFO:root:Gibbs Iter 170, no. changed = 27, nlp = -2043.154193
2022-12-21 10:38:24,597:INFO:root:Gibbs Iter 180, no. changed = 28, nlp = -2009.283095
2022-12-21 10:38:25,578:INFO:root:Gibbs Iter 190, no. changed = 26, nlp = -2001.680264
2022-12-21 10:38:26,556:INFO:root:Gibbs Iter 200, no. changed = 26, nlp = -2042.341100
2022-12-21 10:38:27,538:INFO:root:Gibbs Iter 210, no. changed = 27, nlp = -2019.997888
2022-12-21 10:38:28,506:INFO:root:Gibbs Iter 220, no. changed = 26, nlp = -2049.071979
2022-12-21 10:38:29,471:INFO:root:Gibbs Iter 230, no. changed = 28, nlp = -2031.964861
2022-12-21 10:38:30,428:INFO:root:Gibbs Iter 240, no. changed = 28, nlp = -2048.749265
2022-12-21 10:38:31,374:INFO:root:Gibbs Iter 250, no. changed = 27, nlp = -2041.154675
2022-12-21 10:38:32,364:INFO:root:Gibbs Iter 260, no. changed = 28, nlp = -2038.004369
2022-12-21 10:38:33,308:INFO:root:Gibbs Iter 270, no. changed = 27, nlp = -2046.526095
2022-12-21 10:38:34,275:INFO:root:Gibbs Iter 280, no. changed = 26, nlp = -2053.289454
2022-12-21 10:38:35,264:INFO:root:Gibbs Iter 290, no. changed = 27, nlp = -2037.499751
2022-12-21 10:38:36,260:INFO:root:Gibbs Iter 300, no. changed = 27, nlp = -2051.603286
2022-12-21 10:38:37,233:INFO:root:Gibbs Iter 310, no. changed = 25, nlp = -2053.974900
2022-12-21 10:38:38,211:INFO:root:Gibbs Iter 320, no. changed = 26, nlp = -2051.907376
2022-12-21 10:38:39,183:INFO:root:Gibbs Iter 330, no. changed = 27, nlp = -2049.452609
2022-12-21 10:38:40,179:INFO:root:Gibbs Iter 340, no. changed = 27, nlp = -2056.316684
2022-12-21 10:38:41,128:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -1997.208532
2022-12-21 10:38:42,073:INFO:root:Gibbs Iter 360, no. changed = 28, nlp = -2028.579492
2022-12-21 10:38:43,021:INFO:root:Gibbs Iter 370, no. changed = 26, nlp = -2047.207502
2022-12-21 10:38:43,966:INFO:root:Gibbs Iter 380, no. changed = 27, nlp = -2058.559324
2022-12-21 10:38:44,907:INFO:root:Gibbs Iter 390, no. changed = 26, nlp = -2054.353617
2022-12-21 10:38:45,852:INFO:root:Gibbs Iter 400, no. changed = 25, nlp = -2046.631748
2022-12-21 10:38:46,803:INFO:root:Gibbs Iter 410, no. changed = 25, nlp = -2034.471831
2022-12-21 10:38:47,756:INFO:root:Gibbs Iter 420, no. changed = 26, nlp = -2040.895089
2022-12-21 10:38:48,707:INFO:root:Gibbs Iter 430, no. changed = 28, nlp = -2035.174181
2022-12-21 10:38:49,663:INFO:root:Gibbs Iter 440, no. changed = 27, nlp = -2050.135257
2022-12-21 10:38:50,611:INFO:root:Gibbs Iter 450, no. changed = 28, nlp = -2023.927564
2022-12-21 10:38:51,556:INFO:root:Gibbs Iter 460, no. changed = 26, nlp = -2045.689893
2022-12-21 10:38:52,511:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2043.426929
2022-12-21 10:38:53,504:INFO:root:Gibbs Iter 480, no. changed = 26, nlp = -2024.553484
2022-12-21 10:38:54,490:INFO:root:Gibbs Iter 490, no. changed = 26, nlp = -2044.418363
2022-12-21 10:38:55,471:INFO:root:Gibbs Iter 500, no. changed = 24, nlp = -2064.809118
2022-12-21 10:38:56,463:INFO:root:Gibbs Iter 510, no. changed = 25, nlp = -2040.621641
2022-12-21 10:38:57,422:INFO:root:Gibbs Iter 520, no. changed = 26, nlp = -2054.655310
2022-12-21 10:38:58,417:INFO:root:Gibbs Iter 530, no. changed = 24, nlp = -2041.793660
2022-12-21 10:38:59,417:INFO:root:Gibbs Iter 540, no. changed = 27, nlp = -2039.228891
2022-12-21 10:39:00,410:INFO:root:Gibbs Iter 550, no. changed = 26, nlp = -2055.862311
2022-12-21 10:39:01,413:INFO:root:Gibbs Iter 560, no. changed = 27, nlp = -2045.517532
2022-12-21 10:39:02,398:INFO:root:Gibbs Iter 570, no. changed = 26, nlp = -2057.826488
2022-12-21 10:39:03,395:INFO:root:Gibbs Iter 580, no. changed = 27, nlp = -2047.919778
2022-12-21 10:39:04,386:INFO:root:Gibbs Iter 590, no. changed = 29, nlp = -2048.339522
2022-12-21 10:39:05,390:INFO:root:Gibbs Iter 600, no. changed = 28, nlp = -2016.555027
2022-12-21 10:39:06,387:INFO:root:Gibbs Iter 610, no. changed = 25, nlp = -2039.691194
2022-12-21 10:39:07,386:INFO:root:Gibbs Iter 620, no. changed = 27, nlp = -2051.827951
2022-12-21 10:39:08,384:INFO:root:Gibbs Iter 630, no. changed = 26, nlp = -2038.249684
2022-12-21 10:39:09,377:INFO:root:Gibbs Iter 640, no. changed = 25, nlp = -2055.414188
2022-12-21 10:39:10,375:INFO:root:Gibbs Iter 650, no. changed = 28, nlp = -2038.504592
2022-12-21 10:39:11,374:INFO:root:Gibbs Iter 660, no. changed = 27, nlp = -2020.699354
2022-12-21 10:39:12,372:INFO:root:Gibbs Iter 670, no. changed = 27, nlp = -2059.485109
2022-12-21 10:39:13,357:INFO:root:Gibbs Iter 680, no. changed = 26, nlp = -2048.701095
2022-12-21 10:39:14,336:INFO:root:Gibbs Iter 690, no. changed = 27, nlp = -2020.682935
2022-12-21 10:39:15,287:INFO:root:Gibbs Iter 700, no. changed = 29, nlp = -2013.885611
2022-12-21 10:39:16,234:INFO:root:Gibbs Iter 710, no. changed = 27, nlp = -2052.373265
2022-12-21 10:39:17,179:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2032.308744
2022-12-21 10:39:18,132:INFO:root:Gibbs Iter 730, no. changed = 26, nlp = -2047.758283
2022-12-21 10:39:19,095:INFO:root:Gibbs Iter 740, no. changed = 26, nlp = -2051.894040
2022-12-21 10:39:20,069:INFO:root:Gibbs Iter 750, no. changed = 26, nlp = -2025.102825
2022-12-21 10:39:21,018:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2031.946026
2022-12-21 10:39:21,963:INFO:root:Gibbs Iter 770, no. changed = 27, nlp = -2062.885181
2022-12-21 10:39:22,909:INFO:root:Gibbs Iter 780, no. changed = 28, nlp = -2048.832757
2022-12-21 10:39:23,859:INFO:root:Gibbs Iter 790, no. changed = 26, nlp = -2012.787176
2022-12-21 10:39:24,841:INFO:root:Gibbs Iter 800, no. changed = 26, nlp = -2053.615830
2022-12-21 10:39:25,837:INFO:root:Gibbs Iter 810, no. changed = 27, nlp = -2045.054609
2022-12-21 10:39:26,827:INFO:root:Gibbs Iter 820, no. changed = 28, nlp = -2055.093617
2022-12-21 10:39:27,811:INFO:root:Gibbs Iter 830, no. changed = 27, nlp = -2037.677402
2022-12-21 10:39:28,775:INFO:root:Gibbs Iter 840, no. changed = 27, nlp = -2022.756541
2022-12-21 10:39:29,731:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2035.685556
2022-12-21 10:39:30,690:INFO:root:Gibbs Iter 860, no. changed = 28, nlp = -2042.555385
2022-12-21 10:39:31,642:INFO:root:Gibbs Iter 870, no. changed = 27, nlp = -2049.749370
2022-12-21 10:39:32,608:INFO:root:Gibbs Iter 880, no. changed = 27, nlp = -2054.880022
2022-12-21 10:39:33,578:INFO:root:Gibbs Iter 890, no. changed = 27, nlp = -2038.891104
2022-12-21 10:39:34,528:INFO:root:Gibbs Iter 900, no. changed = 25, nlp = -2023.477177
2022-12-21 10:39:35,481:INFO:root:Gibbs Iter 910, no. changed = 26, nlp = -2014.662816
2022-12-21 10:39:36,434:INFO:root:Gibbs Iter 920, no. changed = 27, nlp = -2054.068537
2022-12-21 10:39:37,420:INFO:root:Gibbs Iter 930, no. changed = 28, nlp = -2033.211462
2022-12-21 10:39:38,419:INFO:root:Gibbs Iter 940, no. changed = 26, nlp = -2019.689149
2022-12-21 10:39:39,413:INFO:root:Gibbs Iter 950, no. changed = 25, nlp = -2049.687714
2022-12-21 10:39:40,383:INFO:root:Gibbs Iter 960, no. changed = 27, nlp = -2048.330625
2022-12-21 10:39:41,371:INFO:root:Gibbs Iter 970, no. changed = 28, nlp = -2028.391831
2022-12-21 10:39:42,369:INFO:root:Gibbs Iter 980, no. changed = 28, nlp = -2040.086084
2022-12-21 10:39:43,368:INFO:root:Gibbs Iter 990, no. changed = 27, nlp = -2047.363402
2022-12-21 10:39:44,282:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 10:39:44,402:INFO:root:Gibbs Iter 0, no. changed = 27, nlp = -2036.661559
2022-12-21 10:39:45,367:INFO:root:Gibbs Iter 10, no. changed = 25, nlp = -2054.642931
2022-12-21 10:39:46,318:INFO:root:Gibbs Iter 20, no. changed = 26, nlp = -2045.701341
2022-12-21 10:39:47,268:INFO:root:Gibbs Iter 30, no. changed = 26, nlp = -2042.200786
2022-12-21 10:39:48,218:INFO:root:Gibbs Iter 40, no. changed = 27, nlp = -2025.253384
2022-12-21 10:39:49,170:INFO:root:Gibbs Iter 50, no. changed = 26, nlp = -2053.267143
2022-12-21 10:39:50,122:INFO:root:Gibbs Iter 60, no. changed = 27, nlp = -2026.014278
2022-12-21 10:39:51,067:INFO:root:Gibbs Iter 70, no. changed = 27, nlp = -2039.530387
2022-12-21 10:39:52,022:INFO:root:Gibbs Iter 80, no. changed = 27, nlp = -1994.159321
2022-12-21 10:39:52,972:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2019.120326
2022-12-21 10:39:53,924:INFO:root:Gibbs Iter 100, no. changed = 27, nlp = -2011.715195
2022-12-21 10:39:54,896:INFO:root:Gibbs Iter 110, no. changed = 26, nlp = -2052.128895
2022-12-21 10:39:55,870:INFO:root:Gibbs Iter 120, no. changed = 28, nlp = -2046.667337
2022-12-21 10:39:56,814:INFO:root:Gibbs Iter 130, no. changed = 28, nlp = -2043.566274
2022-12-21 10:39:57,753:INFO:root:Gibbs Iter 140, no. changed = 26, nlp = -2029.161035
2022-12-21 10:39:58,696:INFO:root:Gibbs Iter 150, no. changed = 28, nlp = -2043.407544
2022-12-21 10:39:59,663:INFO:root:Gibbs Iter 160, no. changed = 26, nlp = -2041.787363
2022-12-21 10:40:00,602:INFO:root:Gibbs Iter 170, no. changed = 28, nlp = -2047.546334
2022-12-21 10:40:01,552:INFO:root:Gibbs Iter 180, no. changed = 26, nlp = -2054.380150
2022-12-21 10:40:02,547:INFO:root:Gibbs Iter 190, no. changed = 27, nlp = -2056.348300
2022-12-21 10:40:03,520:INFO:root:Gibbs Iter 200, no. changed = 26, nlp = -2035.698120
2022-12-21 10:40:04,497:INFO:root:Gibbs Iter 210, no. changed = 27, nlp = -2016.955689
2022-12-21 10:40:05,480:INFO:root:Gibbs Iter 220, no. changed = 28, nlp = -2062.424531
2022-12-21 10:40:06,452:INFO:root:Gibbs Iter 230, no. changed = 26, nlp = -2055.759852
2022-12-21 10:40:07,432:INFO:root:Gibbs Iter 240, no. changed = 24, nlp = -2056.978648
2022-12-21 10:40:08,414:INFO:root:Gibbs Iter 250, no. changed = 26, nlp = -2055.986427
2022-12-21 10:40:09,399:INFO:root:Gibbs Iter 260, no. changed = 28, nlp = -2038.913963
2022-12-21 10:40:10,363:INFO:root:Gibbs Iter 270, no. changed = 27, nlp = -2040.214074
2022-12-21 10:40:11,305:INFO:root:Gibbs Iter 280, no. changed = 28, nlp = -2053.557650
2022-12-21 10:40:12,247:INFO:root:Gibbs Iter 290, no. changed = 27, nlp = -2001.123139
2022-12-21 10:40:13,194:INFO:root:Gibbs Iter 300, no. changed = 28, nlp = -2054.856665
2022-12-21 10:40:14,179:INFO:root:Gibbs Iter 310, no. changed = 26, nlp = -2017.356254
2022-12-21 10:40:15,159:INFO:root:Gibbs Iter 320, no. changed = 27, nlp = -2058.973439
2022-12-21 10:40:16,102:INFO:root:Gibbs Iter 330, no. changed = 28, nlp = -2041.591642
2022-12-21 10:40:17,063:INFO:root:Gibbs Iter 340, no. changed = 27, nlp = -2054.413053
2022-12-21 10:40:18,057:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -2029.356943
2022-12-21 10:40:19,038:INFO:root:Gibbs Iter 360, no. changed = 26, nlp = -2050.808947
2022-12-21 10:40:20,015:INFO:root:Gibbs Iter 370, no. changed = 26, nlp = -2046.880522
2022-12-21 10:40:20,965:INFO:root:Gibbs Iter 380, no. changed = 27, nlp = -2063.613203
2022-12-21 10:40:21,907:INFO:root:Gibbs Iter 390, no. changed = 26, nlp = -2048.521324
2022-12-21 10:40:22,846:INFO:root:Gibbs Iter 400, no. changed = 27, nlp = -2048.050606
2022-12-21 10:40:23,836:INFO:root:Gibbs Iter 410, no. changed = 26, nlp = -2041.736007
2022-12-21 10:40:24,803:INFO:root:Gibbs Iter 420, no. changed = 25, nlp = -2041.681087
2022-12-21 10:40:25,768:INFO:root:Gibbs Iter 430, no. changed = 27, nlp = -2060.721712
2022-12-21 10:40:26,753:INFO:root:Gibbs Iter 440, no. changed = 28, nlp = -2026.941018
2022-12-21 10:40:27,743:INFO:root:Gibbs Iter 450, no. changed = 27, nlp = -2010.783815
2022-12-21 10:40:28,723:INFO:root:Gibbs Iter 460, no. changed = 27, nlp = -2046.858007
2022-12-21 10:40:29,688:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2066.299806
2022-12-21 10:40:30,640:INFO:root:Gibbs Iter 480, no. changed = 26, nlp = -2057.381039
2022-12-21 10:40:31,591:INFO:root:Gibbs Iter 490, no. changed = 27, nlp = -2038.791044
2022-12-21 10:40:32,566:INFO:root:Gibbs Iter 500, no. changed = 24, nlp = -2048.742903
2022-12-21 10:40:33,541:INFO:root:Gibbs Iter 510, no. changed = 27, nlp = -2054.277868
2022-12-21 10:40:34,522:INFO:root:Gibbs Iter 520, no. changed = 27, nlp = -2014.353627
2022-12-21 10:40:35,460:INFO:root:Gibbs Iter 530, no. changed = 27, nlp = -2026.715196
2022-12-21 10:40:36,401:INFO:root:Gibbs Iter 540, no. changed = 26, nlp = -2059.147875
2022-12-21 10:40:37,345:INFO:root:Gibbs Iter 550, no. changed = 26, nlp = -2043.587185
2022-12-21 10:40:38,288:INFO:root:Gibbs Iter 560, no. changed = 25, nlp = -2056.291459
2022-12-21 10:40:39,235:INFO:root:Gibbs Iter 570, no. changed = 26, nlp = -2054.795224
2022-12-21 10:40:40,182:INFO:root:Gibbs Iter 580, no. changed = 26, nlp = -2061.292136
2022-12-21 10:40:41,123:INFO:root:Gibbs Iter 590, no. changed = 25, nlp = -2055.641752
2022-12-21 10:40:42,065:INFO:root:Gibbs Iter 600, no. changed = 27, nlp = -2036.654382
2022-12-21 10:40:43,012:INFO:root:Gibbs Iter 610, no. changed = 27, nlp = -2060.052550
2022-12-21 10:40:43,986:INFO:root:Gibbs Iter 620, no. changed = 25, nlp = -2042.036183
2022-12-21 10:40:44,973:INFO:root:Gibbs Iter 630, no. changed = 27, nlp = -2047.150666
2022-12-21 10:40:45,953:INFO:root:Gibbs Iter 640, no. changed = 28, nlp = -2030.306424
2022-12-21 10:40:46,898:INFO:root:Gibbs Iter 650, no. changed = 27, nlp = -2056.782840
2022-12-21 10:40:47,839:INFO:root:Gibbs Iter 660, no. changed = 26, nlp = -2041.252496
2022-12-21 10:40:48,781:INFO:root:Gibbs Iter 670, no. changed = 26, nlp = -2036.084924
2022-12-21 10:40:49,726:INFO:root:Gibbs Iter 680, no. changed = 25, nlp = -2046.747741
2022-12-21 10:40:50,690:INFO:root:Gibbs Iter 690, no. changed = 27, nlp = -2047.646413
2022-12-21 10:40:51,683:INFO:root:Gibbs Iter 700, no. changed = 26, nlp = -2044.033764
2022-12-21 10:40:52,673:INFO:root:Gibbs Iter 710, no. changed = 27, nlp = -2051.494826
2022-12-21 10:40:53,615:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2048.654323
2022-12-21 10:40:54,556:INFO:root:Gibbs Iter 730, no. changed = 27, nlp = -2039.381039
2022-12-21 10:40:55,502:INFO:root:Gibbs Iter 740, no. changed = 28, nlp = -2048.330395
2022-12-21 10:40:56,448:INFO:root:Gibbs Iter 750, no. changed = 27, nlp = -2029.381151
2022-12-21 10:40:57,387:INFO:root:Gibbs Iter 760, no. changed = 28, nlp = -2037.765960
2022-12-21 10:40:58,329:INFO:root:Gibbs Iter 770, no. changed = 27, nlp = -2061.251496
2022-12-21 10:40:59,272:INFO:root:Gibbs Iter 780, no. changed = 26, nlp = -2042.130302
2022-12-21 10:41:00,216:INFO:root:Gibbs Iter 790, no. changed = 26, nlp = -2053.329885
2022-12-21 10:41:01,199:INFO:root:Gibbs Iter 800, no. changed = 28, nlp = -2054.134843
2022-12-21 10:41:02,185:INFO:root:Gibbs Iter 810, no. changed = 28, nlp = -2054.739568
2022-12-21 10:41:03,176:INFO:root:Gibbs Iter 820, no. changed = 25, nlp = -2052.253610
2022-12-21 10:41:04,169:INFO:root:Gibbs Iter 830, no. changed = 27, nlp = -2031.333882
2022-12-21 10:41:05,158:INFO:root:Gibbs Iter 840, no. changed = 26, nlp = -2002.346229
2022-12-21 10:41:06,119:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2041.795737
2022-12-21 10:41:07,065:INFO:root:Gibbs Iter 860, no. changed = 27, nlp = -2037.730785
2022-12-21 10:41:08,022:INFO:root:Gibbs Iter 870, no. changed = 28, nlp = -2043.643097
2022-12-21 10:41:08,974:INFO:root:Gibbs Iter 880, no. changed = 28, nlp = -2045.659281
2022-12-21 10:41:09,917:INFO:root:Gibbs Iter 890, no. changed = 27, nlp = -2053.643404
2022-12-21 10:41:10,865:INFO:root:Gibbs Iter 900, no. changed = 27, nlp = -2057.696510
2022-12-21 10:41:11,835:INFO:root:Gibbs Iter 910, no. changed = 27, nlp = -2057.971586
2022-12-21 10:41:12,822:INFO:root:Gibbs Iter 920, no. changed = 26, nlp = -2027.451848
2022-12-21 10:41:13,812:INFO:root:Gibbs Iter 930, no. changed = 26, nlp = -2029.229603
2022-12-21 10:41:14,776:INFO:root:Gibbs Iter 940, no. changed = 27, nlp = -2047.180099
2022-12-21 10:41:15,719:INFO:root:Gibbs Iter 950, no. changed = 27, nlp = -2050.924203
2022-12-21 10:41:16,683:INFO:root:Gibbs Iter 960, no. changed = 27, nlp = -2045.232788
2022-12-21 10:41:17,625:INFO:root:Gibbs Iter 970, no. changed = 27, nlp = -2034.833015
2022-12-21 10:41:18,567:INFO:root:Gibbs Iter 980, no. changed = 26, nlp = -2051.398576
2022-12-21 10:41:19,517:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2034.001293
2022-12-21 10:41:20,416:INFO:root:Wrote fit stats
2022-12-21 10:41:20,422:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 10:41:20,428:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 10:41:20,431:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 10:41:20,475:INFO:root:Wrote ll_store
2022-12-21 10:41:20,479:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 10:41:20,482:INFO:root:Wrote transition error matrix
2022-12-21 10:41:20,485:INFO:root:Wrote transition error matrix
2022-12-21 10:41:20,488:INFO:root:Wrote selected variants
