2022-12-21 11:01:50,413:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.4.1
2022-12-21 11:01:50,413:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 11:01:50,447:INFO:root:Running Desman with 9 samples and 94 variant positions finding 4 genomes.
2022-12-21 11:01:50,447:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 11:01:50,448:INFO:root:Set second adjustable random seed = 1
2022-12-21 11:01:50,449:INFO:root:Perform NTF initialisation
2022-12-21 11:01:50,453:INFO:root:NTF Iter 0, div = 634.467623
2022-12-21 11:01:50,599:INFO:root:NTF Iter 100, div = 34.965121
2022-12-21 11:01:50,744:INFO:root:NTF Iter 200, div = 31.854142
2022-12-21 11:01:50,888:INFO:root:NTF Iter 300, div = 27.378596
2022-12-21 11:01:51,032:INFO:root:NTF Iter 400, div = 23.657521
2022-12-21 11:01:51,177:INFO:root:NTF Iter 500, div = 20.346127
2022-12-21 11:01:51,323:INFO:root:NTF Iter 600, div = 18.183897
2022-12-21 11:01:51,468:INFO:root:NTF Iter 700, div = 16.661219
2022-12-21 11:01:51,613:INFO:root:NTF Iter 800, div = 15.520922
2022-12-21 11:01:51,757:INFO:root:NTF Iter 900, div = 15.092886
2022-12-21 11:01:51,901:INFO:root:NTF Iter 1000, div = 14.717115
2022-12-21 11:01:52,046:INFO:root:NTF Iter 1100, div = 14.352640
2022-12-21 11:01:52,189:INFO:root:NTF Iter 1200, div = 14.012048
2022-12-21 11:01:52,333:INFO:root:NTF Iter 1300, div = 13.634107
2022-12-21 11:01:52,478:INFO:root:NTF Iter 1400, div = 13.210593
2022-12-21 11:01:52,624:INFO:root:NTF Iter 1500, div = 12.756070
2022-12-21 11:01:52,768:INFO:root:NTF Iter 1600, div = 12.456151
2022-12-21 11:01:52,911:INFO:root:NTF Iter 1700, div = 12.290924
2022-12-21 11:01:53,055:INFO:root:NTF Iter 1800, div = 12.175625
2022-12-21 11:01:53,201:INFO:root:NTF Iter 1900, div = 12.066454
2022-12-21 11:01:53,345:INFO:root:NTF Iter 2000, div = 11.948235
2022-12-21 11:01:53,489:INFO:root:NTF Iter 2100, div = 11.828714
2022-12-21 11:01:53,632:INFO:root:NTF Iter 2200, div = 11.697290
2022-12-21 11:01:53,776:INFO:root:NTF Iter 2300, div = 11.654291
2022-12-21 11:01:53,922:INFO:root:NTF Iter 2400, div = 11.607623
2022-12-21 11:01:54,066:INFO:root:NTF Iter 2500, div = 11.573642
2022-12-21 11:01:54,210:INFO:root:NTF Iter 2600, div = 11.548542
2022-12-21 11:01:54,353:INFO:root:NTF Iter 2700, div = 11.529366
2022-12-21 11:01:54,496:INFO:root:NTF Iter 2800, div = 11.524999
2022-12-21 11:01:54,642:INFO:root:NTF Iter 2900, div = 11.521433
2022-12-21 11:01:54,786:INFO:root:NTF Iter 3000, div = 11.508434
2022-12-21 11:01:54,930:INFO:root:NTF Iter 3100, div = 11.497805
2022-12-21 11:01:55,073:INFO:root:NTF Iter 3200, div = 11.480400
2022-12-21 11:01:55,217:INFO:root:NTF Iter 3300, div = 11.475260
2022-12-21 11:01:55,362:INFO:root:NTF Iter 3400, div = 11.472471
2022-12-21 11:01:55,506:INFO:root:NTF Iter 3500, div = 11.470172
2022-12-21 11:01:55,650:INFO:root:NTF Iter 3600, div = 11.466567
2022-12-21 11:01:55,793:INFO:root:NTF Iter 3700, div = 11.460160
2022-12-21 11:01:55,936:INFO:root:NTF Iter 3800, div = 11.453186
2022-12-21 11:01:56,084:INFO:root:NTF Iter 3900, div = 11.449432
2022-12-21 11:01:56,228:INFO:root:NTF Iter 4000, div = 11.444054
2022-12-21 11:01:56,372:INFO:root:NTF Iter 4100, div = 11.439783
2022-12-21 11:01:56,515:INFO:root:NTF Iter 4200, div = 11.436840
2022-12-21 11:01:56,660:INFO:root:NTF Iter 4300, div = 11.435224
2022-12-21 11:01:56,806:INFO:root:NTF Iter 4400, div = 11.433755
2022-12-21 11:01:56,903:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 11:01:57,020:INFO:root:Gibbs Iter 0, no. changed = 57, nlp = -2085.199162
2022-12-21 11:01:57,991:INFO:root:Gibbs Iter 10, no. changed = 36, nlp = -2028.076878
2022-12-21 11:01:58,962:INFO:root:Gibbs Iter 20, no. changed = 29, nlp = -1948.181824
2022-12-21 11:01:59,932:INFO:root:Gibbs Iter 30, no. changed = 30, nlp = -1923.578115
2022-12-21 11:02:00,905:INFO:root:Gibbs Iter 40, no. changed = 38, nlp = -1932.468120
2022-12-21 11:02:01,878:INFO:root:Gibbs Iter 50, no. changed = 33, nlp = -1937.677937
2022-12-21 11:02:02,852:INFO:root:Gibbs Iter 60, no. changed = 30, nlp = -1926.699441
2022-12-21 11:02:03,823:INFO:root:Gibbs Iter 70, no. changed = 36, nlp = -1931.508044
2022-12-21 11:02:04,795:INFO:root:Gibbs Iter 80, no. changed = 32, nlp = -1901.851526
2022-12-21 11:02:05,768:INFO:root:Gibbs Iter 90, no. changed = 39, nlp = -1892.665093
2022-12-21 11:02:06,740:INFO:root:Gibbs Iter 100, no. changed = 40, nlp = -1903.009776
2022-12-21 11:02:07,709:INFO:root:Gibbs Iter 110, no. changed = 33, nlp = -1953.198979
2022-12-21 11:02:08,680:INFO:root:Gibbs Iter 120, no. changed = 39, nlp = -1935.536103
2022-12-21 11:02:09,648:INFO:root:Gibbs Iter 130, no. changed = 37, nlp = -1937.506036
2022-12-21 11:02:10,616:INFO:root:Gibbs Iter 140, no. changed = 33, nlp = -1928.470210
2022-12-21 11:02:11,584:INFO:root:Gibbs Iter 150, no. changed = 29, nlp = -1937.378349
2022-12-21 11:02:12,551:INFO:root:Gibbs Iter 160, no. changed = 29, nlp = -1972.204668
2022-12-21 11:02:13,519:INFO:root:Gibbs Iter 170, no. changed = 31, nlp = -1946.566730
2022-12-21 11:02:14,486:INFO:root:Gibbs Iter 180, no. changed = 26, nlp = -1945.351096
2022-12-21 11:02:15,455:INFO:root:Gibbs Iter 190, no. changed = 33, nlp = -1974.788139
2022-12-21 11:02:16,422:INFO:root:Gibbs Iter 200, no. changed = 33, nlp = -1946.994722
2022-12-21 11:02:17,393:INFO:root:Gibbs Iter 210, no. changed = 36, nlp = -1955.425768
2022-12-21 11:02:18,359:INFO:root:Gibbs Iter 220, no. changed = 35, nlp = -1940.609726
2022-12-21 11:02:19,326:INFO:root:Gibbs Iter 230, no. changed = 28, nlp = -1948.865834
2022-12-21 11:02:20,292:INFO:root:Gibbs Iter 240, no. changed = 31, nlp = -1959.454613
2022-12-21 11:02:21,259:INFO:root:Gibbs Iter 250, no. changed = 28, nlp = -1952.863494
2022-12-21 11:02:22,229:INFO:root:Gibbs Iter 260, no. changed = 39, nlp = -1932.626697
2022-12-21 11:02:23,198:INFO:root:Gibbs Iter 270, no. changed = 35, nlp = -1949.304168
2022-12-21 11:02:24,166:INFO:root:Gibbs Iter 280, no. changed = 31, nlp = -1921.615821
2022-12-21 11:02:25,134:INFO:root:Gibbs Iter 290, no. changed = 30, nlp = -1940.437796
2022-12-21 11:02:26,100:INFO:root:Gibbs Iter 300, no. changed = 30, nlp = -1976.546247
2022-12-21 11:02:27,068:INFO:root:Gibbs Iter 310, no. changed = 36, nlp = -1929.734891
2022-12-21 11:02:28,038:INFO:root:Gibbs Iter 320, no. changed = 30, nlp = -1960.365828
2022-12-21 11:02:29,007:INFO:root:Gibbs Iter 330, no. changed = 30, nlp = -1921.192837
2022-12-21 11:02:29,973:INFO:root:Gibbs Iter 340, no. changed = 33, nlp = -1938.127845
2022-12-21 11:02:30,941:INFO:root:Gibbs Iter 350, no. changed = 32, nlp = -1954.480838
2022-12-21 11:02:31,909:INFO:root:Gibbs Iter 360, no. changed = 33, nlp = -1906.235671
2022-12-21 11:02:32,877:INFO:root:Gibbs Iter 370, no. changed = 30, nlp = -1962.865424
2022-12-21 11:02:33,843:INFO:root:Gibbs Iter 380, no. changed = 36, nlp = -1945.199183
2022-12-21 11:02:34,810:INFO:root:Gibbs Iter 390, no. changed = 32, nlp = -1925.543833
2022-12-21 11:02:35,774:INFO:root:Gibbs Iter 400, no. changed = 31, nlp = -1959.457390
2022-12-21 11:02:36,742:INFO:root:Gibbs Iter 410, no. changed = 31, nlp = -1951.336654
2022-12-21 11:02:37,708:INFO:root:Gibbs Iter 420, no. changed = 32, nlp = -1965.165299
2022-12-21 11:02:38,676:INFO:root:Gibbs Iter 430, no. changed = 38, nlp = -1963.382349
2022-12-21 11:02:39,644:INFO:root:Gibbs Iter 440, no. changed = 32, nlp = -1940.922185
2022-12-21 11:02:40,612:INFO:root:Gibbs Iter 450, no. changed = 28, nlp = -1955.832884
2022-12-21 11:02:41,580:INFO:root:Gibbs Iter 460, no. changed = 33, nlp = -1960.837350
2022-12-21 11:02:42,548:INFO:root:Gibbs Iter 470, no. changed = 29, nlp = -1976.651536
2022-12-21 11:02:43,516:INFO:root:Gibbs Iter 480, no. changed = 30, nlp = -1961.546779
2022-12-21 11:02:44,486:INFO:root:Gibbs Iter 490, no. changed = 31, nlp = -1954.709183
2022-12-21 11:02:45,457:INFO:root:Gibbs Iter 500, no. changed = 33, nlp = -1948.001182
2022-12-21 11:02:46,428:INFO:root:Gibbs Iter 510, no. changed = 36, nlp = -1836.589356
2022-12-21 11:02:47,399:INFO:root:Gibbs Iter 520, no. changed = 34, nlp = -1902.531529
2022-12-21 11:02:48,367:INFO:root:Gibbs Iter 530, no. changed = 34, nlp = -1956.497053
2022-12-21 11:02:49,337:INFO:root:Gibbs Iter 540, no. changed = 32, nlp = -1933.877431
2022-12-21 11:02:50,308:INFO:root:Gibbs Iter 550, no. changed = 34, nlp = -1938.800061
2022-12-21 11:02:51,278:INFO:root:Gibbs Iter 560, no. changed = 39, nlp = -1966.277981
2022-12-21 11:02:52,247:INFO:root:Gibbs Iter 570, no. changed = 29, nlp = -1958.606439
2022-12-21 11:02:53,220:INFO:root:Gibbs Iter 580, no. changed = 29, nlp = -1952.106764
2022-12-21 11:02:54,188:INFO:root:Gibbs Iter 590, no. changed = 34, nlp = -1895.974001
2022-12-21 11:02:55,156:INFO:root:Gibbs Iter 600, no. changed = 32, nlp = -1935.082793
2022-12-21 11:02:56,122:INFO:root:Gibbs Iter 610, no. changed = 30, nlp = -1948.310683
2022-12-21 11:02:57,093:INFO:root:Gibbs Iter 620, no. changed = 28, nlp = -1914.984122
2022-12-21 11:02:58,063:INFO:root:Gibbs Iter 630, no. changed = 29, nlp = -1941.912242
2022-12-21 11:02:59,031:INFO:root:Gibbs Iter 640, no. changed = 31, nlp = -1983.099143
2022-12-21 11:02:59,999:INFO:root:Gibbs Iter 650, no. changed = 31, nlp = -1966.929544
2022-12-21 11:03:00,969:INFO:root:Gibbs Iter 660, no. changed = 39, nlp = -1959.104760
2022-12-21 11:03:01,939:INFO:root:Gibbs Iter 670, no. changed = 25, nlp = -1955.446965
2022-12-21 11:03:02,907:INFO:root:Gibbs Iter 680, no. changed = 33, nlp = -1968.402349
2022-12-21 11:03:03,877:INFO:root:Gibbs Iter 690, no. changed = 33, nlp = -1934.772129
2022-12-21 11:03:04,846:INFO:root:Gibbs Iter 700, no. changed = 31, nlp = -1935.518537
2022-12-21 11:03:05,813:INFO:root:Gibbs Iter 710, no. changed = 32, nlp = -1923.232927
2022-12-21 11:03:06,780:INFO:root:Gibbs Iter 720, no. changed = 35, nlp = -1948.385737
2022-12-21 11:03:07,750:INFO:root:Gibbs Iter 730, no. changed = 40, nlp = -1943.616499
2022-12-21 11:03:08,719:INFO:root:Gibbs Iter 740, no. changed = 33, nlp = -1945.113481
2022-12-21 11:03:09,688:INFO:root:Gibbs Iter 750, no. changed = 36, nlp = -1958.695088
2022-12-21 11:03:10,659:INFO:root:Gibbs Iter 760, no. changed = 31, nlp = -1948.605271
2022-12-21 11:03:11,631:INFO:root:Gibbs Iter 770, no. changed = 34, nlp = -1948.336632
2022-12-21 11:03:12,599:INFO:root:Gibbs Iter 780, no. changed = 32, nlp = -1953.664889
2022-12-21 11:03:13,569:INFO:root:Gibbs Iter 790, no. changed = 33, nlp = -1955.288395
2022-12-21 11:03:14,539:INFO:root:Gibbs Iter 800, no. changed = 37, nlp = -1964.819635
2022-12-21 11:03:15,509:INFO:root:Gibbs Iter 810, no. changed = 36, nlp = -1954.292936
2022-12-21 11:03:16,479:INFO:root:Gibbs Iter 820, no. changed = 36, nlp = -1966.514258
2022-12-21 11:03:17,447:INFO:root:Gibbs Iter 830, no. changed = 30, nlp = -1944.800150
2022-12-21 11:03:18,417:INFO:root:Gibbs Iter 840, no. changed = 27, nlp = -1976.073609
2022-12-21 11:03:19,387:INFO:root:Gibbs Iter 850, no. changed = 32, nlp = -1943.973566
2022-12-21 11:03:20,354:INFO:root:Gibbs Iter 860, no. changed = 32, nlp = -1970.951531
2022-12-21 11:03:21,319:INFO:root:Gibbs Iter 870, no. changed = 31, nlp = -1895.095649
2022-12-21 11:03:22,290:INFO:root:Gibbs Iter 880, no. changed = 26, nlp = -1918.672120
2022-12-21 11:03:23,256:INFO:root:Gibbs Iter 890, no. changed = 33, nlp = -1925.101888
2022-12-21 11:03:24,221:INFO:root:Gibbs Iter 900, no. changed = 32, nlp = -1940.662040
2022-12-21 11:03:25,188:INFO:root:Gibbs Iter 910, no. changed = 37, nlp = -1943.794952
2022-12-21 11:03:26,154:INFO:root:Gibbs Iter 920, no. changed = 33, nlp = -1951.403963
2022-12-21 11:03:27,121:INFO:root:Gibbs Iter 930, no. changed = 30, nlp = -1917.909088
2022-12-21 11:03:28,091:INFO:root:Gibbs Iter 940, no. changed = 31, nlp = -1935.960879
2022-12-21 11:03:29,056:INFO:root:Gibbs Iter 950, no. changed = 36, nlp = -1961.949494
2022-12-21 11:03:30,027:INFO:root:Gibbs Iter 960, no. changed = 31, nlp = -1947.110609
2022-12-21 11:03:30,995:INFO:root:Gibbs Iter 970, no. changed = 36, nlp = -1959.899930
2022-12-21 11:03:31,963:INFO:root:Gibbs Iter 980, no. changed = 26, nlp = -1932.300544
2022-12-21 11:03:32,923:INFO:root:Gibbs Iter 990, no. changed = 34, nlp = -1970.243950
2022-12-21 11:03:33,870:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 11:03:33,987:INFO:root:Gibbs Iter 0, no. changed = 36, nlp = -1943.488613
2022-12-21 11:03:34,950:INFO:root:Gibbs Iter 10, no. changed = 31, nlp = -1921.199409
2022-12-21 11:03:35,910:INFO:root:Gibbs Iter 20, no. changed = 30, nlp = -1982.507413
2022-12-21 11:03:36,871:INFO:root:Gibbs Iter 30, no. changed = 35, nlp = -1968.618605
2022-12-21 11:03:37,831:INFO:root:Gibbs Iter 40, no. changed = 35, nlp = -1960.295906
2022-12-21 11:03:38,793:INFO:root:Gibbs Iter 50, no. changed = 32, nlp = -1945.902638
2022-12-21 11:03:39,750:INFO:root:Gibbs Iter 60, no. changed = 27, nlp = -1947.236899
2022-12-21 11:03:40,708:INFO:root:Gibbs Iter 70, no. changed = 35, nlp = -1933.792093
2022-12-21 11:03:41,667:INFO:root:Gibbs Iter 80, no. changed = 28, nlp = -1955.067580
2022-12-21 11:03:42,628:INFO:root:Gibbs Iter 90, no. changed = 32, nlp = -1942.273686
2022-12-21 11:03:43,585:INFO:root:Gibbs Iter 100, no. changed = 32, nlp = -1943.016253
2022-12-21 11:03:44,541:INFO:root:Gibbs Iter 110, no. changed = 35, nlp = -1933.751052
2022-12-21 11:03:45,494:INFO:root:Gibbs Iter 120, no. changed = 30, nlp = -1919.409606
2022-12-21 11:03:46,455:INFO:root:Gibbs Iter 130, no. changed = 28, nlp = -1956.366694
2022-12-21 11:03:47,411:INFO:root:Gibbs Iter 140, no. changed = 28, nlp = -1893.970444
2022-12-21 11:03:48,366:INFO:root:Gibbs Iter 150, no. changed = 33, nlp = -1950.835123
2022-12-21 11:03:49,322:INFO:root:Gibbs Iter 160, no. changed = 30, nlp = -1934.922610
2022-12-21 11:03:50,282:INFO:root:Gibbs Iter 170, no. changed = 36, nlp = -1953.035725
2022-12-21 11:03:51,238:INFO:root:Gibbs Iter 180, no. changed = 28, nlp = -1963.743354
2022-12-21 11:03:52,195:INFO:root:Gibbs Iter 190, no. changed = 31, nlp = -1957.640663
2022-12-21 11:03:53,153:INFO:root:Gibbs Iter 200, no. changed = 26, nlp = -1936.255738
2022-12-21 11:03:54,114:INFO:root:Gibbs Iter 210, no. changed = 36, nlp = -1916.794813
2022-12-21 11:03:55,071:INFO:root:Gibbs Iter 220, no. changed = 27, nlp = -1955.865917
2022-12-21 11:03:56,027:INFO:root:Gibbs Iter 230, no. changed = 35, nlp = -1965.514675
2022-12-21 11:03:56,984:INFO:root:Gibbs Iter 240, no. changed = 34, nlp = -1941.326058
2022-12-21 11:03:57,943:INFO:root:Gibbs Iter 250, no. changed = 30, nlp = -1867.247394
2022-12-21 11:03:58,900:INFO:root:Gibbs Iter 260, no. changed = 32, nlp = -1917.659653
2022-12-21 11:03:59,850:INFO:root:Gibbs Iter 270, no. changed = 28, nlp = -1966.286338
2022-12-21 11:04:00,801:INFO:root:Gibbs Iter 280, no. changed = 30, nlp = -1946.678243
2022-12-21 11:04:01,760:INFO:root:Gibbs Iter 290, no. changed = 32, nlp = -1968.575507
2022-12-21 11:04:02,719:INFO:root:Gibbs Iter 300, no. changed = 38, nlp = -1968.907203
2022-12-21 11:04:03,676:INFO:root:Gibbs Iter 310, no. changed = 33, nlp = -1943.212816
2022-12-21 11:04:04,632:INFO:root:Gibbs Iter 320, no. changed = 32, nlp = -1921.374067
2022-12-21 11:04:05,591:INFO:root:Gibbs Iter 330, no. changed = 34, nlp = -1943.301809
2022-12-21 11:04:06,552:INFO:root:Gibbs Iter 340, no. changed = 31, nlp = -1902.109282
2022-12-21 11:04:07,510:INFO:root:Gibbs Iter 350, no. changed = 31, nlp = -1953.869779
2022-12-21 11:04:08,470:INFO:root:Gibbs Iter 360, no. changed = 33, nlp = -1958.400051
2022-12-21 11:04:09,429:INFO:root:Gibbs Iter 370, no. changed = 35, nlp = -1924.113524
2022-12-21 11:04:10,391:INFO:root:Gibbs Iter 380, no. changed = 25, nlp = -1931.349515
2022-12-21 11:04:11,350:INFO:root:Gibbs Iter 390, no. changed = 32, nlp = -1923.344492
2022-12-21 11:04:12,309:INFO:root:Gibbs Iter 400, no. changed = 30, nlp = -1936.349074
2022-12-21 11:04:13,266:INFO:root:Gibbs Iter 410, no. changed = 36, nlp = -1955.577950
2022-12-21 11:04:14,226:INFO:root:Gibbs Iter 420, no. changed = 32, nlp = -1919.622831
2022-12-21 11:04:15,187:INFO:root:Gibbs Iter 430, no. changed = 31, nlp = -1905.437332
2022-12-21 11:04:16,148:INFO:root:Gibbs Iter 440, no. changed = 36, nlp = -1946.553501
2022-12-21 11:04:17,103:INFO:root:Gibbs Iter 450, no. changed = 30, nlp = -1961.075445
2022-12-21 11:04:18,060:INFO:root:Gibbs Iter 460, no. changed = 29, nlp = -1902.243957
2022-12-21 11:04:19,019:INFO:root:Gibbs Iter 470, no. changed = 32, nlp = -1954.229859
2022-12-21 11:04:19,976:INFO:root:Gibbs Iter 480, no. changed = 31, nlp = -1974.094229
2022-12-21 11:04:20,933:INFO:root:Gibbs Iter 490, no. changed = 39, nlp = -1926.009272
2022-12-21 11:04:21,889:INFO:root:Gibbs Iter 500, no. changed = 32, nlp = -1950.055817
2022-12-21 11:04:22,848:INFO:root:Gibbs Iter 510, no. changed = 33, nlp = -1916.988795
2022-12-21 11:04:23,803:INFO:root:Gibbs Iter 520, no. changed = 34, nlp = -1955.075754
2022-12-21 11:04:24,762:INFO:root:Gibbs Iter 530, no. changed = 32, nlp = -1943.189338
2022-12-21 11:04:25,720:INFO:root:Gibbs Iter 540, no. changed = 29, nlp = -1930.357205
2022-12-21 11:04:26,675:INFO:root:Gibbs Iter 550, no. changed = 32, nlp = -1959.009163
2022-12-21 11:04:27,634:INFO:root:Gibbs Iter 560, no. changed = 32, nlp = -1917.187273
2022-12-21 11:04:28,591:INFO:root:Gibbs Iter 570, no. changed = 38, nlp = -1954.949371
2022-12-21 11:04:29,547:INFO:root:Gibbs Iter 580, no. changed = 33, nlp = -1961.963293
2022-12-21 11:04:30,507:INFO:root:Gibbs Iter 590, no. changed = 35, nlp = -1923.963655
2022-12-21 11:04:31,463:INFO:root:Gibbs Iter 600, no. changed = 42, nlp = -1969.147767
2022-12-21 11:04:32,423:INFO:root:Gibbs Iter 610, no. changed = 30, nlp = -1923.340096
2022-12-21 11:04:33,376:INFO:root:Gibbs Iter 620, no. changed = 31, nlp = -1954.430770
2022-12-21 11:04:34,331:INFO:root:Gibbs Iter 630, no. changed = 34, nlp = -1940.560035
2022-12-21 11:04:35,286:INFO:root:Gibbs Iter 640, no. changed = 34, nlp = -1937.603428
2022-12-21 11:04:36,243:INFO:root:Gibbs Iter 650, no. changed = 28, nlp = -1943.367593
2022-12-21 11:04:37,197:INFO:root:Gibbs Iter 660, no. changed = 37, nlp = -1938.924387
2022-12-21 11:04:38,156:INFO:root:Gibbs Iter 670, no. changed = 33, nlp = -1947.467472
2022-12-21 11:04:39,113:INFO:root:Gibbs Iter 680, no. changed = 34, nlp = -1932.268128
2022-12-21 11:04:40,070:INFO:root:Gibbs Iter 690, no. changed = 35, nlp = -1881.391833
2022-12-21 11:04:41,027:INFO:root:Gibbs Iter 700, no. changed = 30, nlp = -1958.831515
2022-12-21 11:04:41,982:INFO:root:Gibbs Iter 710, no. changed = 30, nlp = -1948.701658
2022-12-21 11:04:42,941:INFO:root:Gibbs Iter 720, no. changed = 31, nlp = -1944.593963
2022-12-21 11:04:43,896:INFO:root:Gibbs Iter 730, no. changed = 37, nlp = -1957.036288
2022-12-21 11:04:44,852:INFO:root:Gibbs Iter 740, no. changed = 32, nlp = -1961.663814
2022-12-21 11:04:45,812:INFO:root:Gibbs Iter 750, no. changed = 32, nlp = -1963.660790
2022-12-21 11:04:46,771:INFO:root:Gibbs Iter 760, no. changed = 29, nlp = -1931.231344
2022-12-21 11:04:47,730:INFO:root:Gibbs Iter 770, no. changed = 33, nlp = -1944.848417
2022-12-21 11:04:48,682:INFO:root:Gibbs Iter 780, no. changed = 35, nlp = -1960.211011
2022-12-21 11:04:49,642:INFO:root:Gibbs Iter 790, no. changed = 32, nlp = -1955.751411
2022-12-21 11:04:50,602:INFO:root:Gibbs Iter 800, no. changed = 35, nlp = -1932.292731
2022-12-21 11:04:51,558:INFO:root:Gibbs Iter 810, no. changed = 34, nlp = -1924.977896
2022-12-21 11:04:52,516:INFO:root:Gibbs Iter 820, no. changed = 31, nlp = -1951.273768
2022-12-21 11:04:53,473:INFO:root:Gibbs Iter 830, no. changed = 30, nlp = -1962.399595
2022-12-21 11:04:54,434:INFO:root:Gibbs Iter 840, no. changed = 30, nlp = -1963.158301
2022-12-21 11:04:55,395:INFO:root:Gibbs Iter 850, no. changed = 32, nlp = -1921.661250
2022-12-21 11:04:56,354:INFO:root:Gibbs Iter 860, no. changed = 33, nlp = -1907.465389
2022-12-21 11:04:57,314:INFO:root:Gibbs Iter 870, no. changed = 40, nlp = -1920.450491
2022-12-21 11:04:58,273:INFO:root:Gibbs Iter 880, no. changed = 36, nlp = -1962.628920
2022-12-21 11:04:59,233:INFO:root:Gibbs Iter 890, no. changed = 36, nlp = -1947.756484
2022-12-21 11:05:00,191:INFO:root:Gibbs Iter 900, no. changed = 35, nlp = -1931.238919
2022-12-21 11:05:01,150:INFO:root:Gibbs Iter 910, no. changed = 37, nlp = -1923.075177
2022-12-21 11:05:02,108:INFO:root:Gibbs Iter 920, no. changed = 34, nlp = -1939.494836
2022-12-21 11:05:03,063:INFO:root:Gibbs Iter 930, no. changed = 43, nlp = -1951.106064
2022-12-21 11:05:04,017:INFO:root:Gibbs Iter 940, no. changed = 36, nlp = -1938.980884
2022-12-21 11:05:04,978:INFO:root:Gibbs Iter 950, no. changed = 30, nlp = -1944.544991
2022-12-21 11:05:05,933:INFO:root:Gibbs Iter 960, no. changed = 33, nlp = -1941.600509
2022-12-21 11:05:06,891:INFO:root:Gibbs Iter 970, no. changed = 36, nlp = -1967.278305
2022-12-21 11:05:07,843:INFO:root:Gibbs Iter 980, no. changed = 33, nlp = -1936.013498
2022-12-21 11:05:08,806:INFO:root:Gibbs Iter 990, no. changed = 31, nlp = -1933.071932
2022-12-21 11:05:09,679:INFO:root:Wrote fit stats
2022-12-21 11:05:09,686:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 11:05:09,693:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 11:05:09,697:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 11:05:09,745:INFO:root:Wrote ll_store
2022-12-21 11:05:09,749:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 11:05:09,752:INFO:root:Wrote transition error matrix
2022-12-21 11:05:09,756:INFO:root:Wrote transition error matrix
2022-12-21 11:05:09,760:INFO:root:Wrote selected variants
