2022-12-21 10:34:45,947:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.2.3
2022-12-21 10:34:45,947:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 10:34:45,981:INFO:root:Running Desman with 9 samples and 94 variant positions finding 2 genomes.
2022-12-21 10:34:45,981:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 10:34:45,982:INFO:root:Set second adjustable random seed = 3
2022-12-21 10:34:45,983:INFO:root:Perform NTF initialisation
2022-12-21 10:34:45,985:INFO:root:NTF Iter 0, div = 401.673201
2022-12-21 10:34:46,073:INFO:root:NTF Iter 100, div = 35.905415
2022-12-21 10:34:46,159:INFO:root:NTF Iter 200, div = 34.678309
2022-12-21 10:34:46,245:INFO:root:NTF Iter 300, div = 32.674952
2022-12-21 10:34:46,329:INFO:root:NTF Iter 400, div = 29.044829
2022-12-21 10:34:46,412:INFO:root:NTF Iter 500, div = 26.728856
2022-12-21 10:34:46,503:INFO:root:NTF Iter 600, div = 24.968507
2022-12-21 10:34:46,597:INFO:root:NTF Iter 700, div = 23.974963
2022-12-21 10:34:46,693:INFO:root:NTF Iter 800, div = 23.547927
2022-12-21 10:34:46,787:INFO:root:NTF Iter 900, div = 23.292046
2022-12-21 10:34:46,881:INFO:root:NTF Iter 1000, div = 23.152288
2022-12-21 10:34:46,977:INFO:root:NTF Iter 1100, div = 23.092997
2022-12-21 10:34:47,073:INFO:root:NTF Iter 1200, div = 23.067701
2022-12-21 10:34:47,165:INFO:root:NTF Iter 1300, div = 23.026394
2022-12-21 10:34:47,259:INFO:root:NTF Iter 1400, div = 23.014974
2022-12-21 10:34:47,350:INFO:root:NTF Iter 1500, div = 22.992087
2022-12-21 10:34:47,442:INFO:root:NTF Iter 1600, div = 22.981763
2022-12-21 10:34:47,534:INFO:root:NTF Iter 1700, div = 22.967089
2022-12-21 10:34:47,626:INFO:root:NTF Iter 1800, div = 22.957591
2022-12-21 10:34:47,720:INFO:root:NTF Iter 1900, div = 22.948808
2022-12-21 10:34:47,815:INFO:root:NTF Iter 2000, div = 22.944808
2022-12-21 10:34:47,909:INFO:root:NTF Iter 2100, div = 22.942929
2022-12-21 10:34:47,915:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 10:34:48,035:INFO:root:Gibbs Iter 0, no. changed = 29, nlp = -2046.315873
2022-12-21 10:34:49,029:INFO:root:Gibbs Iter 10, no. changed = 27, nlp = -2024.606362
2022-12-21 10:34:50,021:INFO:root:Gibbs Iter 20, no. changed = 27, nlp = -2042.745058
2022-12-21 10:34:51,022:INFO:root:Gibbs Iter 30, no. changed = 27, nlp = -2058.543883
2022-12-21 10:34:52,024:INFO:root:Gibbs Iter 40, no. changed = 26, nlp = -2056.236317
2022-12-21 10:34:53,026:INFO:root:Gibbs Iter 50, no. changed = 25, nlp = -2055.075947
2022-12-21 10:34:54,013:INFO:root:Gibbs Iter 60, no. changed = 26, nlp = -2057.255114
2022-12-21 10:34:54,995:INFO:root:Gibbs Iter 70, no. changed = 26, nlp = -2036.444374
2022-12-21 10:34:55,973:INFO:root:Gibbs Iter 80, no. changed = 26, nlp = -2015.076090
2022-12-21 10:34:56,953:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2043.489602
2022-12-21 10:34:57,944:INFO:root:Gibbs Iter 100, no. changed = 26, nlp = -2044.825548
2022-12-21 10:34:58,933:INFO:root:Gibbs Iter 110, no. changed = 28, nlp = -2048.682461
2022-12-21 10:34:59,931:INFO:root:Gibbs Iter 120, no. changed = 27, nlp = -2040.281228
2022-12-21 10:35:00,927:INFO:root:Gibbs Iter 130, no. changed = 28, nlp = -2045.581937
2022-12-21 10:35:01,925:INFO:root:Gibbs Iter 140, no. changed = 26, nlp = -2030.564647
2022-12-21 10:35:02,898:INFO:root:Gibbs Iter 150, no. changed = 27, nlp = -2046.072540
2022-12-21 10:35:03,868:INFO:root:Gibbs Iter 160, no. changed = 27, nlp = -2009.217075
2022-12-21 10:35:04,854:INFO:root:Gibbs Iter 170, no. changed = 27, nlp = -2053.901162
2022-12-21 10:35:05,828:INFO:root:Gibbs Iter 180, no. changed = 26, nlp = -2055.756367
2022-12-21 10:35:06,815:INFO:root:Gibbs Iter 190, no. changed = 26, nlp = -2032.803219
2022-12-21 10:35:07,804:INFO:root:Gibbs Iter 200, no. changed = 27, nlp = -2037.984419
2022-12-21 10:35:08,790:INFO:root:Gibbs Iter 210, no. changed = 28, nlp = -2048.815286
2022-12-21 10:35:09,770:INFO:root:Gibbs Iter 220, no. changed = 28, nlp = -2058.195952
2022-12-21 10:35:10,767:INFO:root:Gibbs Iter 230, no. changed = 27, nlp = -2020.582516
2022-12-21 10:35:11,738:INFO:root:Gibbs Iter 240, no. changed = 26, nlp = -2051.085157
2022-12-21 10:35:12,690:INFO:root:Gibbs Iter 250, no. changed = 28, nlp = -2018.458738
2022-12-21 10:35:13,641:INFO:root:Gibbs Iter 260, no. changed = 28, nlp = -2052.762645
2022-12-21 10:35:14,599:INFO:root:Gibbs Iter 270, no. changed = 27, nlp = -2022.391756
2022-12-21 10:35:15,553:INFO:root:Gibbs Iter 280, no. changed = 28, nlp = -2049.033166
2022-12-21 10:35:16,502:INFO:root:Gibbs Iter 290, no. changed = 27, nlp = -2057.411635
2022-12-21 10:35:17,449:INFO:root:Gibbs Iter 300, no. changed = 27, nlp = -2050.269050
2022-12-21 10:35:18,397:INFO:root:Gibbs Iter 310, no. changed = 27, nlp = -2032.253942
2022-12-21 10:35:19,352:INFO:root:Gibbs Iter 320, no. changed = 27, nlp = -2020.375974
2022-12-21 10:35:20,338:INFO:root:Gibbs Iter 330, no. changed = 26, nlp = -2034.253487
2022-12-21 10:35:21,332:INFO:root:Gibbs Iter 340, no. changed = 28, nlp = -2050.128882
2022-12-21 10:35:22,319:INFO:root:Gibbs Iter 350, no. changed = 27, nlp = -2033.546317
2022-12-21 10:35:23,313:INFO:root:Gibbs Iter 360, no. changed = 26, nlp = -2056.239607
2022-12-21 10:35:24,284:INFO:root:Gibbs Iter 370, no. changed = 28, nlp = -2041.918950
2022-12-21 10:35:25,276:INFO:root:Gibbs Iter 380, no. changed = 27, nlp = -2042.880969
2022-12-21 10:35:26,270:INFO:root:Gibbs Iter 390, no. changed = 26, nlp = -2056.834147
2022-12-21 10:35:27,265:INFO:root:Gibbs Iter 400, no. changed = 27, nlp = -2047.848102
2022-12-21 10:35:28,254:INFO:root:Gibbs Iter 410, no. changed = 27, nlp = -2045.777778
2022-12-21 10:35:29,254:INFO:root:Gibbs Iter 420, no. changed = 29, nlp = -2025.216508
2022-12-21 10:35:30,246:INFO:root:Gibbs Iter 430, no. changed = 27, nlp = -2051.863969
2022-12-21 10:35:31,246:INFO:root:Gibbs Iter 440, no. changed = 26, nlp = -2058.198524
2022-12-21 10:35:32,213:INFO:root:Gibbs Iter 450, no. changed = 26, nlp = -2049.784329
2022-12-21 10:35:33,189:INFO:root:Gibbs Iter 460, no. changed = 27, nlp = -2044.747211
2022-12-21 10:35:34,142:INFO:root:Gibbs Iter 470, no. changed = 26, nlp = -2048.822582
2022-12-21 10:35:35,096:INFO:root:Gibbs Iter 480, no. changed = 25, nlp = -2052.291309
2022-12-21 10:35:36,045:INFO:root:Gibbs Iter 490, no. changed = 27, nlp = -2030.414667
2022-12-21 10:35:36,998:INFO:root:Gibbs Iter 500, no. changed = 28, nlp = -2052.538406
2022-12-21 10:35:37,965:INFO:root:Gibbs Iter 510, no. changed = 28, nlp = -2044.381360
2022-12-21 10:35:38,915:INFO:root:Gibbs Iter 520, no. changed = 28, nlp = -2046.958757
2022-12-21 10:35:39,883:INFO:root:Gibbs Iter 530, no. changed = 27, nlp = -2054.664523
2022-12-21 10:35:40,845:INFO:root:Gibbs Iter 540, no. changed = 27, nlp = -2043.075905
2022-12-21 10:35:41,795:INFO:root:Gibbs Iter 550, no. changed = 26, nlp = -2054.447679
2022-12-21 10:35:42,745:INFO:root:Gibbs Iter 560, no. changed = 28, nlp = -2051.451034
2022-12-21 10:35:43,732:INFO:root:Gibbs Iter 570, no. changed = 26, nlp = -2052.279826
2022-12-21 10:35:44,723:INFO:root:Gibbs Iter 580, no. changed = 27, nlp = -2043.435408
2022-12-21 10:35:45,685:INFO:root:Gibbs Iter 590, no. changed = 26, nlp = -2060.115429
2022-12-21 10:35:46,659:INFO:root:Gibbs Iter 600, no. changed = 28, nlp = -2040.992532
2022-12-21 10:35:47,633:INFO:root:Gibbs Iter 610, no. changed = 27, nlp = -2045.002860
2022-12-21 10:35:48,634:INFO:root:Gibbs Iter 620, no. changed = 27, nlp = -2034.824572
2022-12-21 10:35:49,626:INFO:root:Gibbs Iter 630, no. changed = 27, nlp = -2043.940280
2022-12-21 10:35:50,621:INFO:root:Gibbs Iter 640, no. changed = 26, nlp = -2046.436881
2022-12-21 10:35:51,621:INFO:root:Gibbs Iter 650, no. changed = 27, nlp = -2049.183369
2022-12-21 10:35:52,619:INFO:root:Gibbs Iter 660, no. changed = 28, nlp = -2016.453124
2022-12-21 10:35:53,584:INFO:root:Gibbs Iter 670, no. changed = 26, nlp = -2051.364539
2022-12-21 10:35:54,559:INFO:root:Gibbs Iter 680, no. changed = 28, nlp = -2048.075222
2022-12-21 10:35:55,560:INFO:root:Gibbs Iter 690, no. changed = 26, nlp = -2058.338015
2022-12-21 10:35:56,523:INFO:root:Gibbs Iter 700, no. changed = 27, nlp = -2044.430044
2022-12-21 10:35:57,470:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2050.449479
2022-12-21 10:35:58,419:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2015.772456
2022-12-21 10:35:59,369:INFO:root:Gibbs Iter 730, no. changed = 26, nlp = -2046.149125
2022-12-21 10:36:00,322:INFO:root:Gibbs Iter 740, no. changed = 28, nlp = -2055.117799
2022-12-21 10:36:01,322:INFO:root:Gibbs Iter 750, no. changed = 26, nlp = -2052.019383
2022-12-21 10:36:02,315:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2038.581359
2022-12-21 10:36:03,313:INFO:root:Gibbs Iter 770, no. changed = 28, nlp = -2057.944021
2022-12-21 10:36:04,303:INFO:root:Gibbs Iter 780, no. changed = 25, nlp = -2066.689083
2022-12-21 10:36:05,297:INFO:root:Gibbs Iter 790, no. changed = 28, nlp = -2066.704826
2022-12-21 10:36:06,251:INFO:root:Gibbs Iter 800, no. changed = 27, nlp = -2046.776483
2022-12-21 10:36:07,207:INFO:root:Gibbs Iter 810, no. changed = 29, nlp = -2053.537625
2022-12-21 10:36:08,162:INFO:root:Gibbs Iter 820, no. changed = 28, nlp = -2011.503772
2022-12-21 10:36:09,158:INFO:root:Gibbs Iter 830, no. changed = 27, nlp = -2022.129145
2022-12-21 10:36:10,109:INFO:root:Gibbs Iter 840, no. changed = 27, nlp = -2039.820597
2022-12-21 10:36:11,064:INFO:root:Gibbs Iter 850, no. changed = 25, nlp = -2051.536697
2022-12-21 10:36:12,017:INFO:root:Gibbs Iter 860, no. changed = 27, nlp = -2052.557955
2022-12-21 10:36:12,994:INFO:root:Gibbs Iter 870, no. changed = 25, nlp = -2048.256972
2022-12-21 10:36:13,964:INFO:root:Gibbs Iter 880, no. changed = 27, nlp = -2045.505662
2022-12-21 10:36:14,953:INFO:root:Gibbs Iter 890, no. changed = 28, nlp = -2035.663018
2022-12-21 10:36:15,945:INFO:root:Gibbs Iter 900, no. changed = 28, nlp = -2050.479094
2022-12-21 10:36:16,935:INFO:root:Gibbs Iter 910, no. changed = 28, nlp = -2040.376576
2022-12-21 10:36:17,924:INFO:root:Gibbs Iter 920, no. changed = 26, nlp = -2043.964724
2022-12-21 10:36:18,909:INFO:root:Gibbs Iter 930, no. changed = 27, nlp = -2033.756421
2022-12-21 10:36:19,903:INFO:root:Gibbs Iter 940, no. changed = 28, nlp = -2057.203503
2022-12-21 10:36:20,856:INFO:root:Gibbs Iter 950, no. changed = 28, nlp = -2013.608246
2022-12-21 10:36:21,808:INFO:root:Gibbs Iter 960, no. changed = 26, nlp = -2023.288049
2022-12-21 10:36:22,764:INFO:root:Gibbs Iter 970, no. changed = 28, nlp = -2052.799411
2022-12-21 10:36:23,722:INFO:root:Gibbs Iter 980, no. changed = 25, nlp = -2035.048584
2022-12-21 10:36:24,681:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2037.446457
2022-12-21 10:36:25,622:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 10:36:25,736:INFO:root:Gibbs Iter 0, no. changed = 27, nlp = -2036.338626
2022-12-21 10:36:26,671:INFO:root:Gibbs Iter 10, no. changed = 28, nlp = -2039.478986
2022-12-21 10:36:27,603:INFO:root:Gibbs Iter 20, no. changed = 26, nlp = -2009.853090
2022-12-21 10:36:28,571:INFO:root:Gibbs Iter 30, no. changed = 28, nlp = -2021.346381
2022-12-21 10:36:29,544:INFO:root:Gibbs Iter 40, no. changed = 27, nlp = -2033.458530
2022-12-21 10:36:30,507:INFO:root:Gibbs Iter 50, no. changed = 25, nlp = -2027.293867
2022-12-21 10:36:31,440:INFO:root:Gibbs Iter 60, no. changed = 27, nlp = -2036.055614
2022-12-21 10:36:32,372:INFO:root:Gibbs Iter 70, no. changed = 25, nlp = -2011.435015
2022-12-21 10:36:33,306:INFO:root:Gibbs Iter 80, no. changed = 27, nlp = -2039.992674
2022-12-21 10:36:34,239:INFO:root:Gibbs Iter 90, no. changed = 28, nlp = -2047.031036
2022-12-21 10:36:35,177:INFO:root:Gibbs Iter 100, no. changed = 25, nlp = -2052.452036
2022-12-21 10:36:36,133:INFO:root:Gibbs Iter 110, no. changed = 29, nlp = -1997.893896
2022-12-21 10:36:37,111:INFO:root:Gibbs Iter 120, no. changed = 27, nlp = -2013.749418
2022-12-21 10:36:38,087:INFO:root:Gibbs Iter 130, no. changed = 27, nlp = -2023.392306
2022-12-21 10:36:39,061:INFO:root:Gibbs Iter 140, no. changed = 26, nlp = -1991.723591
2022-12-21 10:36:40,036:INFO:root:Gibbs Iter 150, no. changed = 26, nlp = -2037.463241
2022-12-21 10:36:41,000:INFO:root:Gibbs Iter 160, no. changed = 28, nlp = -2038.776541
2022-12-21 10:36:41,957:INFO:root:Gibbs Iter 170, no. changed = 27, nlp = -2044.260782
2022-12-21 10:36:42,888:INFO:root:Gibbs Iter 180, no. changed = 26, nlp = -2052.186454
2022-12-21 10:36:43,851:INFO:root:Gibbs Iter 190, no. changed = 26, nlp = -2028.832464
2022-12-21 10:36:44,830:INFO:root:Gibbs Iter 200, no. changed = 27, nlp = -2052.731689
2022-12-21 10:36:45,794:INFO:root:Gibbs Iter 210, no. changed = 27, nlp = -2020.604249
2022-12-21 10:36:46,760:INFO:root:Gibbs Iter 220, no. changed = 29, nlp = -2056.157057
2022-12-21 10:36:47,729:INFO:root:Gibbs Iter 230, no. changed = 27, nlp = -2038.631115
2022-12-21 10:36:48,703:INFO:root:Gibbs Iter 240, no. changed = 28, nlp = -2014.906897
2022-12-21 10:36:49,659:INFO:root:Gibbs Iter 250, no. changed = 26, nlp = -2033.126381
2022-12-21 10:36:50,583:INFO:root:Gibbs Iter 260, no. changed = 26, nlp = -2047.949767
2022-12-21 10:36:51,511:INFO:root:Gibbs Iter 270, no. changed = 28, nlp = -2052.316589
2022-12-21 10:36:52,438:INFO:root:Gibbs Iter 280, no. changed = 26, nlp = -2058.860074
2022-12-21 10:36:53,367:INFO:root:Gibbs Iter 290, no. changed = 26, nlp = -2038.367783
2022-12-21 10:36:54,297:INFO:root:Gibbs Iter 300, no. changed = 26, nlp = -2049.117380
2022-12-21 10:36:55,240:INFO:root:Gibbs Iter 310, no. changed = 28, nlp = -2038.747895
2022-12-21 10:36:56,217:INFO:root:Gibbs Iter 320, no. changed = 28, nlp = -2052.356167
2022-12-21 10:36:57,196:INFO:root:Gibbs Iter 330, no. changed = 28, nlp = -2032.315162
2022-12-21 10:36:58,177:INFO:root:Gibbs Iter 340, no. changed = 27, nlp = -2026.689047
2022-12-21 10:36:59,155:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -2043.440200
2022-12-21 10:37:00,137:INFO:root:Gibbs Iter 360, no. changed = 25, nlp = -2054.850215
2022-12-21 10:37:01,126:INFO:root:Gibbs Iter 370, no. changed = 27, nlp = -2054.012358
2022-12-21 10:37:02,110:INFO:root:Gibbs Iter 380, no. changed = 27, nlp = -2039.841308
2022-12-21 10:37:03,092:INFO:root:Gibbs Iter 390, no. changed = 27, nlp = -2035.907145
2022-12-21 10:37:04,040:INFO:root:Gibbs Iter 400, no. changed = 28, nlp = -2048.107951
2022-12-21 10:37:04,986:INFO:root:Gibbs Iter 410, no. changed = 27, nlp = -2054.629969
2022-12-21 10:37:05,930:INFO:root:Gibbs Iter 420, no. changed = 26, nlp = -2030.131434
2022-12-21 10:37:06,913:INFO:root:Gibbs Iter 430, no. changed = 25, nlp = -2060.316005
2022-12-21 10:37:07,899:INFO:root:Gibbs Iter 440, no. changed = 27, nlp = -2048.441507
2022-12-21 10:37:08,838:INFO:root:Gibbs Iter 450, no. changed = 27, nlp = -2059.179190
2022-12-21 10:37:09,772:INFO:root:Gibbs Iter 460, no. changed = 26, nlp = -2044.097701
2022-12-21 10:37:10,707:INFO:root:Gibbs Iter 470, no. changed = 28, nlp = -2041.944163
2022-12-21 10:37:11,661:INFO:root:Gibbs Iter 480, no. changed = 27, nlp = -2022.000717
2022-12-21 10:37:12,632:INFO:root:Gibbs Iter 490, no. changed = 24, nlp = -2064.611874
2022-12-21 10:37:13,601:INFO:root:Gibbs Iter 500, no. changed = 28, nlp = -2051.790720
2022-12-21 10:37:14,566:INFO:root:Gibbs Iter 510, no. changed = 27, nlp = -2047.964463
2022-12-21 10:37:15,540:INFO:root:Gibbs Iter 520, no. changed = 26, nlp = -2050.985768
2022-12-21 10:37:16,512:INFO:root:Gibbs Iter 530, no. changed = 28, nlp = -2055.150101
2022-12-21 10:37:17,483:INFO:root:Gibbs Iter 540, no. changed = 28, nlp = -2046.894778
2022-12-21 10:37:18,447:INFO:root:Gibbs Iter 550, no. changed = 27, nlp = -2036.068322
2022-12-21 10:37:19,425:INFO:root:Gibbs Iter 560, no. changed = 25, nlp = -2015.080681
2022-12-21 10:37:20,408:INFO:root:Gibbs Iter 570, no. changed = 28, nlp = -2035.946007
2022-12-21 10:37:21,389:INFO:root:Gibbs Iter 580, no. changed = 28, nlp = -2062.072162
2022-12-21 10:37:22,375:INFO:root:Gibbs Iter 590, no. changed = 27, nlp = -2032.167237
2022-12-21 10:37:23,358:INFO:root:Gibbs Iter 600, no. changed = 26, nlp = -2044.601265
2022-12-21 10:37:24,336:INFO:root:Gibbs Iter 610, no. changed = 27, nlp = -2048.835300
2022-12-21 10:37:25,273:INFO:root:Gibbs Iter 620, no. changed = 28, nlp = -2057.334296
2022-12-21 10:37:26,207:INFO:root:Gibbs Iter 630, no. changed = 28, nlp = -2042.068001
2022-12-21 10:37:27,141:INFO:root:Gibbs Iter 640, no. changed = 26, nlp = -2066.836014
2022-12-21 10:37:28,074:INFO:root:Gibbs Iter 650, no. changed = 26, nlp = -2045.343352
2022-12-21 10:37:29,013:INFO:root:Gibbs Iter 660, no. changed = 27, nlp = -2033.987653
2022-12-21 10:37:29,968:INFO:root:Gibbs Iter 670, no. changed = 27, nlp = -2046.002238
2022-12-21 10:37:30,952:INFO:root:Gibbs Iter 680, no. changed = 27, nlp = -2025.596615
2022-12-21 10:37:31,930:INFO:root:Gibbs Iter 690, no. changed = 28, nlp = -2059.006624
2022-12-21 10:37:32,913:INFO:root:Gibbs Iter 700, no. changed = 27, nlp = -2054.604887
2022-12-21 10:37:33,887:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2039.217193
2022-12-21 10:37:34,870:INFO:root:Gibbs Iter 720, no. changed = 25, nlp = -2048.574360
2022-12-21 10:37:35,836:INFO:root:Gibbs Iter 730, no. changed = 26, nlp = -2032.644090
2022-12-21 10:37:36,797:INFO:root:Gibbs Iter 740, no. changed = 27, nlp = -2055.076514
2022-12-21 10:37:37,752:INFO:root:Gibbs Iter 750, no. changed = 26, nlp = -2033.438574
2022-12-21 10:37:38,689:INFO:root:Gibbs Iter 760, no. changed = 26, nlp = -2042.541894
2022-12-21 10:37:39,624:INFO:root:Gibbs Iter 770, no. changed = 27, nlp = -2054.212321
2022-12-21 10:37:40,595:INFO:root:Gibbs Iter 780, no. changed = 27, nlp = -1979.099129
2022-12-21 10:37:41,568:INFO:root:Gibbs Iter 790, no. changed = 27, nlp = -2046.306880
2022-12-21 10:37:42,536:INFO:root:Gibbs Iter 800, no. changed = 26, nlp = -2034.727972
2022-12-21 10:37:43,487:INFO:root:Gibbs Iter 810, no. changed = 29, nlp = -2040.714463
2022-12-21 10:37:44,435:INFO:root:Gibbs Iter 820, no. changed = 25, nlp = -2050.240835
2022-12-21 10:37:45,396:INFO:root:Gibbs Iter 830, no. changed = 28, nlp = -2049.519901
2022-12-21 10:37:46,354:INFO:root:Gibbs Iter 840, no. changed = 26, nlp = -2034.041418
2022-12-21 10:37:47,328:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2048.898688
2022-12-21 10:37:48,285:INFO:root:Gibbs Iter 860, no. changed = 27, nlp = -2054.794930
2022-12-21 10:37:49,261:INFO:root:Gibbs Iter 870, no. changed = 25, nlp = -2063.572470
2022-12-21 10:37:50,233:INFO:root:Gibbs Iter 880, no. changed = 27, nlp = -2067.843458
2022-12-21 10:37:51,216:INFO:root:Gibbs Iter 890, no. changed = 27, nlp = -2043.319029
2022-12-21 10:37:52,151:INFO:root:Gibbs Iter 900, no. changed = 27, nlp = -2055.040385
2022-12-21 10:37:53,083:INFO:root:Gibbs Iter 910, no. changed = 27, nlp = -2047.873380
2022-12-21 10:37:54,015:INFO:root:Gibbs Iter 920, no. changed = 27, nlp = -2028.250670
2022-12-21 10:37:54,951:INFO:root:Gibbs Iter 930, no. changed = 27, nlp = -2056.368342
2022-12-21 10:37:55,889:INFO:root:Gibbs Iter 940, no. changed = 24, nlp = -2061.988238
2022-12-21 10:37:56,827:INFO:root:Gibbs Iter 950, no. changed = 26, nlp = -2047.937500
2022-12-21 10:37:57,764:INFO:root:Gibbs Iter 960, no. changed = 29, nlp = -2037.110341
2022-12-21 10:37:58,699:INFO:root:Gibbs Iter 970, no. changed = 28, nlp = -2046.673129
2022-12-21 10:37:59,635:INFO:root:Gibbs Iter 980, no. changed = 27, nlp = -2033.710897
2022-12-21 10:38:00,569:INFO:root:Gibbs Iter 990, no. changed = 28, nlp = -2048.188642
2022-12-21 10:38:01,416:INFO:root:Wrote fit stats
2022-12-21 10:38:01,422:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 10:38:01,433:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 10:38:01,437:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 10:38:01,474:INFO:root:Wrote ll_store
2022-12-21 10:38:01,477:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 10:38:01,480:INFO:root:Wrote transition error matrix
2022-12-21 10:38:01,483:INFO:root:Wrote transition error matrix
2022-12-21 10:38:01,488:INFO:root:Wrote selected variants
