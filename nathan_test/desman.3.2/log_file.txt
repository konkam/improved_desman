2022-12-21 10:48:01,181:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.3.2
2022-12-21 10:48:01,182:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 10:48:01,217:INFO:root:Running Desman with 9 samples and 94 variant positions finding 3 genomes.
2022-12-21 10:48:01,217:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 10:48:01,218:INFO:root:Set second adjustable random seed = 2
2022-12-21 10:48:01,219:INFO:root:Perform NTF initialisation
2022-12-21 10:48:01,222:INFO:root:NTF Iter 0, div = 505.791653
2022-12-21 10:48:01,334:INFO:root:NTF Iter 100, div = 37.169138
2022-12-21 10:48:01,445:INFO:root:NTF Iter 200, div = 36.696091
2022-12-21 10:48:01,556:INFO:root:NTF Iter 300, div = 35.729817
2022-12-21 10:48:01,669:INFO:root:NTF Iter 400, div = 34.293382
2022-12-21 10:48:01,779:INFO:root:NTF Iter 500, div = 31.547899
2022-12-21 10:48:01,890:INFO:root:NTF Iter 600, div = 26.335405
2022-12-21 10:48:02,001:INFO:root:NTF Iter 700, div = 24.005249
2022-12-21 10:48:02,112:INFO:root:NTF Iter 800, div = 22.950621
2022-12-21 10:48:02,222:INFO:root:NTF Iter 900, div = 22.420486
2022-12-21 10:48:02,333:INFO:root:NTF Iter 1000, div = 21.964452
2022-12-21 10:48:02,444:INFO:root:NTF Iter 1100, div = 21.418430
2022-12-21 10:48:02,554:INFO:root:NTF Iter 1200, div = 20.708903
2022-12-21 10:48:02,666:INFO:root:NTF Iter 1300, div = 19.820676
2022-12-21 10:48:02,777:INFO:root:NTF Iter 1400, div = 18.896102
2022-12-21 10:48:02,887:INFO:root:NTF Iter 1500, div = 17.766456
2022-12-21 10:48:02,997:INFO:root:NTF Iter 1600, div = 17.124837
2022-12-21 10:48:03,108:INFO:root:NTF Iter 1700, div = 16.678977
2022-12-21 10:48:03,218:INFO:root:NTF Iter 1800, div = 16.468608
2022-12-21 10:48:03,330:INFO:root:NTF Iter 1900, div = 16.342801
2022-12-21 10:48:03,440:INFO:root:NTF Iter 2000, div = 16.260112
2022-12-21 10:48:03,550:INFO:root:NTF Iter 2100, div = 16.214536
2022-12-21 10:48:03,662:INFO:root:NTF Iter 2200, div = 16.169201
2022-12-21 10:48:03,772:INFO:root:NTF Iter 2300, div = 16.139955
2022-12-21 10:48:03,882:INFO:root:NTF Iter 2400, div = 16.108152
2022-12-21 10:48:03,993:INFO:root:NTF Iter 2500, div = 16.088312
2022-12-21 10:48:04,103:INFO:root:NTF Iter 2600, div = 16.078541
2022-12-21 10:48:04,213:INFO:root:NTF Iter 2700, div = 16.058568
2022-12-21 10:48:04,323:INFO:root:NTF Iter 2800, div = 16.051185
2022-12-21 10:48:04,433:INFO:root:NTF Iter 2900, div = 16.046209
2022-12-21 10:48:04,544:INFO:root:NTF Iter 3000, div = 16.037960
2022-12-21 10:48:04,655:INFO:root:NTF Iter 3100, div = 16.025076
2022-12-21 10:48:04,765:INFO:root:NTF Iter 3200, div = 16.011776
2022-12-21 10:48:04,875:INFO:root:NTF Iter 3300, div = 15.999274
2022-12-21 10:48:04,985:INFO:root:NTF Iter 3400, div = 15.978770
2022-12-21 10:48:05,095:INFO:root:NTF Iter 3500, div = 15.967018
2022-12-21 10:48:05,206:INFO:root:NTF Iter 3600, div = 15.956942
2022-12-21 10:48:05,316:INFO:root:NTF Iter 3700, div = 15.942678
2022-12-21 10:48:05,426:INFO:root:NTF Iter 3800, div = 15.936593
2022-12-21 10:48:05,536:INFO:root:NTF Iter 3900, div = 15.934545
2022-12-21 10:48:05,647:INFO:root:NTF Iter 4000, div = 15.926714
2022-12-21 10:48:05,757:INFO:root:NTF Iter 4100, div = 15.924784
2022-12-21 10:48:05,868:INFO:root:NTF Iter 4200, div = 15.920585
2022-12-21 10:48:05,979:INFO:root:NTF Iter 4300, div = 15.915148
2022-12-21 10:48:06,091:INFO:root:NTF Iter 4400, div = 15.910759
2022-12-21 10:48:06,201:INFO:root:NTF Iter 4500, div = 15.905857
2022-12-21 10:48:06,226:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 10:48:06,340:INFO:root:Gibbs Iter 0, no. changed = 24, nlp = -2205.904391
2022-12-21 10:48:07,295:INFO:root:Gibbs Iter 10, no. changed = 24, nlp = -2111.612705
2022-12-21 10:48:08,245:INFO:root:Gibbs Iter 20, no. changed = 24, nlp = -2100.286578
2022-12-21 10:48:09,195:INFO:root:Gibbs Iter 30, no. changed = 24, nlp = -2072.472044
2022-12-21 10:48:10,149:INFO:root:Gibbs Iter 40, no. changed = 26, nlp = -2047.386200
2022-12-21 10:48:11,098:INFO:root:Gibbs Iter 50, no. changed = 28, nlp = -2031.595553
2022-12-21 10:48:12,046:INFO:root:Gibbs Iter 60, no. changed = 25, nlp = -2067.968795
2022-12-21 10:48:12,995:INFO:root:Gibbs Iter 70, no. changed = 27, nlp = -2067.561805
2022-12-21 10:48:13,947:INFO:root:Gibbs Iter 80, no. changed = 23, nlp = -2065.515966
2022-12-21 10:48:14,898:INFO:root:Gibbs Iter 90, no. changed = 26, nlp = -2045.982613
2022-12-21 10:48:15,850:INFO:root:Gibbs Iter 100, no. changed = 25, nlp = -2049.900340
2022-12-21 10:48:16,801:INFO:root:Gibbs Iter 110, no. changed = 24, nlp = -2058.958714
2022-12-21 10:48:17,751:INFO:root:Gibbs Iter 120, no. changed = 23, nlp = -2051.347968
2022-12-21 10:48:18,703:INFO:root:Gibbs Iter 130, no. changed = 27, nlp = -2051.262831
2022-12-21 10:48:19,653:INFO:root:Gibbs Iter 140, no. changed = 27, nlp = -2043.712169
2022-12-21 10:48:20,602:INFO:root:Gibbs Iter 150, no. changed = 26, nlp = -2057.992220
2022-12-21 10:48:21,552:INFO:root:Gibbs Iter 160, no. changed = 27, nlp = -2055.903596
2022-12-21 10:48:22,503:INFO:root:Gibbs Iter 170, no. changed = 28, nlp = -2071.535824
2022-12-21 10:48:23,452:INFO:root:Gibbs Iter 180, no. changed = 29, nlp = -2035.939828
2022-12-21 10:48:24,403:INFO:root:Gibbs Iter 190, no. changed = 25, nlp = -1994.800536
2022-12-21 10:48:25,354:INFO:root:Gibbs Iter 200, no. changed = 24, nlp = -2044.253720
2022-12-21 10:48:26,308:INFO:root:Gibbs Iter 210, no. changed = 24, nlp = -2025.581158
2022-12-21 10:48:27,260:INFO:root:Gibbs Iter 220, no. changed = 28, nlp = -2051.668069
2022-12-21 10:48:28,209:INFO:root:Gibbs Iter 230, no. changed = 28, nlp = -2052.513788
2022-12-21 10:48:29,156:INFO:root:Gibbs Iter 240, no. changed = 23, nlp = -2052.649657
2022-12-21 10:48:30,107:INFO:root:Gibbs Iter 250, no. changed = 26, nlp = -2055.313440
2022-12-21 10:48:31,061:INFO:root:Gibbs Iter 260, no. changed = 25, nlp = -2037.335945
2022-12-21 10:48:32,009:INFO:root:Gibbs Iter 270, no. changed = 25, nlp = -2067.989940
2022-12-21 10:48:32,957:INFO:root:Gibbs Iter 280, no. changed = 23, nlp = -2053.007322
2022-12-21 10:48:33,907:INFO:root:Gibbs Iter 290, no. changed = 27, nlp = -2055.551882
2022-12-21 10:48:34,857:INFO:root:Gibbs Iter 300, no. changed = 28, nlp = -2056.227108
2022-12-21 10:48:35,807:INFO:root:Gibbs Iter 310, no. changed = 25, nlp = -2049.695375
2022-12-21 10:48:36,757:INFO:root:Gibbs Iter 320, no. changed = 26, nlp = -2003.150104
2022-12-21 10:48:37,705:INFO:root:Gibbs Iter 330, no. changed = 24, nlp = -2051.684902
2022-12-21 10:48:38,657:INFO:root:Gibbs Iter 340, no. changed = 30, nlp = -2065.270793
2022-12-21 10:48:39,605:INFO:root:Gibbs Iter 350, no. changed = 24, nlp = -2060.011074
2022-12-21 10:48:40,552:INFO:root:Gibbs Iter 360, no. changed = 25, nlp = -2070.844281
2022-12-21 10:48:41,501:INFO:root:Gibbs Iter 370, no. changed = 30, nlp = -2053.254613
2022-12-21 10:48:42,451:INFO:root:Gibbs Iter 380, no. changed = 25, nlp = -2071.804543
2022-12-21 10:48:43,400:INFO:root:Gibbs Iter 390, no. changed = 31, nlp = -2058.038667
2022-12-21 10:48:44,347:INFO:root:Gibbs Iter 400, no. changed = 23, nlp = -2031.787306
2022-12-21 10:48:45,297:INFO:root:Gibbs Iter 410, no. changed = 27, nlp = -2043.745799
2022-12-21 10:48:46,247:INFO:root:Gibbs Iter 420, no. changed = 23, nlp = -2072.359795
2022-12-21 10:48:47,199:INFO:root:Gibbs Iter 430, no. changed = 23, nlp = -2066.503762
2022-12-21 10:48:48,147:INFO:root:Gibbs Iter 440, no. changed = 27, nlp = -2079.294231
2022-12-21 10:48:49,098:INFO:root:Gibbs Iter 450, no. changed = 27, nlp = -2080.592817
2022-12-21 10:48:50,046:INFO:root:Gibbs Iter 460, no. changed = 29, nlp = -2052.527081
2022-12-21 10:48:50,996:INFO:root:Gibbs Iter 470, no. changed = 25, nlp = -2051.659248
2022-12-21 10:48:51,945:INFO:root:Gibbs Iter 480, no. changed = 29, nlp = -2064.481661
2022-12-21 10:48:52,893:INFO:root:Gibbs Iter 490, no. changed = 28, nlp = -2038.470089
2022-12-21 10:48:53,843:INFO:root:Gibbs Iter 500, no. changed = 31, nlp = -2064.292723
2022-12-21 10:48:54,791:INFO:root:Gibbs Iter 510, no. changed = 28, nlp = -2054.697301
2022-12-21 10:48:55,738:INFO:root:Gibbs Iter 520, no. changed = 23, nlp = -2048.590739
2022-12-21 10:48:56,692:INFO:root:Gibbs Iter 530, no. changed = 28, nlp = -2057.479198
2022-12-21 10:48:57,639:INFO:root:Gibbs Iter 540, no. changed = 26, nlp = -2058.715106
2022-12-21 10:48:58,588:INFO:root:Gibbs Iter 550, no. changed = 25, nlp = -2054.247349
2022-12-21 10:48:59,535:INFO:root:Gibbs Iter 560, no. changed = 29, nlp = -2060.831019
2022-12-21 10:49:00,481:INFO:root:Gibbs Iter 570, no. changed = 29, nlp = -2067.846392
2022-12-21 10:49:01,432:INFO:root:Gibbs Iter 580, no. changed = 26, nlp = -2056.858878
2022-12-21 10:49:02,382:INFO:root:Gibbs Iter 590, no. changed = 27, nlp = -2070.403372
2022-12-21 10:49:03,328:INFO:root:Gibbs Iter 600, no. changed = 24, nlp = -2054.151760
2022-12-21 10:49:04,277:INFO:root:Gibbs Iter 610, no. changed = 28, nlp = -2052.165371
2022-12-21 10:49:05,225:INFO:root:Gibbs Iter 620, no. changed = 26, nlp = -2036.098526
2022-12-21 10:49:06,174:INFO:root:Gibbs Iter 630, no. changed = 29, nlp = -2054.873947
2022-12-21 10:49:07,125:INFO:root:Gibbs Iter 640, no. changed = 28, nlp = -2058.452024
2022-12-21 10:49:08,073:INFO:root:Gibbs Iter 650, no. changed = 25, nlp = -2071.749799
2022-12-21 10:49:09,022:INFO:root:Gibbs Iter 660, no. changed = 24, nlp = -2055.272422
2022-12-21 10:49:09,972:INFO:root:Gibbs Iter 670, no. changed = 26, nlp = -2065.468095
2022-12-21 10:49:10,925:INFO:root:Gibbs Iter 680, no. changed = 23, nlp = -2063.261864
2022-12-21 10:49:11,871:INFO:root:Gibbs Iter 690, no. changed = 26, nlp = -2032.987194
2022-12-21 10:49:12,818:INFO:root:Gibbs Iter 700, no. changed = 24, nlp = -2041.166733
2022-12-21 10:49:13,764:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2027.628721
2022-12-21 10:49:14,718:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2040.485356
2022-12-21 10:49:15,666:INFO:root:Gibbs Iter 730, no. changed = 29, nlp = -2059.057004
2022-12-21 10:49:16,614:INFO:root:Gibbs Iter 740, no. changed = 24, nlp = -2047.397818
2022-12-21 10:49:17,563:INFO:root:Gibbs Iter 750, no. changed = 26, nlp = -2059.581037
2022-12-21 10:49:18,514:INFO:root:Gibbs Iter 760, no. changed = 32, nlp = -2050.335514
2022-12-21 10:49:19,465:INFO:root:Gibbs Iter 770, no. changed = 28, nlp = -2065.538337
2022-12-21 10:49:20,413:INFO:root:Gibbs Iter 780, no. changed = 24, nlp = -2068.550128
2022-12-21 10:49:21,363:INFO:root:Gibbs Iter 790, no. changed = 27, nlp = -2066.904441
2022-12-21 10:49:22,312:INFO:root:Gibbs Iter 800, no. changed = 27, nlp = -2066.014609
2022-12-21 10:49:23,263:INFO:root:Gibbs Iter 810, no. changed = 28, nlp = -2064.927178
2022-12-21 10:49:24,211:INFO:root:Gibbs Iter 820, no. changed = 27, nlp = -2043.252849
2022-12-21 10:49:25,159:INFO:root:Gibbs Iter 830, no. changed = 26, nlp = -2073.010542
2022-12-21 10:49:26,108:INFO:root:Gibbs Iter 840, no. changed = 26, nlp = -2057.746435
2022-12-21 10:49:27,058:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2061.529829
2022-12-21 10:49:28,006:INFO:root:Gibbs Iter 860, no. changed = 23, nlp = -2050.904761
2022-12-21 10:49:28,956:INFO:root:Gibbs Iter 870, no. changed = 28, nlp = -2053.612854
2022-12-21 10:49:29,905:INFO:root:Gibbs Iter 880, no. changed = 25, nlp = -2061.590130
2022-12-21 10:49:30,855:INFO:root:Gibbs Iter 890, no. changed = 25, nlp = -2043.455826
2022-12-21 10:49:31,803:INFO:root:Gibbs Iter 900, no. changed = 24, nlp = -2055.428918
2022-12-21 10:49:32,752:INFO:root:Gibbs Iter 910, no. changed = 27, nlp = -2048.769899
2022-12-21 10:49:33,701:INFO:root:Gibbs Iter 920, no. changed = 26, nlp = -2042.898593
2022-12-21 10:49:34,650:INFO:root:Gibbs Iter 930, no. changed = 27, nlp = -2058.596385
2022-12-21 10:49:35,597:INFO:root:Gibbs Iter 940, no. changed = 28, nlp = -2054.107405
2022-12-21 10:49:36,546:INFO:root:Gibbs Iter 950, no. changed = 31, nlp = -2058.436790
2022-12-21 10:49:37,495:INFO:root:Gibbs Iter 960, no. changed = 30, nlp = -2058.903744
2022-12-21 10:49:38,442:INFO:root:Gibbs Iter 970, no. changed = 22, nlp = -2036.085122
2022-12-21 10:49:39,393:INFO:root:Gibbs Iter 980, no. changed = 26, nlp = -2058.133884
2022-12-21 10:49:40,340:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2044.359366
2022-12-21 10:49:41,262:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 10:49:41,378:INFO:root:Gibbs Iter 0, no. changed = 26, nlp = -2060.293662
2022-12-21 10:49:42,325:INFO:root:Gibbs Iter 10, no. changed = 28, nlp = -2069.569393
2022-12-21 10:49:43,272:INFO:root:Gibbs Iter 20, no. changed = 26, nlp = -2060.554151
2022-12-21 10:49:44,218:INFO:root:Gibbs Iter 30, no. changed = 27, nlp = -2052.266655
2022-12-21 10:49:45,169:INFO:root:Gibbs Iter 40, no. changed = 26, nlp = -2035.495113
2022-12-21 10:49:46,122:INFO:root:Gibbs Iter 50, no. changed = 22, nlp = -2048.095877
2022-12-21 10:49:47,077:INFO:root:Gibbs Iter 60, no. changed = 25, nlp = -2055.960642
2022-12-21 10:49:48,027:INFO:root:Gibbs Iter 70, no. changed = 29, nlp = -2047.452080
2022-12-21 10:49:48,973:INFO:root:Gibbs Iter 80, no. changed = 23, nlp = -2058.968145
2022-12-21 10:49:49,927:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2030.326576
2022-12-21 10:49:50,876:INFO:root:Gibbs Iter 100, no. changed = 27, nlp = -2070.604534
2022-12-21 10:49:51,826:INFO:root:Gibbs Iter 110, no. changed = 27, nlp = -2071.057870
2022-12-21 10:49:52,775:INFO:root:Gibbs Iter 120, no. changed = 25, nlp = -2068.997311
2022-12-21 10:49:53,724:INFO:root:Gibbs Iter 130, no. changed = 25, nlp = -2044.068972
2022-12-21 10:49:54,679:INFO:root:Gibbs Iter 140, no. changed = 31, nlp = -2056.702438
2022-12-21 10:49:55,628:INFO:root:Gibbs Iter 150, no. changed = 28, nlp = -2076.839552
2022-12-21 10:49:56,580:INFO:root:Gibbs Iter 160, no. changed = 26, nlp = -2072.971032
2022-12-21 10:49:57,530:INFO:root:Gibbs Iter 170, no. changed = 23, nlp = -2072.840045
2022-12-21 10:49:58,478:INFO:root:Gibbs Iter 180, no. changed = 30, nlp = -2039.163429
2022-12-21 10:49:59,428:INFO:root:Gibbs Iter 190, no. changed = 23, nlp = -2049.885110
2022-12-21 10:50:00,377:INFO:root:Gibbs Iter 200, no. changed = 27, nlp = -2028.618786
2022-12-21 10:50:01,329:INFO:root:Gibbs Iter 210, no. changed = 31, nlp = -2039.986377
2022-12-21 10:50:02,279:INFO:root:Gibbs Iter 220, no. changed = 24, nlp = -2026.397774
2022-12-21 10:50:03,232:INFO:root:Gibbs Iter 230, no. changed = 30, nlp = -2073.427620
2022-12-21 10:50:04,179:INFO:root:Gibbs Iter 240, no. changed = 23, nlp = -2055.148995
2022-12-21 10:50:05,127:INFO:root:Gibbs Iter 250, no. changed = 23, nlp = -2038.937018
2022-12-21 10:50:06,078:INFO:root:Gibbs Iter 260, no. changed = 23, nlp = -2059.261212
2022-12-21 10:50:07,032:INFO:root:Gibbs Iter 270, no. changed = 20, nlp = -2069.151073
2022-12-21 10:50:07,985:INFO:root:Gibbs Iter 280, no. changed = 29, nlp = -2047.527679
2022-12-21 10:50:08,937:INFO:root:Gibbs Iter 290, no. changed = 26, nlp = -2057.806225
2022-12-21 10:50:09,884:INFO:root:Gibbs Iter 300, no. changed = 27, nlp = -2038.958907
2022-12-21 10:50:10,836:INFO:root:Gibbs Iter 310, no. changed = 26, nlp = -2065.449898
2022-12-21 10:50:11,786:INFO:root:Gibbs Iter 320, no. changed = 27, nlp = -2043.083587
2022-12-21 10:50:12,734:INFO:root:Gibbs Iter 330, no. changed = 26, nlp = -2051.880394
2022-12-21 10:50:13,681:INFO:root:Gibbs Iter 340, no. changed = 27, nlp = -2054.685926
2022-12-21 10:50:14,631:INFO:root:Gibbs Iter 350, no. changed = 26, nlp = -2034.917899
2022-12-21 10:50:15,579:INFO:root:Gibbs Iter 360, no. changed = 28, nlp = -2078.502821
2022-12-21 10:50:16,530:INFO:root:Gibbs Iter 370, no. changed = 28, nlp = -2057.420231
2022-12-21 10:50:17,477:INFO:root:Gibbs Iter 380, no. changed = 26, nlp = -2045.595443
2022-12-21 10:50:18,426:INFO:root:Gibbs Iter 390, no. changed = 23, nlp = -2037.929389
2022-12-21 10:50:19,382:INFO:root:Gibbs Iter 400, no. changed = 32, nlp = -2061.814429
2022-12-21 10:50:20,332:INFO:root:Gibbs Iter 410, no. changed = 29, nlp = -2075.375465
2022-12-21 10:50:21,280:INFO:root:Gibbs Iter 420, no. changed = 29, nlp = -2075.651538
2022-12-21 10:50:22,227:INFO:root:Gibbs Iter 430, no. changed = 27, nlp = -2075.065067
2022-12-21 10:50:23,178:INFO:root:Gibbs Iter 440, no. changed = 26, nlp = -2052.572422
2022-12-21 10:50:24,126:INFO:root:Gibbs Iter 450, no. changed = 28, nlp = -2074.471759
2022-12-21 10:50:25,074:INFO:root:Gibbs Iter 460, no. changed = 26, nlp = -2039.012170
2022-12-21 10:50:26,025:INFO:root:Gibbs Iter 470, no. changed = 26, nlp = -2061.613071
2022-12-21 10:50:26,978:INFO:root:Gibbs Iter 480, no. changed = 23, nlp = -2055.788490
2022-12-21 10:50:27,925:INFO:root:Gibbs Iter 490, no. changed = 23, nlp = -2073.924878
2022-12-21 10:50:28,876:INFO:root:Gibbs Iter 500, no. changed = 26, nlp = -2067.817019
2022-12-21 10:50:29,829:INFO:root:Gibbs Iter 510, no. changed = 28, nlp = -2048.313637
2022-12-21 10:50:30,783:INFO:root:Gibbs Iter 520, no. changed = 24, nlp = -2041.745123
2022-12-21 10:50:31,734:INFO:root:Gibbs Iter 530, no. changed = 30, nlp = -2010.658150
2022-12-21 10:50:32,680:INFO:root:Gibbs Iter 540, no. changed = 26, nlp = -2060.219538
2022-12-21 10:50:33,631:INFO:root:Gibbs Iter 550, no. changed = 26, nlp = -2077.361803
2022-12-21 10:50:34,582:INFO:root:Gibbs Iter 560, no. changed = 27, nlp = -2045.861892
2022-12-21 10:50:35,530:INFO:root:Gibbs Iter 570, no. changed = 28, nlp = -2050.800158
2022-12-21 10:50:36,478:INFO:root:Gibbs Iter 580, no. changed = 26, nlp = -2068.434719
2022-12-21 10:50:37,427:INFO:root:Gibbs Iter 590, no. changed = 23, nlp = -2057.554925
2022-12-21 10:50:38,374:INFO:root:Gibbs Iter 600, no. changed = 31, nlp = -2057.734187
2022-12-21 10:50:39,324:INFO:root:Gibbs Iter 610, no. changed = 23, nlp = -2042.771060
2022-12-21 10:50:40,274:INFO:root:Gibbs Iter 620, no. changed = 23, nlp = -2058.290574
2022-12-21 10:50:41,223:INFO:root:Gibbs Iter 630, no. changed = 33, nlp = -2017.086460
2022-12-21 10:50:42,176:INFO:root:Gibbs Iter 640, no. changed = 26, nlp = -2079.641692
2022-12-21 10:50:43,126:INFO:root:Gibbs Iter 650, no. changed = 25, nlp = -2049.628310
2022-12-21 10:50:44,073:INFO:root:Gibbs Iter 660, no. changed = 26, nlp = -2066.251260
2022-12-21 10:50:45,020:INFO:root:Gibbs Iter 670, no. changed = 25, nlp = -2045.339871
2022-12-21 10:50:45,968:INFO:root:Gibbs Iter 680, no. changed = 28, nlp = -2053.793607
2022-12-21 10:50:46,921:INFO:root:Gibbs Iter 690, no. changed = 24, nlp = -2065.366802
2022-12-21 10:50:47,874:INFO:root:Gibbs Iter 700, no. changed = 26, nlp = -2040.525382
2022-12-21 10:50:48,824:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2065.058275
2022-12-21 10:50:49,779:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2062.352478
2022-12-21 10:50:50,732:INFO:root:Gibbs Iter 730, no. changed = 26, nlp = -2063.506206
2022-12-21 10:50:51,681:INFO:root:Gibbs Iter 740, no. changed = 30, nlp = -2054.206215
2022-12-21 10:50:52,633:INFO:root:Gibbs Iter 750, no. changed = 32, nlp = -2062.295391
2022-12-21 10:50:53,581:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2047.346332
2022-12-21 10:50:54,530:INFO:root:Gibbs Iter 770, no. changed = 24, nlp = -2005.859226
2022-12-21 10:50:55,482:INFO:root:Gibbs Iter 780, no. changed = 28, nlp = -2042.146915
2022-12-21 10:50:56,432:INFO:root:Gibbs Iter 790, no. changed = 25, nlp = -2059.590326
2022-12-21 10:50:57,382:INFO:root:Gibbs Iter 800, no. changed = 23, nlp = -2071.132297
2022-12-21 10:50:58,334:INFO:root:Gibbs Iter 810, no. changed = 26, nlp = -2043.559441
2022-12-21 10:50:59,288:INFO:root:Gibbs Iter 820, no. changed = 23, nlp = -2068.663070
2022-12-21 10:51:00,237:INFO:root:Gibbs Iter 830, no. changed = 25, nlp = -2072.077478
2022-12-21 10:51:01,190:INFO:root:Gibbs Iter 840, no. changed = 26, nlp = -2054.585408
2022-12-21 10:51:02,139:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2060.187505
2022-12-21 10:51:03,094:INFO:root:Gibbs Iter 860, no. changed = 26, nlp = -2071.604491
2022-12-21 10:51:04,045:INFO:root:Gibbs Iter 870, no. changed = 26, nlp = -2052.243755
2022-12-21 10:51:04,992:INFO:root:Gibbs Iter 880, no. changed = 29, nlp = -2051.560978
2022-12-21 10:51:05,941:INFO:root:Gibbs Iter 890, no. changed = 25, nlp = -2067.243092
2022-12-21 10:51:06,891:INFO:root:Gibbs Iter 900, no. changed = 30, nlp = -2045.193937
2022-12-21 10:51:07,841:INFO:root:Gibbs Iter 910, no. changed = 27, nlp = -2042.186357
2022-12-21 10:51:08,790:INFO:root:Gibbs Iter 920, no. changed = 25, nlp = -2061.656108
2022-12-21 10:51:09,743:INFO:root:Gibbs Iter 930, no. changed = 26, nlp = -2056.067287
2022-12-21 10:51:10,693:INFO:root:Gibbs Iter 940, no. changed = 27, nlp = -2071.023938
2022-12-21 10:51:11,641:INFO:root:Gibbs Iter 950, no. changed = 28, nlp = -2050.667515
2022-12-21 10:51:12,590:INFO:root:Gibbs Iter 960, no. changed = 25, nlp = -2064.102843
2022-12-21 10:51:13,542:INFO:root:Gibbs Iter 970, no. changed = 29, nlp = -2062.611598
2022-12-21 10:51:14,493:INFO:root:Gibbs Iter 980, no. changed = 27, nlp = -2027.390633
2022-12-21 10:51:15,447:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2052.990974
2022-12-21 10:51:16,308:INFO:root:Wrote fit stats
2022-12-21 10:51:16,314:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 10:51:16,321:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 10:51:16,324:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 10:51:16,370:INFO:root:Wrote ll_store
2022-12-21 10:51:16,374:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 10:51:16,377:INFO:root:Wrote transition error matrix
2022-12-21 10:51:16,379:INFO:root:Wrote transition error matrix
2022-12-21 10:51:16,384:INFO:root:Wrote selected variants
