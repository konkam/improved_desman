2022-12-21 10:41:24,058:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.2.5
2022-12-21 10:41:24,059:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 10:41:24,095:INFO:root:Running Desman with 9 samples and 94 variant positions finding 2 genomes.
2022-12-21 10:41:24,095:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 10:41:24,096:INFO:root:Set second adjustable random seed = 5
2022-12-21 10:41:24,097:INFO:root:Perform NTF initialisation
2022-12-21 10:41:24,099:INFO:root:NTF Iter 0, div = 408.993302
2022-12-21 10:41:24,182:INFO:root:NTF Iter 100, div = 37.261944
2022-12-21 10:41:24,269:INFO:root:NTF Iter 200, div = 36.394072
2022-12-21 10:41:24,354:INFO:root:NTF Iter 300, div = 35.196227
2022-12-21 10:41:24,438:INFO:root:NTF Iter 400, div = 32.733220
2022-12-21 10:41:24,520:INFO:root:NTF Iter 500, div = 28.521198
2022-12-21 10:41:24,604:INFO:root:NTF Iter 600, div = 26.090716
2022-12-21 10:41:24,688:INFO:root:NTF Iter 700, div = 24.288997
2022-12-21 10:41:24,772:INFO:root:NTF Iter 800, div = 23.592147
2022-12-21 10:41:24,855:INFO:root:NTF Iter 900, div = 23.303325
2022-12-21 10:41:24,941:INFO:root:NTF Iter 1000, div = 23.216906
2022-12-21 10:41:25,025:INFO:root:NTF Iter 1100, div = 23.173929
2022-12-21 10:41:25,110:INFO:root:NTF Iter 1200, div = 23.111986
2022-12-21 10:41:25,194:INFO:root:NTF Iter 1300, div = 23.055899
2022-12-21 10:41:25,279:INFO:root:NTF Iter 1400, div = 23.026190
2022-12-21 10:41:25,363:INFO:root:NTF Iter 1500, div = 22.984840
2022-12-21 10:41:25,448:INFO:root:NTF Iter 1600, div = 22.975777
2022-12-21 10:41:25,532:INFO:root:NTF Iter 1700, div = 22.966287
2022-12-21 10:41:25,616:INFO:root:NTF Iter 1800, div = 22.956402
2022-12-21 10:41:25,700:INFO:root:NTF Iter 1900, div = 22.948945
2022-12-21 10:41:25,783:INFO:root:NTF Iter 2000, div = 22.944105
2022-12-21 10:41:25,867:INFO:root:NTF Iter 2100, div = 22.936299
2022-12-21 10:41:25,898:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 10:41:26,010:INFO:root:Gibbs Iter 0, no. changed = 30, nlp = -2088.895816
2022-12-21 10:41:26,957:INFO:root:Gibbs Iter 10, no. changed = 28, nlp = -2049.860476
2022-12-21 10:41:27,904:INFO:root:Gibbs Iter 20, no. changed = 28, nlp = -2027.544626
2022-12-21 10:41:28,849:INFO:root:Gibbs Iter 30, no. changed = 28, nlp = -2065.617564
2022-12-21 10:41:29,797:INFO:root:Gibbs Iter 40, no. changed = 29, nlp = -2050.796880
2022-12-21 10:41:30,747:INFO:root:Gibbs Iter 50, no. changed = 28, nlp = -2007.527849
2022-12-21 10:41:31,693:INFO:root:Gibbs Iter 60, no. changed = 29, nlp = -2038.653397
2022-12-21 10:41:32,640:INFO:root:Gibbs Iter 70, no. changed = 28, nlp = -2057.539645
2022-12-21 10:41:33,589:INFO:root:Gibbs Iter 80, no. changed = 28, nlp = -2061.721211
2022-12-21 10:41:34,535:INFO:root:Gibbs Iter 90, no. changed = 26, nlp = -2058.895493
2022-12-21 10:41:35,483:INFO:root:Gibbs Iter 100, no. changed = 27, nlp = -2058.108715
2022-12-21 10:41:36,433:INFO:root:Gibbs Iter 110, no. changed = 27, nlp = -2060.114482
2022-12-21 10:41:37,377:INFO:root:Gibbs Iter 120, no. changed = 28, nlp = -2049.722977
2022-12-21 10:41:38,322:INFO:root:Gibbs Iter 130, no. changed = 27, nlp = -2064.814813
2022-12-21 10:41:39,266:INFO:root:Gibbs Iter 140, no. changed = 28, nlp = -2046.852054
2022-12-21 10:41:40,208:INFO:root:Gibbs Iter 150, no. changed = 27, nlp = -2047.372084
2022-12-21 10:41:41,152:INFO:root:Gibbs Iter 160, no. changed = 28, nlp = -2067.594989
2022-12-21 10:41:42,097:INFO:root:Gibbs Iter 170, no. changed = 26, nlp = -2043.585055
2022-12-21 10:41:43,042:INFO:root:Gibbs Iter 180, no. changed = 26, nlp = -2058.004686
2022-12-21 10:41:43,985:INFO:root:Gibbs Iter 190, no. changed = 28, nlp = -2055.074045
2022-12-21 10:41:44,927:INFO:root:Gibbs Iter 200, no. changed = 27, nlp = -2055.280115
2022-12-21 10:41:45,873:INFO:root:Gibbs Iter 210, no. changed = 28, nlp = -2043.957565
2022-12-21 10:41:46,816:INFO:root:Gibbs Iter 220, no. changed = 26, nlp = -2047.977363
2022-12-21 10:41:47,756:INFO:root:Gibbs Iter 230, no. changed = 27, nlp = -2039.027600
2022-12-21 10:41:48,697:INFO:root:Gibbs Iter 240, no. changed = 29, nlp = -2064.558529
2022-12-21 10:41:49,632:INFO:root:Gibbs Iter 250, no. changed = 28, nlp = -2059.019916
2022-12-21 10:41:50,566:INFO:root:Gibbs Iter 260, no. changed = 27, nlp = -2061.468431
2022-12-21 10:41:51,500:INFO:root:Gibbs Iter 270, no. changed = 26, nlp = -2060.755325
2022-12-21 10:41:52,435:INFO:root:Gibbs Iter 280, no. changed = 27, nlp = -2062.444615
2022-12-21 10:41:53,369:INFO:root:Gibbs Iter 290, no. changed = 28, nlp = -2064.717078
2022-12-21 10:41:54,307:INFO:root:Gibbs Iter 300, no. changed = 26, nlp = -2060.689064
2022-12-21 10:41:55,244:INFO:root:Gibbs Iter 310, no. changed = 27, nlp = -2043.170731
2022-12-21 10:41:56,182:INFO:root:Gibbs Iter 320, no. changed = 28, nlp = -2034.411532
2022-12-21 10:41:57,126:INFO:root:Gibbs Iter 330, no. changed = 29, nlp = -2067.345271
2022-12-21 10:41:58,073:INFO:root:Gibbs Iter 340, no. changed = 26, nlp = -2060.730268
2022-12-21 10:41:59,014:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -2062.351920
2022-12-21 10:41:59,959:INFO:root:Gibbs Iter 360, no. changed = 27, nlp = -2049.129552
2022-12-21 10:42:00,904:INFO:root:Gibbs Iter 370, no. changed = 27, nlp = -2047.527824
2022-12-21 10:42:01,847:INFO:root:Gibbs Iter 380, no. changed = 28, nlp = -2052.586049
2022-12-21 10:42:02,790:INFO:root:Gibbs Iter 390, no. changed = 28, nlp = -2057.102302
2022-12-21 10:42:03,734:INFO:root:Gibbs Iter 400, no. changed = 28, nlp = -2037.826443
2022-12-21 10:42:04,677:INFO:root:Gibbs Iter 410, no. changed = 26, nlp = -2051.786021
2022-12-21 10:42:05,622:INFO:root:Gibbs Iter 420, no. changed = 28, nlp = -2040.908897
2022-12-21 10:42:06,567:INFO:root:Gibbs Iter 430, no. changed = 29, nlp = -2040.121652
2022-12-21 10:42:07,510:INFO:root:Gibbs Iter 440, no. changed = 27, nlp = -2046.059775
2022-12-21 10:42:08,454:INFO:root:Gibbs Iter 450, no. changed = 27, nlp = -2031.128296
2022-12-21 10:42:09,397:INFO:root:Gibbs Iter 460, no. changed = 27, nlp = -2053.565931
2022-12-21 10:42:10,343:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2058.519448
2022-12-21 10:42:11,287:INFO:root:Gibbs Iter 480, no. changed = 27, nlp = -2068.785877
2022-12-21 10:42:12,228:INFO:root:Gibbs Iter 490, no. changed = 30, nlp = -2062.637042
2022-12-21 10:42:13,172:INFO:root:Gibbs Iter 500, no. changed = 27, nlp = -2052.776359
2022-12-21 10:42:14,117:INFO:root:Gibbs Iter 510, no. changed = 28, nlp = -2043.095924
2022-12-21 10:42:15,059:INFO:root:Gibbs Iter 520, no. changed = 28, nlp = -2006.760135
2022-12-21 10:42:16,003:INFO:root:Gibbs Iter 530, no. changed = 29, nlp = -2060.812360
2022-12-21 10:42:16,946:INFO:root:Gibbs Iter 540, no. changed = 25, nlp = -2058.503140
2022-12-21 10:42:17,889:INFO:root:Gibbs Iter 550, no. changed = 27, nlp = -2037.064140
2022-12-21 10:42:18,834:INFO:root:Gibbs Iter 560, no. changed = 28, nlp = -2061.329539
2022-12-21 10:42:19,770:INFO:root:Gibbs Iter 570, no. changed = 27, nlp = -2063.350532
2022-12-21 10:42:20,708:INFO:root:Gibbs Iter 580, no. changed = 26, nlp = -2056.616795
2022-12-21 10:42:21,645:INFO:root:Gibbs Iter 590, no. changed = 28, nlp = -2053.806370
2022-12-21 10:42:22,581:INFO:root:Gibbs Iter 600, no. changed = 27, nlp = -2064.767687
2022-12-21 10:42:23,514:INFO:root:Gibbs Iter 610, no. changed = 29, nlp = -2064.490987
2022-12-21 10:42:24,452:INFO:root:Gibbs Iter 620, no. changed = 28, nlp = -2067.912885
2022-12-21 10:42:25,387:INFO:root:Gibbs Iter 630, no. changed = 28, nlp = -2043.260916
2022-12-21 10:42:26,323:INFO:root:Gibbs Iter 640, no. changed = 27, nlp = -2052.605688
2022-12-21 10:42:27,258:INFO:root:Gibbs Iter 650, no. changed = 26, nlp = -2065.284749
2022-12-21 10:42:28,192:INFO:root:Gibbs Iter 660, no. changed = 27, nlp = -2040.082774
2022-12-21 10:42:29,128:INFO:root:Gibbs Iter 670, no. changed = 28, nlp = -2047.939950
2022-12-21 10:42:30,062:INFO:root:Gibbs Iter 680, no. changed = 28, nlp = -2050.686166
2022-12-21 10:42:30,999:INFO:root:Gibbs Iter 690, no. changed = 25, nlp = -2067.318704
2022-12-21 10:42:31,933:INFO:root:Gibbs Iter 700, no. changed = 28, nlp = -2039.545161
2022-12-21 10:42:32,866:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2049.450934
2022-12-21 10:42:33,803:INFO:root:Gibbs Iter 720, no. changed = 27, nlp = -2065.325588
2022-12-21 10:42:34,741:INFO:root:Gibbs Iter 730, no. changed = 27, nlp = -2036.317592
2022-12-21 10:42:35,677:INFO:root:Gibbs Iter 740, no. changed = 28, nlp = -2065.304457
2022-12-21 10:42:36,614:INFO:root:Gibbs Iter 750, no. changed = 28, nlp = -2056.161535
2022-12-21 10:42:37,550:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2045.223529
2022-12-21 10:42:38,488:INFO:root:Gibbs Iter 770, no. changed = 28, nlp = -2052.536391
2022-12-21 10:42:39,423:INFO:root:Gibbs Iter 780, no. changed = 28, nlp = -2053.016714
2022-12-21 10:42:40,359:INFO:root:Gibbs Iter 790, no. changed = 28, nlp = -2062.085317
2022-12-21 10:42:41,295:INFO:root:Gibbs Iter 800, no. changed = 26, nlp = -2047.465476
2022-12-21 10:42:42,230:INFO:root:Gibbs Iter 810, no. changed = 28, nlp = -2051.753142
2022-12-21 10:42:43,168:INFO:root:Gibbs Iter 820, no. changed = 27, nlp = -2060.183479
2022-12-21 10:42:44,104:INFO:root:Gibbs Iter 830, no. changed = 28, nlp = -2062.761007
2022-12-21 10:42:45,039:INFO:root:Gibbs Iter 840, no. changed = 27, nlp = -2034.627257
2022-12-21 10:42:45,972:INFO:root:Gibbs Iter 850, no. changed = 28, nlp = -2048.657467
2022-12-21 10:42:46,908:INFO:root:Gibbs Iter 860, no. changed = 27, nlp = -2035.287789
2022-12-21 10:42:47,844:INFO:root:Gibbs Iter 870, no. changed = 30, nlp = -2026.377369
2022-12-21 10:42:48,790:INFO:root:Gibbs Iter 880, no. changed = 29, nlp = -2054.264868
2022-12-21 10:42:49,727:INFO:root:Gibbs Iter 890, no. changed = 28, nlp = -2055.407580
2022-12-21 10:42:50,664:INFO:root:Gibbs Iter 900, no. changed = 27, nlp = -2022.346219
2022-12-21 10:42:51,600:INFO:root:Gibbs Iter 910, no. changed = 28, nlp = -2040.563978
2022-12-21 10:42:52,538:INFO:root:Gibbs Iter 920, no. changed = 26, nlp = -2024.872585
2022-12-21 10:42:53,475:INFO:root:Gibbs Iter 930, no. changed = 28, nlp = -2064.885915
2022-12-21 10:42:54,409:INFO:root:Gibbs Iter 940, no. changed = 27, nlp = -2065.791543
2022-12-21 10:42:55,345:INFO:root:Gibbs Iter 950, no. changed = 29, nlp = -2051.073396
2022-12-21 10:42:56,284:INFO:root:Gibbs Iter 960, no. changed = 28, nlp = -2058.778702
2022-12-21 10:42:57,220:INFO:root:Gibbs Iter 970, no. changed = 28, nlp = -2052.470474
2022-12-21 10:42:58,160:INFO:root:Gibbs Iter 980, no. changed = 28, nlp = -2060.647570
2022-12-21 10:42:59,097:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2045.184078
2022-12-21 10:43:00,001:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 10:43:00,115:INFO:root:Gibbs Iter 0, no. changed = 28, nlp = -2022.400250
2022-12-21 10:43:01,053:INFO:root:Gibbs Iter 10, no. changed = 28, nlp = -2052.503110
2022-12-21 10:43:01,988:INFO:root:Gibbs Iter 20, no. changed = 28, nlp = -2060.924569
2022-12-21 10:43:02,922:INFO:root:Gibbs Iter 30, no. changed = 27, nlp = -2062.473791
2022-12-21 10:43:03,856:INFO:root:Gibbs Iter 40, no. changed = 27, nlp = -2053.979655
2022-12-21 10:43:04,788:INFO:root:Gibbs Iter 50, no. changed = 28, nlp = -2020.860051
2022-12-21 10:43:05,719:INFO:root:Gibbs Iter 60, no. changed = 28, nlp = -2070.314160
2022-12-21 10:43:06,656:INFO:root:Gibbs Iter 70, no. changed = 28, nlp = -2049.272206
2022-12-21 10:43:07,591:INFO:root:Gibbs Iter 80, no. changed = 27, nlp = -2061.013823
2022-12-21 10:43:08,524:INFO:root:Gibbs Iter 90, no. changed = 29, nlp = -2050.045006
2022-12-21 10:43:09,455:INFO:root:Gibbs Iter 100, no. changed = 28, nlp = -2052.542548
2022-12-21 10:43:10,390:INFO:root:Gibbs Iter 110, no. changed = 29, nlp = -2061.565872
2022-12-21 10:43:11,325:INFO:root:Gibbs Iter 120, no. changed = 30, nlp = -2023.039446
2022-12-21 10:43:12,257:INFO:root:Gibbs Iter 130, no. changed = 28, nlp = -2050.757092
2022-12-21 10:43:13,188:INFO:root:Gibbs Iter 140, no. changed = 27, nlp = -2056.006839
2022-12-21 10:43:14,123:INFO:root:Gibbs Iter 150, no. changed = 28, nlp = -2044.993422
2022-12-21 10:43:15,063:INFO:root:Gibbs Iter 160, no. changed = 29, nlp = -2056.359194
2022-12-21 10:43:15,997:INFO:root:Gibbs Iter 170, no. changed = 28, nlp = -2054.377598
2022-12-21 10:43:16,932:INFO:root:Gibbs Iter 180, no. changed = 28, nlp = -2047.242036
2022-12-21 10:43:17,866:INFO:root:Gibbs Iter 190, no. changed = 26, nlp = -2062.838825
2022-12-21 10:43:18,801:INFO:root:Gibbs Iter 200, no. changed = 28, nlp = -2063.226539
2022-12-21 10:43:19,737:INFO:root:Gibbs Iter 210, no. changed = 27, nlp = -2015.053871
2022-12-21 10:43:20,671:INFO:root:Gibbs Iter 220, no. changed = 25, nlp = -2042.463610
2022-12-21 10:43:21,606:INFO:root:Gibbs Iter 230, no. changed = 28, nlp = -2037.440412
2022-12-21 10:43:22,538:INFO:root:Gibbs Iter 240, no. changed = 29, nlp = -2053.282246
2022-12-21 10:43:23,469:INFO:root:Gibbs Iter 250, no. changed = 27, nlp = -2051.106032
2022-12-21 10:43:24,401:INFO:root:Gibbs Iter 260, no. changed = 27, nlp = -2043.244792
2022-12-21 10:43:25,335:INFO:root:Gibbs Iter 270, no. changed = 27, nlp = -2058.555450
2022-12-21 10:43:26,269:INFO:root:Gibbs Iter 280, no. changed = 27, nlp = -2036.883303
2022-12-21 10:43:27,206:INFO:root:Gibbs Iter 290, no. changed = 26, nlp = -2059.076889
2022-12-21 10:43:28,141:INFO:root:Gibbs Iter 300, no. changed = 27, nlp = -2066.069527
2022-12-21 10:43:29,078:INFO:root:Gibbs Iter 310, no. changed = 26, nlp = -2021.760200
2022-12-21 10:43:30,012:INFO:root:Gibbs Iter 320, no. changed = 28, nlp = -2064.405341
2022-12-21 10:43:30,949:INFO:root:Gibbs Iter 330, no. changed = 28, nlp = -2064.717705
2022-12-21 10:43:31,886:INFO:root:Gibbs Iter 340, no. changed = 29, nlp = -2057.583126
2022-12-21 10:43:32,821:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -2051.746714
2022-12-21 10:43:33,754:INFO:root:Gibbs Iter 360, no. changed = 25, nlp = -2063.559917
2022-12-21 10:43:34,689:INFO:root:Gibbs Iter 370, no. changed = 27, nlp = -2057.922664
2022-12-21 10:43:35,623:INFO:root:Gibbs Iter 380, no. changed = 27, nlp = -2028.411560
2022-12-21 10:43:36,560:INFO:root:Gibbs Iter 390, no. changed = 28, nlp = -2059.475395
2022-12-21 10:43:37,493:INFO:root:Gibbs Iter 400, no. changed = 28, nlp = -2034.298308
2022-12-21 10:43:38,424:INFO:root:Gibbs Iter 410, no. changed = 27, nlp = -2064.432122
2022-12-21 10:43:39,357:INFO:root:Gibbs Iter 420, no. changed = 28, nlp = -2057.570507
2022-12-21 10:43:40,292:INFO:root:Gibbs Iter 430, no. changed = 28, nlp = -2046.338281
2022-12-21 10:43:41,226:INFO:root:Gibbs Iter 440, no. changed = 28, nlp = -2055.744470
2022-12-21 10:43:42,161:INFO:root:Gibbs Iter 450, no. changed = 29, nlp = -2045.178273
2022-12-21 10:43:43,097:INFO:root:Gibbs Iter 460, no. changed = 29, nlp = -2036.588791
2022-12-21 10:43:44,032:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2065.966663
2022-12-21 10:43:44,966:INFO:root:Gibbs Iter 480, no. changed = 25, nlp = -2072.619227
2022-12-21 10:43:45,899:INFO:root:Gibbs Iter 490, no. changed = 29, nlp = -2041.338005
2022-12-21 10:43:46,838:INFO:root:Gibbs Iter 500, no. changed = 26, nlp = -2052.731890
2022-12-21 10:43:47,774:INFO:root:Gibbs Iter 510, no. changed = 29, nlp = -2041.039256
2022-12-21 10:43:48,708:INFO:root:Gibbs Iter 520, no. changed = 28, nlp = -2060.125600
2022-12-21 10:43:49,642:INFO:root:Gibbs Iter 530, no. changed = 27, nlp = -2037.915785
2022-12-21 10:43:50,575:INFO:root:Gibbs Iter 540, no. changed = 28, nlp = -2052.798621
2022-12-21 10:43:51,509:INFO:root:Gibbs Iter 550, no. changed = 28, nlp = -2052.327854
2022-12-21 10:43:52,440:INFO:root:Gibbs Iter 560, no. changed = 28, nlp = -1991.300978
2022-12-21 10:43:53,374:INFO:root:Gibbs Iter 570, no. changed = 27, nlp = -2073.347175
2022-12-21 10:43:54,306:INFO:root:Gibbs Iter 580, no. changed = 29, nlp = -2058.415460
2022-12-21 10:43:55,242:INFO:root:Gibbs Iter 590, no. changed = 27, nlp = -2018.672494
2022-12-21 10:43:56,176:INFO:root:Gibbs Iter 600, no. changed = 29, nlp = -2061.333888
2022-12-21 10:43:57,108:INFO:root:Gibbs Iter 610, no. changed = 29, nlp = -2055.024191
2022-12-21 10:43:58,041:INFO:root:Gibbs Iter 620, no. changed = 29, nlp = -2044.441312
2022-12-21 10:43:58,974:INFO:root:Gibbs Iter 630, no. changed = 24, nlp = -2064.568017
2022-12-21 10:43:59,906:INFO:root:Gibbs Iter 640, no. changed = 28, nlp = -2011.632935
2022-12-21 10:44:00,840:INFO:root:Gibbs Iter 650, no. changed = 29, nlp = -2058.214603
2022-12-21 10:44:01,775:INFO:root:Gibbs Iter 660, no. changed = 27, nlp = -2038.631954
2022-12-21 10:44:02,709:INFO:root:Gibbs Iter 670, no. changed = 28, nlp = -2050.459331
2022-12-21 10:44:03,643:INFO:root:Gibbs Iter 680, no. changed = 28, nlp = -2054.901188
2022-12-21 10:44:04,577:INFO:root:Gibbs Iter 690, no. changed = 25, nlp = -2052.243747
2022-12-21 10:44:05,512:INFO:root:Gibbs Iter 700, no. changed = 28, nlp = -2057.714891
2022-12-21 10:44:06,445:INFO:root:Gibbs Iter 710, no. changed = 29, nlp = -2043.591204
2022-12-21 10:44:07,379:INFO:root:Gibbs Iter 720, no. changed = 28, nlp = -2052.213321
2022-12-21 10:44:08,310:INFO:root:Gibbs Iter 730, no. changed = 29, nlp = -2042.381935
2022-12-21 10:44:09,243:INFO:root:Gibbs Iter 740, no. changed = 28, nlp = -2027.596437
2022-12-21 10:44:10,177:INFO:root:Gibbs Iter 750, no. changed = 27, nlp = -2067.737685
2022-12-21 10:44:11,112:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2067.032727
2022-12-21 10:44:12,045:INFO:root:Gibbs Iter 770, no. changed = 28, nlp = -2060.624539
2022-12-21 10:44:12,978:INFO:root:Gibbs Iter 780, no. changed = 27, nlp = -2061.215447
2022-12-21 10:44:13,911:INFO:root:Gibbs Iter 790, no. changed = 28, nlp = -2049.171050
2022-12-21 10:44:14,845:INFO:root:Gibbs Iter 800, no. changed = 27, nlp = -2048.989028
2022-12-21 10:44:15,779:INFO:root:Gibbs Iter 810, no. changed = 30, nlp = -2061.224825
2022-12-21 10:44:16,712:INFO:root:Gibbs Iter 820, no. changed = 28, nlp = -2046.674999
2022-12-21 10:44:17,643:INFO:root:Gibbs Iter 830, no. changed = 28, nlp = -2068.999066
2022-12-21 10:44:18,577:INFO:root:Gibbs Iter 840, no. changed = 27, nlp = -2055.990796
2022-12-21 10:44:19,510:INFO:root:Gibbs Iter 850, no. changed = 29, nlp = -2046.802430
2022-12-21 10:44:20,442:INFO:root:Gibbs Iter 860, no. changed = 30, nlp = -2013.098219
2022-12-21 10:44:21,375:INFO:root:Gibbs Iter 870, no. changed = 27, nlp = -2047.808732
2022-12-21 10:44:22,308:INFO:root:Gibbs Iter 880, no. changed = 28, nlp = -2062.729758
2022-12-21 10:44:23,244:INFO:root:Gibbs Iter 890, no. changed = 26, nlp = -2063.570690
2022-12-21 10:44:24,177:INFO:root:Gibbs Iter 900, no. changed = 27, nlp = -2067.248390
2022-12-21 10:44:25,110:INFO:root:Gibbs Iter 910, no. changed = 28, nlp = -2061.546020
2022-12-21 10:44:26,044:INFO:root:Gibbs Iter 920, no. changed = 27, nlp = -2068.118362
2022-12-21 10:44:26,981:INFO:root:Gibbs Iter 930, no. changed = 29, nlp = -2039.609537
2022-12-21 10:44:27,915:INFO:root:Gibbs Iter 940, no. changed = 28, nlp = -2042.500501
2022-12-21 10:44:28,848:INFO:root:Gibbs Iter 950, no. changed = 29, nlp = -2065.001106
2022-12-21 10:44:29,780:INFO:root:Gibbs Iter 960, no. changed = 27, nlp = -2054.862836
2022-12-21 10:44:30,714:INFO:root:Gibbs Iter 970, no. changed = 30, nlp = -2042.026635
2022-12-21 10:44:31,649:INFO:root:Gibbs Iter 980, no. changed = 29, nlp = -2073.380189
2022-12-21 10:44:32,584:INFO:root:Gibbs Iter 990, no. changed = 27, nlp = -2065.253471
2022-12-21 10:44:33,432:INFO:root:Wrote fit stats
2022-12-21 10:44:33,438:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 10:44:33,444:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 10:44:33,447:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 10:44:33,485:INFO:root:Wrote ll_store
2022-12-21 10:44:33,490:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 10:44:33,492:INFO:root:Wrote transition error matrix
2022-12-21 10:44:33,495:INFO:root:Wrote transition error matrix
2022-12-21 10:44:33,499:INFO:root:Wrote selected variants
