2022-12-21 11:11:50,396:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.4.4
2022-12-21 11:11:50,396:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 11:11:50,435:INFO:root:Running Desman with 9 samples and 94 variant positions finding 4 genomes.
2022-12-21 11:11:50,435:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 11:11:50,436:INFO:root:Set second adjustable random seed = 4
2022-12-21 11:11:50,437:INFO:root:Perform NTF initialisation
2022-12-21 11:11:50,441:INFO:root:NTF Iter 0, div = 561.256914
2022-12-21 11:11:50,597:INFO:root:NTF Iter 100, div = 34.757088
2022-12-21 11:11:50,769:INFO:root:NTF Iter 200, div = 32.441613
2022-12-21 11:11:50,937:INFO:root:NTF Iter 300, div = 28.279360
2022-12-21 11:11:51,105:INFO:root:NTF Iter 400, div = 24.727984
2022-12-21 11:11:51,274:INFO:root:NTF Iter 500, div = 21.294931
2022-12-21 11:11:51,445:INFO:root:NTF Iter 600, div = 19.634276
2022-12-21 11:11:51,618:INFO:root:NTF Iter 700, div = 18.035410
2022-12-21 11:11:51,788:INFO:root:NTF Iter 800, div = 16.259235
2022-12-21 11:11:51,954:INFO:root:NTF Iter 900, div = 15.055657
2022-12-21 11:11:52,124:INFO:root:NTF Iter 1000, div = 14.159185
2022-12-21 11:11:52,295:INFO:root:NTF Iter 1100, div = 13.461076
2022-12-21 11:11:52,463:INFO:root:NTF Iter 1200, div = 12.908913
2022-12-21 11:11:52,631:INFO:root:NTF Iter 1300, div = 12.460356
2022-12-21 11:11:52,797:INFO:root:NTF Iter 1400, div = 12.240901
2022-12-21 11:11:52,939:INFO:root:NTF Iter 1500, div = 12.064844
2022-12-21 11:11:53,080:INFO:root:NTF Iter 1600, div = 11.895145
2022-12-21 11:11:53,218:INFO:root:NTF Iter 1700, div = 11.792138
2022-12-21 11:11:53,360:INFO:root:NTF Iter 1800, div = 11.733609
2022-12-21 11:11:53,501:INFO:root:NTF Iter 1900, div = 11.666159
2022-12-21 11:11:53,640:INFO:root:NTF Iter 2000, div = 11.631529
2022-12-21 11:11:53,780:INFO:root:NTF Iter 2100, div = 11.600663
2022-12-21 11:11:53,918:INFO:root:NTF Iter 2200, div = 11.554241
2022-12-21 11:11:54,058:INFO:root:NTF Iter 2300, div = 11.523393
2022-12-21 11:11:54,199:INFO:root:NTF Iter 2400, div = 11.509463
2022-12-21 11:11:54,339:INFO:root:NTF Iter 2500, div = 11.491821
2022-12-21 11:11:54,482:INFO:root:NTF Iter 2600, div = 11.476576
2022-12-21 11:11:54,621:INFO:root:NTF Iter 2700, div = 11.440083
2022-12-21 11:11:54,762:INFO:root:NTF Iter 2800, div = 11.417065
2022-12-21 11:11:54,902:INFO:root:NTF Iter 2900, div = 11.404748
2022-12-21 11:11:55,042:INFO:root:NTF Iter 3000, div = 11.396061
2022-12-21 11:11:55,182:INFO:root:NTF Iter 3100, div = 11.388395
2022-12-21 11:11:55,322:INFO:root:NTF Iter 3200, div = 11.371739
2022-12-21 11:11:55,463:INFO:root:NTF Iter 3300, div = 11.359655
2022-12-21 11:11:55,603:INFO:root:NTF Iter 3400, div = 11.346651
2022-12-21 11:11:55,742:INFO:root:NTF Iter 3500, div = 11.341536
2022-12-21 11:11:55,882:INFO:root:NTF Iter 3600, div = 11.338533
2022-12-21 11:11:56,021:INFO:root:NTF Iter 3700, div = 11.333504
2022-12-21 11:11:56,162:INFO:root:NTF Iter 3800, div = 11.324612
2022-12-21 11:11:56,303:INFO:root:NTF Iter 3900, div = 11.320540
2022-12-21 11:11:56,442:INFO:root:NTF Iter 4000, div = 11.317934
2022-12-21 11:11:56,582:INFO:root:NTF Iter 4100, div = 11.316041
2022-12-21 11:11:56,720:INFO:root:NTF Iter 4200, div = 11.314604
2022-12-21 11:11:56,861:INFO:root:NTF Iter 4300, div = 11.313189
2022-12-21 11:11:56,967:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 11:11:57,081:INFO:root:Gibbs Iter 0, no. changed = 49, nlp = -2255.470736
2022-12-21 11:11:58,025:INFO:root:Gibbs Iter 10, no. changed = 35, nlp = -2166.022200
2022-12-21 11:11:58,972:INFO:root:Gibbs Iter 20, no. changed = 38, nlp = -2139.436599
2022-12-21 11:11:59,917:INFO:root:Gibbs Iter 30, no. changed = 30, nlp = -2110.736745
2022-12-21 11:12:00,863:INFO:root:Gibbs Iter 40, no. changed = 32, nlp = -2143.822969
2022-12-21 11:12:01,810:INFO:root:Gibbs Iter 50, no. changed = 40, nlp = -2123.464748
2022-12-21 11:12:02,760:INFO:root:Gibbs Iter 60, no. changed = 34, nlp = -2090.340855
2022-12-21 11:12:03,707:INFO:root:Gibbs Iter 70, no. changed = 30, nlp = -2149.734241
2022-12-21 11:12:04,662:INFO:root:Gibbs Iter 80, no. changed = 38, nlp = -2122.916342
2022-12-21 11:12:05,608:INFO:root:Gibbs Iter 90, no. changed = 39, nlp = -2153.907689
2022-12-21 11:12:06,560:INFO:root:Gibbs Iter 100, no. changed = 36, nlp = -2119.900410
2022-12-21 11:12:07,508:INFO:root:Gibbs Iter 110, no. changed = 41, nlp = -2141.362956
2022-12-21 11:12:08,454:INFO:root:Gibbs Iter 120, no. changed = 35, nlp = -2128.108138
2022-12-21 11:12:09,400:INFO:root:Gibbs Iter 130, no. changed = 39, nlp = -2139.488699
2022-12-21 11:12:10,346:INFO:root:Gibbs Iter 140, no. changed = 36, nlp = -2133.632243
2022-12-21 11:12:11,295:INFO:root:Gibbs Iter 150, no. changed = 37, nlp = -2123.397320
2022-12-21 11:12:12,239:INFO:root:Gibbs Iter 160, no. changed = 36, nlp = -2158.259094
2022-12-21 11:12:13,184:INFO:root:Gibbs Iter 170, no. changed = 34, nlp = -2114.227820
2022-12-21 11:12:14,131:INFO:root:Gibbs Iter 180, no. changed = 32, nlp = -2127.161855
2022-12-21 11:12:15,080:INFO:root:Gibbs Iter 190, no. changed = 33, nlp = -2122.429847
2022-12-21 11:12:16,028:INFO:root:Gibbs Iter 200, no. changed = 36, nlp = -2138.340327
2022-12-21 11:12:16,976:INFO:root:Gibbs Iter 210, no. changed = 39, nlp = -2107.535587
2022-12-21 11:12:17,923:INFO:root:Gibbs Iter 220, no. changed = 35, nlp = -2135.431147
2022-12-21 11:12:18,871:INFO:root:Gibbs Iter 230, no. changed = 41, nlp = -2134.243091
2022-12-21 11:12:19,815:INFO:root:Gibbs Iter 240, no. changed = 29, nlp = -2131.010942
2022-12-21 11:12:20,762:INFO:root:Gibbs Iter 250, no. changed = 33, nlp = -2137.030719
2022-12-21 11:12:21,708:INFO:root:Gibbs Iter 260, no. changed = 31, nlp = -2154.739466
2022-12-21 11:12:22,656:INFO:root:Gibbs Iter 270, no. changed = 38, nlp = -2128.273574
2022-12-21 11:12:23,606:INFO:root:Gibbs Iter 280, no. changed = 38, nlp = -2130.246452
2022-12-21 11:12:24,554:INFO:root:Gibbs Iter 290, no. changed = 38, nlp = -2119.503357
2022-12-21 11:12:25,501:INFO:root:Gibbs Iter 300, no. changed = 32, nlp = -2129.008084
2022-12-21 11:12:26,448:INFO:root:Gibbs Iter 310, no. changed = 33, nlp = -2121.502686
2022-12-21 11:12:27,397:INFO:root:Gibbs Iter 320, no. changed = 35, nlp = -2109.089851
2022-12-21 11:12:28,344:INFO:root:Gibbs Iter 330, no. changed = 36, nlp = -2129.118811
2022-12-21 11:12:29,289:INFO:root:Gibbs Iter 340, no. changed = 35, nlp = -2144.515299
2022-12-21 11:12:30,234:INFO:root:Gibbs Iter 350, no. changed = 40, nlp = -2124.526458
2022-12-21 11:12:31,182:INFO:root:Gibbs Iter 360, no. changed = 36, nlp = -2156.570972
2022-12-21 11:12:32,128:INFO:root:Gibbs Iter 370, no. changed = 33, nlp = -2130.729920
2022-12-21 11:12:33,074:INFO:root:Gibbs Iter 380, no. changed = 34, nlp = -2126.969568
2022-12-21 11:12:34,021:INFO:root:Gibbs Iter 390, no. changed = 26, nlp = -2139.186336
2022-12-21 11:12:34,969:INFO:root:Gibbs Iter 400, no. changed = 34, nlp = -2130.964179
2022-12-21 11:12:35,914:INFO:root:Gibbs Iter 410, no. changed = 36, nlp = -2131.157445
2022-12-21 11:12:36,860:INFO:root:Gibbs Iter 420, no. changed = 29, nlp = -2141.107402
2022-12-21 11:12:37,805:INFO:root:Gibbs Iter 430, no. changed = 36, nlp = -2122.373417
2022-12-21 11:12:38,752:INFO:root:Gibbs Iter 440, no. changed = 31, nlp = -2133.048631
2022-12-21 11:12:39,698:INFO:root:Gibbs Iter 450, no. changed = 35, nlp = -2132.005081
2022-12-21 11:12:40,646:INFO:root:Gibbs Iter 460, no. changed = 30, nlp = -2110.970878
2022-12-21 11:12:41,593:INFO:root:Gibbs Iter 470, no. changed = 33, nlp = -2140.595917
2022-12-21 11:12:42,539:INFO:root:Gibbs Iter 480, no. changed = 32, nlp = -2141.453279
2022-12-21 11:12:43,487:INFO:root:Gibbs Iter 490, no. changed = 30, nlp = -2145.140322
2022-12-21 11:12:44,432:INFO:root:Gibbs Iter 500, no. changed = 29, nlp = -2139.079851
2022-12-21 11:12:45,377:INFO:root:Gibbs Iter 510, no. changed = 36, nlp = -2145.134400
2022-12-21 11:12:46,327:INFO:root:Gibbs Iter 520, no. changed = 31, nlp = -2155.632969
2022-12-21 11:12:47,275:INFO:root:Gibbs Iter 530, no. changed = 35, nlp = -2134.998539
2022-12-21 11:12:48,223:INFO:root:Gibbs Iter 540, no. changed = 30, nlp = -2130.823766
2022-12-21 11:12:49,172:INFO:root:Gibbs Iter 550, no. changed = 37, nlp = -2143.084165
2022-12-21 11:12:50,122:INFO:root:Gibbs Iter 560, no. changed = 34, nlp = -2150.103844
2022-12-21 11:12:51,072:INFO:root:Gibbs Iter 570, no. changed = 33, nlp = -2135.570580
2022-12-21 11:12:52,021:INFO:root:Gibbs Iter 580, no. changed = 35, nlp = -2133.614857
2022-12-21 11:12:52,970:INFO:root:Gibbs Iter 590, no. changed = 30, nlp = -2117.693286
2022-12-21 11:12:53,920:INFO:root:Gibbs Iter 600, no. changed = 31, nlp = -2148.046354
2022-12-21 11:12:54,869:INFO:root:Gibbs Iter 610, no. changed = 33, nlp = -2125.745638
2022-12-21 11:12:55,817:INFO:root:Gibbs Iter 620, no. changed = 32, nlp = -2133.781991
2022-12-21 11:12:56,768:INFO:root:Gibbs Iter 630, no. changed = 36, nlp = -2091.899705
2022-12-21 11:12:57,718:INFO:root:Gibbs Iter 640, no. changed = 33, nlp = -2143.255121
2022-12-21 11:12:58,669:INFO:root:Gibbs Iter 650, no. changed = 34, nlp = -2131.858261
2022-12-21 11:12:59,620:INFO:root:Gibbs Iter 660, no. changed = 36, nlp = -2131.076526
2022-12-21 11:13:00,565:INFO:root:Gibbs Iter 670, no. changed = 31, nlp = -2119.039493
2022-12-21 11:13:01,511:INFO:root:Gibbs Iter 680, no. changed = 30, nlp = -2104.787048
2022-12-21 11:13:02,460:INFO:root:Gibbs Iter 690, no. changed = 32, nlp = -2146.100004
2022-12-21 11:13:03,408:INFO:root:Gibbs Iter 700, no. changed = 30, nlp = -2140.025245
2022-12-21 11:13:04,362:INFO:root:Gibbs Iter 710, no. changed = 33, nlp = -2143.865188
2022-12-21 11:13:05,311:INFO:root:Gibbs Iter 720, no. changed = 36, nlp = -2158.739072
2022-12-21 11:13:06,261:INFO:root:Gibbs Iter 730, no. changed = 37, nlp = -2166.243567
2022-12-21 11:13:07,211:INFO:root:Gibbs Iter 740, no. changed = 35, nlp = -2146.754247
2022-12-21 11:13:08,157:INFO:root:Gibbs Iter 750, no. changed = 32, nlp = -2142.561563
2022-12-21 11:13:09,104:INFO:root:Gibbs Iter 760, no. changed = 41, nlp = -2141.348426
2022-12-21 11:13:10,055:INFO:root:Gibbs Iter 770, no. changed = 33, nlp = -2152.742348
2022-12-21 11:13:11,003:INFO:root:Gibbs Iter 780, no. changed = 31, nlp = -2122.561817
2022-12-21 11:13:11,951:INFO:root:Gibbs Iter 790, no. changed = 37, nlp = -2136.168937
2022-12-21 11:13:12,899:INFO:root:Gibbs Iter 800, no. changed = 32, nlp = -2115.456737
2022-12-21 11:13:13,848:INFO:root:Gibbs Iter 810, no. changed = 31, nlp = -2140.747383
2022-12-21 11:13:14,797:INFO:root:Gibbs Iter 820, no. changed = 37, nlp = -2125.142597
2022-12-21 11:13:15,748:INFO:root:Gibbs Iter 830, no. changed = 34, nlp = -2135.080498
2022-12-21 11:13:16,698:INFO:root:Gibbs Iter 840, no. changed = 45, nlp = -2129.468687
2022-12-21 11:13:17,648:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2130.903316
2022-12-21 11:13:18,596:INFO:root:Gibbs Iter 860, no. changed = 30, nlp = -2149.689498
2022-12-21 11:13:19,544:INFO:root:Gibbs Iter 870, no. changed = 33, nlp = -2143.810162
2022-12-21 11:13:20,493:INFO:root:Gibbs Iter 880, no. changed = 26, nlp = -2147.932677
2022-12-21 11:13:21,443:INFO:root:Gibbs Iter 890, no. changed = 33, nlp = -2140.793930
2022-12-21 11:13:22,392:INFO:root:Gibbs Iter 900, no. changed = 37, nlp = -2137.193414
2022-12-21 11:13:23,339:INFO:root:Gibbs Iter 910, no. changed = 30, nlp = -2134.249252
2022-12-21 11:13:24,288:INFO:root:Gibbs Iter 920, no. changed = 36, nlp = -2141.594840
2022-12-21 11:13:25,237:INFO:root:Gibbs Iter 930, no. changed = 32, nlp = -2134.352703
2022-12-21 11:13:26,187:INFO:root:Gibbs Iter 940, no. changed = 36, nlp = -2134.152025
2022-12-21 11:13:27,136:INFO:root:Gibbs Iter 950, no. changed = 32, nlp = -2127.386179
2022-12-21 11:13:28,087:INFO:root:Gibbs Iter 960, no. changed = 37, nlp = -2135.719499
2022-12-21 11:13:29,035:INFO:root:Gibbs Iter 970, no. changed = 33, nlp = -2130.229151
2022-12-21 11:13:29,986:INFO:root:Gibbs Iter 980, no. changed = 29, nlp = -2148.394220
2022-12-21 11:13:30,937:INFO:root:Gibbs Iter 990, no. changed = 39, nlp = -2143.455008
2022-12-21 11:13:31,876:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 11:13:31,992:INFO:root:Gibbs Iter 0, no. changed = 36, nlp = -2142.938461
2022-12-21 11:13:32,940:INFO:root:Gibbs Iter 10, no. changed = 32, nlp = -2132.772825
2022-12-21 11:13:33,892:INFO:root:Gibbs Iter 20, no. changed = 36, nlp = -2143.808374
2022-12-21 11:13:34,839:INFO:root:Gibbs Iter 30, no. changed = 35, nlp = -2136.072136
2022-12-21 11:13:35,781:INFO:root:Gibbs Iter 40, no. changed = 36, nlp = -2110.880617
2022-12-21 11:13:36,725:INFO:root:Gibbs Iter 50, no. changed = 40, nlp = -2148.281039
2022-12-21 11:13:37,672:INFO:root:Gibbs Iter 60, no. changed = 32, nlp = -2123.930828
2022-12-21 11:13:38,615:INFO:root:Gibbs Iter 70, no. changed = 32, nlp = -2138.122171
2022-12-21 11:13:39,566:INFO:root:Gibbs Iter 80, no. changed = 42, nlp = -2133.053687
2022-12-21 11:13:40,512:INFO:root:Gibbs Iter 90, no. changed = 28, nlp = -2149.414837
2022-12-21 11:13:41,456:INFO:root:Gibbs Iter 100, no. changed = 34, nlp = -2140.818547
2022-12-21 11:13:42,399:INFO:root:Gibbs Iter 110, no. changed = 29, nlp = -2135.530843
2022-12-21 11:13:43,341:INFO:root:Gibbs Iter 120, no. changed = 33, nlp = -2150.687423
2022-12-21 11:13:44,287:INFO:root:Gibbs Iter 130, no. changed = 32, nlp = -2148.096329
2022-12-21 11:13:45,235:INFO:root:Gibbs Iter 140, no. changed = 32, nlp = -2117.228596
2022-12-21 11:13:46,180:INFO:root:Gibbs Iter 150, no. changed = 32, nlp = -2096.085297
2022-12-21 11:13:47,128:INFO:root:Gibbs Iter 160, no. changed = 28, nlp = -2129.544793
2022-12-21 11:13:48,073:INFO:root:Gibbs Iter 170, no. changed = 34, nlp = -2121.551829
2022-12-21 11:13:49,017:INFO:root:Gibbs Iter 180, no. changed = 38, nlp = -2131.013274
2022-12-21 11:13:49,960:INFO:root:Gibbs Iter 190, no. changed = 38, nlp = -2119.098053
2022-12-21 11:13:50,907:INFO:root:Gibbs Iter 200, no. changed = 35, nlp = -2140.011041
2022-12-21 11:13:51,853:INFO:root:Gibbs Iter 210, no. changed = 31, nlp = -2135.803941
2022-12-21 11:13:52,798:INFO:root:Gibbs Iter 220, no. changed = 36, nlp = -2141.660394
2022-12-21 11:13:53,743:INFO:root:Gibbs Iter 230, no. changed = 29, nlp = -2131.872812
2022-12-21 11:13:54,688:INFO:root:Gibbs Iter 240, no. changed = 38, nlp = -2142.684915
2022-12-21 11:13:55,633:INFO:root:Gibbs Iter 250, no. changed = 37, nlp = -2115.640778
2022-12-21 11:13:56,580:INFO:root:Gibbs Iter 260, no. changed = 33, nlp = -2145.616603
2022-12-21 11:13:57,527:INFO:root:Gibbs Iter 270, no. changed = 35, nlp = -2134.801923
2022-12-21 11:13:58,479:INFO:root:Gibbs Iter 280, no. changed = 37, nlp = -2116.922176
2022-12-21 11:13:59,425:INFO:root:Gibbs Iter 290, no. changed = 35, nlp = -2135.867179
2022-12-21 11:14:00,380:INFO:root:Gibbs Iter 300, no. changed = 31, nlp = -2134.315556
2022-12-21 11:14:01,322:INFO:root:Gibbs Iter 310, no. changed = 33, nlp = -2141.811396
2022-12-21 11:14:02,268:INFO:root:Gibbs Iter 320, no. changed = 39, nlp = -2136.507803
2022-12-21 11:14:03,214:INFO:root:Gibbs Iter 330, no. changed = 33, nlp = -2118.859256
2022-12-21 11:14:04,162:INFO:root:Gibbs Iter 340, no. changed = 35, nlp = -2110.093575
2022-12-21 11:14:05,107:INFO:root:Gibbs Iter 350, no. changed = 31, nlp = -2101.450920
2022-12-21 11:14:06,056:INFO:root:Gibbs Iter 360, no. changed = 31, nlp = -2133.898861
2022-12-21 11:14:07,000:INFO:root:Gibbs Iter 370, no. changed = 25, nlp = -2141.793887
2022-12-21 11:14:07,945:INFO:root:Gibbs Iter 380, no. changed = 34, nlp = -2140.042556
2022-12-21 11:14:08,885:INFO:root:Gibbs Iter 390, no. changed = 34, nlp = -2067.297598
2022-12-21 11:14:09,830:INFO:root:Gibbs Iter 400, no. changed = 42, nlp = -2124.442526
2022-12-21 11:14:10,777:INFO:root:Gibbs Iter 410, no. changed = 31, nlp = -2142.841773
2022-12-21 11:14:11,722:INFO:root:Gibbs Iter 420, no. changed = 34, nlp = -2136.475555
2022-12-21 11:14:12,665:INFO:root:Gibbs Iter 430, no. changed = 31, nlp = -2116.353097
2022-12-21 11:14:13,608:INFO:root:Gibbs Iter 440, no. changed = 36, nlp = -2127.455004
2022-12-21 11:14:14,552:INFO:root:Gibbs Iter 450, no. changed = 36, nlp = -2144.883239
2022-12-21 11:14:15,495:INFO:root:Gibbs Iter 460, no. changed = 32, nlp = -2120.333997
2022-12-21 11:14:16,443:INFO:root:Gibbs Iter 470, no. changed = 40, nlp = -2123.757431
2022-12-21 11:14:17,395:INFO:root:Gibbs Iter 480, no. changed = 32, nlp = -2122.177720
2022-12-21 11:14:18,345:INFO:root:Gibbs Iter 490, no. changed = 34, nlp = -2121.902619
2022-12-21 11:14:19,291:INFO:root:Gibbs Iter 500, no. changed = 34, nlp = -2138.700498
2022-12-21 11:14:20,237:INFO:root:Gibbs Iter 510, no. changed = 36, nlp = -2134.936970
2022-12-21 11:14:21,190:INFO:root:Gibbs Iter 520, no. changed = 32, nlp = -2115.801590
2022-12-21 11:14:22,134:INFO:root:Gibbs Iter 530, no. changed = 33, nlp = -2144.007492
2022-12-21 11:14:23,078:INFO:root:Gibbs Iter 540, no. changed = 31, nlp = -2112.721972
2022-12-21 11:14:24,021:INFO:root:Gibbs Iter 550, no. changed = 34, nlp = -2105.701807
2022-12-21 11:14:24,964:INFO:root:Gibbs Iter 560, no. changed = 33, nlp = -2119.265090
2022-12-21 11:14:25,909:INFO:root:Gibbs Iter 570, no. changed = 37, nlp = -2141.490468
2022-12-21 11:14:26,854:INFO:root:Gibbs Iter 580, no. changed = 36, nlp = -2098.109613
2022-12-21 11:14:27,798:INFO:root:Gibbs Iter 590, no. changed = 36, nlp = -2100.644655
2022-12-21 11:14:28,743:INFO:root:Gibbs Iter 600, no. changed = 35, nlp = -2129.247654
2022-12-21 11:14:29,698:INFO:root:Gibbs Iter 610, no. changed = 29, nlp = -2122.443989
2022-12-21 11:14:30,643:INFO:root:Gibbs Iter 620, no. changed = 32, nlp = -2126.218240
2022-12-21 11:14:31,586:INFO:root:Gibbs Iter 630, no. changed = 36, nlp = -2148.442984
2022-12-21 11:14:32,530:INFO:root:Gibbs Iter 640, no. changed = 34, nlp = -2131.210701
2022-12-21 11:14:33,475:INFO:root:Gibbs Iter 650, no. changed = 38, nlp = -2130.215494
2022-12-21 11:14:34,420:INFO:root:Gibbs Iter 660, no. changed = 38, nlp = -2115.452509
2022-12-21 11:14:35,365:INFO:root:Gibbs Iter 670, no. changed = 36, nlp = -2100.234631
2022-12-21 11:14:36,309:INFO:root:Gibbs Iter 680, no. changed = 34, nlp = -2156.376675
2022-12-21 11:14:37,255:INFO:root:Gibbs Iter 690, no. changed = 36, nlp = -2128.780277
2022-12-21 11:14:38,198:INFO:root:Gibbs Iter 700, no. changed = 28, nlp = -2138.695091
2022-12-21 11:14:39,149:INFO:root:Gibbs Iter 710, no. changed = 41, nlp = -2134.838079
2022-12-21 11:14:40,091:INFO:root:Gibbs Iter 720, no. changed = 33, nlp = -2147.579952
2022-12-21 11:14:41,034:INFO:root:Gibbs Iter 730, no. changed = 34, nlp = -2132.406968
2022-12-21 11:14:41,979:INFO:root:Gibbs Iter 740, no. changed = 35, nlp = -2116.945840
2022-12-21 11:14:42,923:INFO:root:Gibbs Iter 750, no. changed = 32, nlp = -2119.267905
2022-12-21 11:14:43,864:INFO:root:Gibbs Iter 760, no. changed = 33, nlp = -2104.690878
2022-12-21 11:14:44,810:INFO:root:Gibbs Iter 770, no. changed = 29, nlp = -2119.885254
2022-12-21 11:14:45,757:INFO:root:Gibbs Iter 780, no. changed = 36, nlp = -2127.974870
2022-12-21 11:14:46,707:INFO:root:Gibbs Iter 790, no. changed = 33, nlp = -2125.260533
2022-12-21 11:14:47,651:INFO:root:Gibbs Iter 800, no. changed = 29, nlp = -2132.018934
2022-12-21 11:14:48,598:INFO:root:Gibbs Iter 810, no. changed = 41, nlp = -2145.357017
2022-12-21 11:14:49,543:INFO:root:Gibbs Iter 820, no. changed = 40, nlp = -2129.302556
2022-12-21 11:14:50,491:INFO:root:Gibbs Iter 830, no. changed = 33, nlp = -2121.227742
2022-12-21 11:14:51,436:INFO:root:Gibbs Iter 840, no. changed = 35, nlp = -2137.818043
2022-12-21 11:14:52,380:INFO:root:Gibbs Iter 850, no. changed = 36, nlp = -2134.218255
2022-12-21 11:14:53,326:INFO:root:Gibbs Iter 860, no. changed = 32, nlp = -2140.769408
2022-12-21 11:14:54,269:INFO:root:Gibbs Iter 870, no. changed = 30, nlp = -2128.956051
2022-12-21 11:14:55,215:INFO:root:Gibbs Iter 880, no. changed = 35, nlp = -2120.095132
2022-12-21 11:14:56,163:INFO:root:Gibbs Iter 890, no. changed = 35, nlp = -2137.676324
2022-12-21 11:14:57,108:INFO:root:Gibbs Iter 900, no. changed = 35, nlp = -2119.591822
2022-12-21 11:14:58,061:INFO:root:Gibbs Iter 910, no. changed = 32, nlp = -2129.418772
2022-12-21 11:14:59,005:INFO:root:Gibbs Iter 920, no. changed = 33, nlp = -2118.824993
2022-12-21 11:14:59,951:INFO:root:Gibbs Iter 930, no. changed = 32, nlp = -2118.432348
2022-12-21 11:15:00,893:INFO:root:Gibbs Iter 940, no. changed = 33, nlp = -2131.585322
2022-12-21 11:15:01,837:INFO:root:Gibbs Iter 950, no. changed = 33, nlp = -2120.364171
2022-12-21 11:15:02,784:INFO:root:Gibbs Iter 960, no. changed = 38, nlp = -2121.185576
2022-12-21 11:15:03,737:INFO:root:Gibbs Iter 970, no. changed = 31, nlp = -2138.805748
2022-12-21 11:15:04,681:INFO:root:Gibbs Iter 980, no. changed = 37, nlp = -2129.419985
2022-12-21 11:15:05,625:INFO:root:Gibbs Iter 990, no. changed = 31, nlp = -2141.424820
2022-12-21 11:15:06,492:INFO:root:Wrote fit stats
2022-12-21 11:15:06,499:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 11:15:06,508:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 11:15:06,512:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 11:15:06,575:INFO:root:Wrote ll_store
2022-12-21 11:15:06,581:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 11:15:06,584:INFO:root:Wrote transition error matrix
2022-12-21 11:15:06,588:INFO:root:Wrote transition error matrix
2022-12-21 11:15:06,595:INFO:root:Wrote selected variants
