2022-12-21 10:27:55,449:INFO:root:Results created in /home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/desman.2.1
2022-12-21 10:27:55,450:INFO:root:Set fixed seed for random position selection = 238329
2022-12-21 10:27:55,505:INFO:root:Running Desman with 9 samples and 94 variant positions finding 2 genomes.
2022-12-21 10:27:55,505:INFO:root:Set eta error transition matrix from = <_io.TextIOWrapper name='/home/osidibe/work/Nathan_project/DESMAN/DESMAN_iter_beta-lactam/file.outtran_df.csv' mode='r' encoding='UTF-8'>
2022-12-21 10:27:55,507:INFO:root:Set second adjustable random seed = 1
2022-12-21 10:27:55,508:INFO:root:Perform NTF initialisation
2022-12-21 10:27:55,515:INFO:root:NTF Iter 0, div = 371.571956
2022-12-21 10:27:55,624:INFO:root:NTF Iter 100, div = 37.622636
2022-12-21 10:27:55,731:INFO:root:NTF Iter 200, div = 36.998429
2022-12-21 10:27:55,840:INFO:root:NTF Iter 300, div = 36.754985
2022-12-21 10:27:55,949:INFO:root:NTF Iter 400, div = 36.459568
2022-12-21 10:27:56,058:INFO:root:NTF Iter 500, div = 36.089556
2022-12-21 10:27:56,166:INFO:root:NTF Iter 600, div = 34.607318
2022-12-21 10:27:56,275:INFO:root:NTF Iter 700, div = 30.596355
2022-12-21 10:27:56,383:INFO:root:NTF Iter 800, div = 27.711903
2022-12-21 10:27:56,492:INFO:root:NTF Iter 900, div = 25.403078
2022-12-21 10:27:56,601:INFO:root:NTF Iter 1000, div = 24.064430
2022-12-21 10:27:56,709:INFO:root:NTF Iter 1100, div = 23.624826
2022-12-21 10:27:56,818:INFO:root:NTF Iter 1200, div = 23.448389
2022-12-21 10:27:56,925:INFO:root:NTF Iter 1300, div = 23.200689
2022-12-21 10:27:57,032:INFO:root:NTF Iter 1400, div = 23.120089
2022-12-21 10:27:57,139:INFO:root:NTF Iter 1500, div = 23.096557
2022-12-21 10:27:57,247:INFO:root:NTF Iter 1600, div = 23.041348
2022-12-21 10:27:57,354:INFO:root:NTF Iter 1700, div = 23.025507
2022-12-21 10:27:57,463:INFO:root:NTF Iter 1800, div = 23.002954
2022-12-21 10:27:57,572:INFO:root:NTF Iter 1900, div = 22.996498
2022-12-21 10:27:57,681:INFO:root:NTF Iter 2000, div = 22.981235
2022-12-21 10:27:57,789:INFO:root:NTF Iter 2100, div = 22.971140
2022-12-21 10:27:57,895:INFO:root:NTF Iter 2200, div = 22.961141
2022-12-21 10:27:57,999:INFO:root:NTF Iter 2300, div = 22.948584
2022-12-21 10:27:58,104:INFO:root:NTF Iter 2400, div = 22.938869
2022-12-21 10:27:58,212:INFO:root:NTF Iter 2500, div = 22.934173
2022-12-21 10:27:58,250:INFO:root:Start Gibbs sampler burn-in phase
2022-12-21 10:27:58,376:INFO:root:Gibbs Iter 0, no. changed = 30, nlp = -2030.491846
2022-12-21 10:27:59,457:INFO:root:Gibbs Iter 10, no. changed = 29, nlp = -2062.716827
2022-12-21 10:28:00,516:INFO:root:Gibbs Iter 20, no. changed = 29, nlp = -2053.744160
2022-12-21 10:28:01,577:INFO:root:Gibbs Iter 30, no. changed = 28, nlp = -2066.544064
2022-12-21 10:28:02,634:INFO:root:Gibbs Iter 40, no. changed = 28, nlp = -2039.761252
2022-12-21 10:28:03,695:INFO:root:Gibbs Iter 50, no. changed = 28, nlp = -2060.321596
2022-12-21 10:28:04,770:INFO:root:Gibbs Iter 60, no. changed = 27, nlp = -2063.458768
2022-12-21 10:28:05,869:INFO:root:Gibbs Iter 70, no. changed = 28, nlp = -2062.289531
2022-12-21 10:28:06,926:INFO:root:Gibbs Iter 80, no. changed = 28, nlp = -2031.016939
2022-12-21 10:28:08,010:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2043.114187
2022-12-21 10:28:09,089:INFO:root:Gibbs Iter 100, no. changed = 25, nlp = -2046.639447
2022-12-21 10:28:10,139:INFO:root:Gibbs Iter 110, no. changed = 29, nlp = -2055.687922
2022-12-21 10:28:11,204:INFO:root:Gibbs Iter 120, no. changed = 27, nlp = -2059.270505
2022-12-21 10:28:12,283:INFO:root:Gibbs Iter 130, no. changed = 28, nlp = -2046.287030
2022-12-21 10:28:13,331:INFO:root:Gibbs Iter 140, no. changed = 27, nlp = -2051.869268
2022-12-21 10:28:14,371:INFO:root:Gibbs Iter 150, no. changed = 27, nlp = -2040.370642
2022-12-21 10:28:15,428:INFO:root:Gibbs Iter 160, no. changed = 26, nlp = -2048.403256
2022-12-21 10:28:16,466:INFO:root:Gibbs Iter 170, no. changed = 27, nlp = -2054.551763
2022-12-21 10:28:17,525:INFO:root:Gibbs Iter 180, no. changed = 27, nlp = -2054.632816
2022-12-21 10:28:18,581:INFO:root:Gibbs Iter 190, no. changed = 28, nlp = -2057.151381
2022-12-21 10:28:19,622:INFO:root:Gibbs Iter 200, no. changed = 28, nlp = -2055.543903
2022-12-21 10:28:20,677:INFO:root:Gibbs Iter 210, no. changed = 28, nlp = -2060.322652
2022-12-21 10:28:21,733:INFO:root:Gibbs Iter 220, no. changed = 27, nlp = -2065.007872
2022-12-21 10:28:22,800:INFO:root:Gibbs Iter 230, no. changed = 28, nlp = -2070.743743
2022-12-21 10:28:23,857:INFO:root:Gibbs Iter 240, no. changed = 25, nlp = -2064.590254
2022-12-21 10:28:24,897:INFO:root:Gibbs Iter 250, no. changed = 28, nlp = -2028.126045
2022-12-21 10:28:25,945:INFO:root:Gibbs Iter 260, no. changed = 26, nlp = -2049.880496
2022-12-21 10:28:26,992:INFO:root:Gibbs Iter 270, no. changed = 29, nlp = -2057.922459
2022-12-21 10:28:28,065:INFO:root:Gibbs Iter 280, no. changed = 28, nlp = -2052.745080
2022-12-21 10:28:29,121:INFO:root:Gibbs Iter 290, no. changed = 26, nlp = -2056.505563
2022-12-21 10:28:30,197:INFO:root:Gibbs Iter 300, no. changed = 27, nlp = -2044.352410
2022-12-21 10:28:31,271:INFO:root:Gibbs Iter 310, no. changed = 27, nlp = -2070.819428
2022-12-21 10:28:32,326:INFO:root:Gibbs Iter 320, no. changed = 28, nlp = -2058.978488
2022-12-21 10:28:33,375:INFO:root:Gibbs Iter 330, no. changed = 29, nlp = -2045.568177
2022-12-21 10:28:34,419:INFO:root:Gibbs Iter 340, no. changed = 27, nlp = -2056.391804
2022-12-21 10:28:35,480:INFO:root:Gibbs Iter 350, no. changed = 28, nlp = -2061.479503
2022-12-21 10:28:36,547:INFO:root:Gibbs Iter 360, no. changed = 26, nlp = -2032.242452
2022-12-21 10:28:37,583:INFO:root:Gibbs Iter 370, no. changed = 28, nlp = -2050.881598
2022-12-21 10:28:38,653:INFO:root:Gibbs Iter 380, no. changed = 28, nlp = -2049.268410
2022-12-21 10:28:39,705:INFO:root:Gibbs Iter 390, no. changed = 28, nlp = -2069.411838
2022-12-21 10:28:40,752:INFO:root:Gibbs Iter 400, no. changed = 27, nlp = -2062.563747
2022-12-21 10:28:41,802:INFO:root:Gibbs Iter 410, no. changed = 28, nlp = -2050.238706
2022-12-21 10:28:42,854:INFO:root:Gibbs Iter 420, no. changed = 30, nlp = -2050.924149
2022-12-21 10:28:43,900:INFO:root:Gibbs Iter 430, no. changed = 27, nlp = -2070.278836
2022-12-21 10:28:44,955:INFO:root:Gibbs Iter 440, no. changed = 28, nlp = -2053.592536
2022-12-21 10:28:46,003:INFO:root:Gibbs Iter 450, no. changed = 28, nlp = -2045.550386
2022-12-21 10:28:47,053:INFO:root:Gibbs Iter 460, no. changed = 29, nlp = -2058.762305
2022-12-21 10:28:48,117:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2037.198129
2022-12-21 10:28:49,173:INFO:root:Gibbs Iter 480, no. changed = 27, nlp = -2054.172200
2022-12-21 10:28:50,232:INFO:root:Gibbs Iter 490, no. changed = 28, nlp = -2059.196734
2022-12-21 10:28:51,288:INFO:root:Gibbs Iter 500, no. changed = 28, nlp = -2043.099833
2022-12-21 10:28:52,350:INFO:root:Gibbs Iter 510, no. changed = 27, nlp = -2053.774734
2022-12-21 10:28:53,403:INFO:root:Gibbs Iter 520, no. changed = 29, nlp = -2037.866445
2022-12-21 10:28:54,477:INFO:root:Gibbs Iter 530, no. changed = 28, nlp = -2074.814990
2022-12-21 10:28:55,528:INFO:root:Gibbs Iter 540, no. changed = 28, nlp = -2060.566016
2022-12-21 10:28:56,593:INFO:root:Gibbs Iter 550, no. changed = 28, nlp = -2060.364669
2022-12-21 10:28:57,651:INFO:root:Gibbs Iter 560, no. changed = 28, nlp = -2070.495524
2022-12-21 10:28:58,706:INFO:root:Gibbs Iter 570, no. changed = 27, nlp = -2059.903131
2022-12-21 10:28:59,772:INFO:root:Gibbs Iter 580, no. changed = 29, nlp = -2064.352762
2022-12-21 10:29:00,829:INFO:root:Gibbs Iter 590, no. changed = 27, nlp = -2047.624181
2022-12-21 10:29:01,906:INFO:root:Gibbs Iter 600, no. changed = 28, nlp = -2068.657023
2022-12-21 10:29:02,975:INFO:root:Gibbs Iter 610, no. changed = 28, nlp = -2045.880287
2022-12-21 10:29:04,050:INFO:root:Gibbs Iter 620, no. changed = 25, nlp = -2026.319682
2022-12-21 10:29:05,097:INFO:root:Gibbs Iter 630, no. changed = 28, nlp = -2048.810454
2022-12-21 10:29:06,143:INFO:root:Gibbs Iter 640, no. changed = 28, nlp = -2020.206216
2022-12-21 10:29:07,231:INFO:root:Gibbs Iter 650, no. changed = 27, nlp = -2059.949635
2022-12-21 10:29:08,300:INFO:root:Gibbs Iter 660, no. changed = 29, nlp = -2063.157162
2022-12-21 10:29:09,394:INFO:root:Gibbs Iter 670, no. changed = 29, nlp = -2065.061352
2022-12-21 10:29:10,469:INFO:root:Gibbs Iter 680, no. changed = 27, nlp = -2047.774136
2022-12-21 10:29:11,536:INFO:root:Gibbs Iter 690, no. changed = 27, nlp = -2058.392531
2022-12-21 10:29:12,615:INFO:root:Gibbs Iter 700, no. changed = 28, nlp = -2053.976872
2022-12-21 10:29:13,655:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2055.579450
2022-12-21 10:29:14,733:INFO:root:Gibbs Iter 720, no. changed = 30, nlp = -2063.062084
2022-12-21 10:29:15,783:INFO:root:Gibbs Iter 730, no. changed = 28, nlp = -2057.849414
2022-12-21 10:29:16,862:INFO:root:Gibbs Iter 740, no. changed = 28, nlp = -2072.599619
2022-12-21 10:29:17,950:INFO:root:Gibbs Iter 750, no. changed = 27, nlp = -2043.068909
2022-12-21 10:29:19,007:INFO:root:Gibbs Iter 760, no. changed = 27, nlp = -2069.253601
2022-12-21 10:29:20,066:INFO:root:Gibbs Iter 770, no. changed = 26, nlp = -2049.062003
2022-12-21 10:29:21,140:INFO:root:Gibbs Iter 780, no. changed = 29, nlp = -2060.451425
2022-12-21 10:29:22,209:INFO:root:Gibbs Iter 790, no. changed = 29, nlp = -2047.412353
2022-12-21 10:29:23,258:INFO:root:Gibbs Iter 800, no. changed = 29, nlp = -2031.714157
2022-12-21 10:29:24,323:INFO:root:Gibbs Iter 810, no. changed = 28, nlp = -2057.469414
2022-12-21 10:29:25,388:INFO:root:Gibbs Iter 820, no. changed = 29, nlp = -2042.495928
2022-12-21 10:29:26,456:INFO:root:Gibbs Iter 830, no. changed = 28, nlp = -2045.157879
2022-12-21 10:29:27,525:INFO:root:Gibbs Iter 840, no. changed = 28, nlp = -2060.238749
2022-12-21 10:29:28,581:INFO:root:Gibbs Iter 850, no. changed = 27, nlp = -2046.501969
2022-12-21 10:29:29,644:INFO:root:Gibbs Iter 860, no. changed = 28, nlp = -2043.672138
2022-12-21 10:29:30,714:INFO:root:Gibbs Iter 870, no. changed = 28, nlp = -2051.580051
2022-12-21 10:29:31,769:INFO:root:Gibbs Iter 880, no. changed = 27, nlp = -2033.319029
2022-12-21 10:29:32,831:INFO:root:Gibbs Iter 890, no. changed = 26, nlp = -2036.710901
2022-12-21 10:29:33,907:INFO:root:Gibbs Iter 900, no. changed = 28, nlp = -2056.008189
2022-12-21 10:29:34,956:INFO:root:Gibbs Iter 910, no. changed = 27, nlp = -2058.561791
2022-12-21 10:29:35,987:INFO:root:Gibbs Iter 920, no. changed = 26, nlp = -2044.934897
2022-12-21 10:29:37,033:INFO:root:Gibbs Iter 930, no. changed = 28, nlp = -2049.372671
2022-12-21 10:29:38,117:INFO:root:Gibbs Iter 940, no. changed = 25, nlp = -2048.099497
2022-12-21 10:29:39,167:INFO:root:Gibbs Iter 950, no. changed = 29, nlp = -2050.657865
2022-12-21 10:29:40,232:INFO:root:Gibbs Iter 960, no. changed = 28, nlp = -2067.040734
2022-12-21 10:29:41,297:INFO:root:Gibbs Iter 970, no. changed = 27, nlp = -2055.548199
2022-12-21 10:29:42,376:INFO:root:Gibbs Iter 980, no. changed = 27, nlp = -2057.972863
2022-12-21 10:29:43,427:INFO:root:Gibbs Iter 990, no. changed = 28, nlp = -2032.347088
2022-12-21 10:29:44,488:INFO:root:Start Gibbs sampler sampling phase
2022-12-21 10:29:44,614:INFO:root:Gibbs Iter 0, no. changed = 29, nlp = -2060.111490
2022-12-21 10:29:45,655:INFO:root:Gibbs Iter 10, no. changed = 29, nlp = -2036.325888
2022-12-21 10:29:46,729:INFO:root:Gibbs Iter 20, no. changed = 29, nlp = -2040.078952
2022-12-21 10:29:47,788:INFO:root:Gibbs Iter 30, no. changed = 27, nlp = -2065.230090
2022-12-21 10:29:48,856:INFO:root:Gibbs Iter 40, no. changed = 29, nlp = -2050.097073
2022-12-21 10:29:49,934:INFO:root:Gibbs Iter 50, no. changed = 28, nlp = -2041.404341
2022-12-21 10:29:50,979:INFO:root:Gibbs Iter 60, no. changed = 28, nlp = -2065.430831
2022-12-21 10:29:52,031:INFO:root:Gibbs Iter 70, no. changed = 28, nlp = -2064.029151
2022-12-21 10:29:53,083:INFO:root:Gibbs Iter 80, no. changed = 27, nlp = -2066.720093
2022-12-21 10:29:54,155:INFO:root:Gibbs Iter 90, no. changed = 27, nlp = -2059.477565
2022-12-21 10:29:55,221:INFO:root:Gibbs Iter 100, no. changed = 28, nlp = -2054.506963
2022-12-21 10:29:56,296:INFO:root:Gibbs Iter 110, no. changed = 28, nlp = -2056.210438
2022-12-21 10:29:57,359:INFO:root:Gibbs Iter 120, no. changed = 26, nlp = -2046.891462
2022-12-21 10:29:58,449:INFO:root:Gibbs Iter 130, no. changed = 27, nlp = -2048.286933
2022-12-21 10:29:59,502:INFO:root:Gibbs Iter 140, no. changed = 28, nlp = -2043.509865
2022-12-21 10:30:00,551:INFO:root:Gibbs Iter 150, no. changed = 27, nlp = -2064.499036
2022-12-21 10:30:01,608:INFO:root:Gibbs Iter 160, no. changed = 29, nlp = -2059.485135
2022-12-21 10:30:02,685:INFO:root:Gibbs Iter 170, no. changed = 28, nlp = -2064.889573
2022-12-21 10:30:03,749:INFO:root:Gibbs Iter 180, no. changed = 28, nlp = -2049.032637
2022-12-21 10:30:04,794:INFO:root:Gibbs Iter 190, no. changed = 26, nlp = -2031.957485
2022-12-21 10:30:05,860:INFO:root:Gibbs Iter 200, no. changed = 29, nlp = -2040.492145
2022-12-21 10:30:06,935:INFO:root:Gibbs Iter 210, no. changed = 27, nlp = -2051.007881
2022-12-21 10:30:07,996:INFO:root:Gibbs Iter 220, no. changed = 27, nlp = -2030.750601
2022-12-21 10:30:09,063:INFO:root:Gibbs Iter 230, no. changed = 27, nlp = -2064.952081
2022-12-21 10:30:10,110:INFO:root:Gibbs Iter 240, no. changed = 27, nlp = -2051.658288
2022-12-21 10:30:11,176:INFO:root:Gibbs Iter 250, no. changed = 28, nlp = -2043.031926
2022-12-21 10:30:12,245:INFO:root:Gibbs Iter 260, no. changed = 27, nlp = -2044.477535
2022-12-21 10:30:13,322:INFO:root:Gibbs Iter 270, no. changed = 27, nlp = -2050.948173
2022-12-21 10:30:14,360:INFO:root:Gibbs Iter 280, no. changed = 27, nlp = -2065.017155
2022-12-21 10:30:15,417:INFO:root:Gibbs Iter 290, no. changed = 29, nlp = -2055.461279
2022-12-21 10:30:16,470:INFO:root:Gibbs Iter 300, no. changed = 29, nlp = -2031.690923
2022-12-21 10:30:17,501:INFO:root:Gibbs Iter 310, no. changed = 28, nlp = -2059.804633
2022-12-21 10:30:18,527:INFO:root:Gibbs Iter 320, no. changed = 26, nlp = -2059.532627
2022-12-21 10:30:19,558:INFO:root:Gibbs Iter 330, no. changed = 28, nlp = -2064.732542
2022-12-21 10:30:20,588:INFO:root:Gibbs Iter 340, no. changed = 26, nlp = -2058.023742
2022-12-21 10:30:21,614:INFO:root:Gibbs Iter 350, no. changed = 27, nlp = -2060.280216
2022-12-21 10:30:22,636:INFO:root:Gibbs Iter 360, no. changed = 29, nlp = -2060.681025
2022-12-21 10:30:23,666:INFO:root:Gibbs Iter 370, no. changed = 29, nlp = -2059.283054
2022-12-21 10:30:24,696:INFO:root:Gibbs Iter 380, no. changed = 28, nlp = -2040.840076
2022-12-21 10:30:25,725:INFO:root:Gibbs Iter 390, no. changed = 29, nlp = -2060.333253
2022-12-21 10:30:26,756:INFO:root:Gibbs Iter 400, no. changed = 27, nlp = -2045.294285
2022-12-21 10:30:27,787:INFO:root:Gibbs Iter 410, no. changed = 28, nlp = -2042.647293
2022-12-21 10:30:28,815:INFO:root:Gibbs Iter 420, no. changed = 28, nlp = -2069.677738
2022-12-21 10:30:29,852:INFO:root:Gibbs Iter 430, no. changed = 29, nlp = -2059.725212
2022-12-21 10:30:30,885:INFO:root:Gibbs Iter 440, no. changed = 28, nlp = -2061.711034
2022-12-21 10:30:31,925:INFO:root:Gibbs Iter 450, no. changed = 27, nlp = -2047.472419
2022-12-21 10:30:32,957:INFO:root:Gibbs Iter 460, no. changed = 24, nlp = -2064.110915
2022-12-21 10:30:33,979:INFO:root:Gibbs Iter 470, no. changed = 27, nlp = -2054.731262
2022-12-21 10:30:35,007:INFO:root:Gibbs Iter 480, no. changed = 26, nlp = -2041.951578
2022-12-21 10:30:36,035:INFO:root:Gibbs Iter 490, no. changed = 26, nlp = -2061.397295
2022-12-21 10:30:37,064:INFO:root:Gibbs Iter 500, no. changed = 27, nlp = -2040.488862
2022-12-21 10:30:38,086:INFO:root:Gibbs Iter 510, no. changed = 28, nlp = -2057.396987
2022-12-21 10:30:39,040:INFO:root:Gibbs Iter 520, no. changed = 28, nlp = -2066.950486
2022-12-21 10:30:39,988:INFO:root:Gibbs Iter 530, no. changed = 28, nlp = -2037.032474
2022-12-21 10:30:40,934:INFO:root:Gibbs Iter 540, no. changed = 27, nlp = -2038.711998
2022-12-21 10:30:41,879:INFO:root:Gibbs Iter 550, no. changed = 27, nlp = -2046.963460
2022-12-21 10:30:42,832:INFO:root:Gibbs Iter 560, no. changed = 27, nlp = -2057.861247
2022-12-21 10:30:43,781:INFO:root:Gibbs Iter 570, no. changed = 28, nlp = -2062.687433
2022-12-21 10:30:44,729:INFO:root:Gibbs Iter 580, no. changed = 28, nlp = -2046.800618
2022-12-21 10:30:45,679:INFO:root:Gibbs Iter 590, no. changed = 28, nlp = -2066.163603
2022-12-21 10:30:46,629:INFO:root:Gibbs Iter 600, no. changed = 28, nlp = -2059.771779
2022-12-21 10:30:47,578:INFO:root:Gibbs Iter 610, no. changed = 27, nlp = -2061.142881
2022-12-21 10:30:48,528:INFO:root:Gibbs Iter 620, no. changed = 28, nlp = -2058.889560
2022-12-21 10:30:49,478:INFO:root:Gibbs Iter 630, no. changed = 28, nlp = -2018.035035
2022-12-21 10:30:50,426:INFO:root:Gibbs Iter 640, no. changed = 26, nlp = -2057.675285
2022-12-21 10:30:51,377:INFO:root:Gibbs Iter 650, no. changed = 28, nlp = -2027.668185
2022-12-21 10:30:52,327:INFO:root:Gibbs Iter 660, no. changed = 29, nlp = -2045.623638
2022-12-21 10:30:53,273:INFO:root:Gibbs Iter 670, no. changed = 29, nlp = -2047.051142
2022-12-21 10:30:54,217:INFO:root:Gibbs Iter 680, no. changed = 26, nlp = -2061.906304
2022-12-21 10:30:55,165:INFO:root:Gibbs Iter 690, no. changed = 28, nlp = -2052.637370
2022-12-21 10:30:56,114:INFO:root:Gibbs Iter 700, no. changed = 26, nlp = -2066.945920
2022-12-21 10:30:57,064:INFO:root:Gibbs Iter 710, no. changed = 28, nlp = -2032.568040
2022-12-21 10:30:58,008:INFO:root:Gibbs Iter 720, no. changed = 29, nlp = -2047.415039
2022-12-21 10:30:58,958:INFO:root:Gibbs Iter 730, no. changed = 28, nlp = -2060.851882
2022-12-21 10:30:59,904:INFO:root:Gibbs Iter 740, no. changed = 25, nlp = -2050.589736
2022-12-21 10:31:00,852:INFO:root:Gibbs Iter 750, no. changed = 27, nlp = -2060.881520
2022-12-21 10:31:01,799:INFO:root:Gibbs Iter 760, no. changed = 29, nlp = -2034.271792
2022-12-21 10:31:02,750:INFO:root:Gibbs Iter 770, no. changed = 29, nlp = -2055.986529
2022-12-21 10:31:03,698:INFO:root:Gibbs Iter 780, no. changed = 29, nlp = -2049.767093
2022-12-21 10:31:04,646:INFO:root:Gibbs Iter 790, no. changed = 27, nlp = -2051.940832
2022-12-21 10:31:05,595:INFO:root:Gibbs Iter 800, no. changed = 28, nlp = -2047.759275
2022-12-21 10:31:06,548:INFO:root:Gibbs Iter 810, no. changed = 28, nlp = -2036.463272
2022-12-21 10:31:07,495:INFO:root:Gibbs Iter 820, no. changed = 28, nlp = -2048.459718
2022-12-21 10:31:08,442:INFO:root:Gibbs Iter 830, no. changed = 28, nlp = -2062.576026
2022-12-21 10:31:09,393:INFO:root:Gibbs Iter 840, no. changed = 30, nlp = -2033.387234
2022-12-21 10:31:10,340:INFO:root:Gibbs Iter 850, no. changed = 30, nlp = -2065.846728
2022-12-21 10:31:11,291:INFO:root:Gibbs Iter 860, no. changed = 28, nlp = -2057.769023
2022-12-21 10:31:12,238:INFO:root:Gibbs Iter 870, no. changed = 27, nlp = -2049.905759
2022-12-21 10:31:13,186:INFO:root:Gibbs Iter 880, no. changed = 27, nlp = -2039.854165
2022-12-21 10:31:14,140:INFO:root:Gibbs Iter 890, no. changed = 29, nlp = -2028.836879
2022-12-21 10:31:15,090:INFO:root:Gibbs Iter 900, no. changed = 29, nlp = -2052.107786
2022-12-21 10:31:16,040:INFO:root:Gibbs Iter 910, no. changed = 26, nlp = -2030.776458
2022-12-21 10:31:16,991:INFO:root:Gibbs Iter 920, no. changed = 28, nlp = -2034.522057
2022-12-21 10:31:17,941:INFO:root:Gibbs Iter 930, no. changed = 28, nlp = -2068.763348
2022-12-21 10:31:18,919:INFO:root:Gibbs Iter 940, no. changed = 27, nlp = -2047.203722
2022-12-21 10:31:19,865:INFO:root:Gibbs Iter 950, no. changed = 27, nlp = -2045.787827
2022-12-21 10:31:20,810:INFO:root:Gibbs Iter 960, no. changed = 28, nlp = -2037.106193
2022-12-21 10:31:21,759:INFO:root:Gibbs Iter 970, no. changed = 28, nlp = -2054.927024
2022-12-21 10:31:22,705:INFO:root:Gibbs Iter 980, no. changed = 28, nlp = -2041.633843
2022-12-21 10:31:23,652:INFO:root:Gibbs Iter 990, no. changed = 26, nlp = -2050.647757
2022-12-21 10:31:24,513:INFO:root:Wrote fit stats
2022-12-21 10:31:24,519:INFO:root:Wrote filtered tau star haplotype predictions
2022-12-21 10:31:24,525:INFO:root:Wrote probabilistic tau haplotype predictions
2022-12-21 10:31:24,529:INFO:root:Wrote gamma haplotype relative frequencies
2022-12-21 10:31:24,578:INFO:root:Wrote ll_store
2022-12-21 10:31:24,582:INFO:root:Wrote mean gamma haplotype relative frequencies
2022-12-21 10:31:24,586:INFO:root:Wrote transition error matrix
2022-12-21 10:31:24,591:INFO:root:Wrote transition error matrix
2022-12-21 10:31:24,596:INFO:root:Wrote selected variants
